#!/usr/bin/env bash

## Initialize a Docksal powered Drupal 8 site
##
## Usage: fin init

# Abort if anything fails
set -e

#-------------------------- Helper functions --------------------------------

# Console colors
red='\033[0;31m'
green='\033[0;32m'
green_bg='\033[42m'
yellow='\033[1;33m'
NC='\033[0m'

echo-red () { echo -e "${red}$1${NC}"; }
echo-green () { echo -e "${green}$1${NC}"; }
echo-green-bg () { echo -e "${green_bg}$1${NC}"; }
echo-yellow () { echo -e "${yellow}$1${NC}"; }

# Fix file/folder permissions
fix_permissions ()
{
	echo-green "Making site directory writable..."
	chmod 755 "${PROJECT_ROOT}"
}

# Project initialization steps
echo -e "${green_bg} Step 1 ${NC}${green} Initializing local project configuration...${NC}"
fix_permissions

echo -e "${green_bg} Step 2 ${NC}${green} Creating services...${NC}"
fin up

SITE_DIRECTORY="default"
DOCROOT_PATH="${PROJECT_ROOT}/${DOCROOT}"
SITEDIR_PATH="${DOCROOT_PATH}/sites/${SITE_DIRECTORY}"

echo -e "${green_bg} Step 3 ${NC}${green} Downloading dependencies...${NC}"
cd $PROJECT_ROOT
fin exec "composer install"

echo -e "${green_bg} Step 4 ${NC}${green} Importing database...${NC}"
cd $PROJECT_ROOT
fin import_db

echo -e "${green_bg} Step 5 ${NC}${green} Running post install commands...${NC}"
cd $DOCROOT_PATH
fin post

echo -e "Open ${yellow}http://${VIRTUAL_HOST}${NC} in your browser to verify the setup."
