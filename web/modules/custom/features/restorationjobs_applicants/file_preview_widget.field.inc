<?php
/**
 * @file
 * Field module functionality for the Restoration Jobs Applicants FilePreview Widget.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Template\Attribute;
use Drupal\image\Entity\ImageStyle;

/**
 * Prepares variables for multi file form widget templates.
 *
 * Default template: nice-imagefield-widget-multiple.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element representing the widgets.
 */
function template_preprocess_file_preview_widget(&$variables) {
  global $base_url;
  $module_route = \Drupal::moduleHandler()->getModule('restorationjobs_applicants')->getPath();

  $style = ImageStyle::load('large');
  $element = $variables['element'];

  // Special ID and classes for draggable tables.
  $weight_class = $element['#id'] . '-weight';
  $sortable_id = $element['#id'] . '-sortable';

  // Get our list of widgets in order (needed when the form comes back after
  // preview or failed validation).
  $widgets = array();
  foreach (Element::children($element) as $key) {
    $widgets[] = &$element[$key];
  }
  usort($widgets, '_field_multiple_value_form_sort_helper');

  $has_files = FALSE;
  foreach ($widgets as $key => &$widget) {
    if(!empty($widget['#files'])) {
      $has_files = TRUE;
    }
  }
  $variables['attributes']['data-message'] = '+ ' . $variables["element"]["#title"];
  if (!$has_files) {
    $widgets[] = $element;
  }

  $rows = array();
  $i = 0;
  foreach ($widgets as $key => &$widget) {
    $mime_type_full = null;
    $mime_type = null;

    hide($widget['_weight']);
    if(!empty($widget['#files'])) {
      $current_file = reset($widget['#files']);
      $mime_type_full =  $current_file->getMimeType();
      $mime_type = explode('/',$mime_type_full)[0];
      $row = array();
      $field = $variables['element']['#field_name'];
      if ($field == 'field_resume') {
        $row['download_link'] = '<a href="' . $current_file->createFileUrl() . '">Download Resumé</a>';
      } else {
        switch ($mime_type) {
          case 'image':
            $image_uri = $current_file->getFileUri();
            $row['preview'] = $style->buildUrl($image_uri);
            break;
          default:
            $image_preview = $module_route . '/img/preview_document.jpg';
            $image = Drupal::service('image.factory')->get($image_preview);
            $row['preview'] = $image->getSource();
            break;
        }
      }
      $row['key'] = $i;
      $rows[] = $row;
      $i++;
    }
    $variables['items'] = $rows;
  }
  if (isset($element["#upload_validators"])) {
    $variables['attributes']['data-extensions'] = $element["#upload_validators"]["file_validate_extensions"][0];
    $variables['attributes']['data-size'] = $element["#upload_validators"]["file_validate_size"][0];
  } elseif (isset($element[0]["#upload_validators"])) {
    $variables['attributes']['data-extensions'] = $element[0]["#upload_validators"]["file_validate_extensions"][0];
    $variables['attributes']['data-size'] = $element[0]["#upload_validators"]["file_validate_size"][0];
  }
  $variables['element'] = $element;
}
