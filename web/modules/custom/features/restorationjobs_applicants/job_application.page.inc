<?php

/**
 * @file
 * Contains job_application.page.inc.
 *
 * Page callback for Job application entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Job application templates.
 *
 * Default template: job_application.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_job_application(array &$variables) {
  // Fetch JobApplication Entity Object.
  $job_application = $variables['elements']['#job_application'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
