(function($) {
  Drupal.behaviors.filePreview = {
    attach: function attach(context, settings) {
      $('.field--widget-file-preview-widget').each(function() {
        var $wrapper = $(this);
        var $file_selector = $(this).find('.file-element');
        $file_selector.find('input[type="file"]').attr('data-message', $file_selector.attr('data-message'));
        $file_selector.find('input[type="file"]').once().on('change', function () {
          if (this.files.length) {
            var file = this.files[0];
            var ext = file.name.split('.').pop().toLowerCase();
            var allowed_extensions_str = $file_selector.attr('data-extensions');
            var allowed_extensions = allowed_extensions_str.split(" ");
            var allowed_size = $file_selector.attr('data-size')
            if (allowed_extensions.indexOf(ext) > -1) {
              if (file.size <= allowed_size) {
                $(this).attr('data-message', 'Processing...');
                $file_selector.find('.js-form-submit').trigger('mousedown');
              } else {
                alert('File size error');
              }
            } else {
              alert('Wrong file format.');
              $(this).val('');
            }
          }
        });
        if ($wrapper.find('.form-managed-file').length) {
          $wrapper.find('.form-managed-file').each(function (index, value) {
            $(this).appendTo($wrapper.find('.image-item[data-key="' + index + '"] .image-meta'));
          });
        } else {
          $file_selector.appendTo($wrapper.find('.image-item[data-key="0"] .image-meta'));
        }
      });
    }
  }
})(jQuery);
