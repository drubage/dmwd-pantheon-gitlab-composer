(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.job_application_owner_profile = {
    attach: function (context, settings) {
      $('.favorite, .decline').each(function(event) {
        $(this).on('click', function (event) {
          event.preventDefault();
          url = $(this).attr('href');

          Drupal.ajax({
            url: url,
            base: false,
            element: false,
            progress: false
          }).execute();
        });
      });

    }
  };

})(jQuery, Drupal, drupalSettings);
