function initialize() {
  var input = document.getElementById('location_autocomplete');
  var autocomplete = new google.maps.places.Autocomplete(input);
  // My jobs page.
  input = document.getElementById('edit-field-job-location-0-value');
  new google.maps.places.Autocomplete(input);
  input = document.getElementById('edit-location--2');
  new google.maps.places.Autocomplete(input);

}
google.maps.event.addDomListener(window, 'load', initialize);

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.saved_searches = {
    attach: function (context, settings) {
      $('.job-applicants-delete a').each(function (event) {
        $(this).once().on('click', function (event) {
          event.preventDefault();
          let id = $(this).data('search-id');
          url = `/ajax/saved-search/${ id }/modal`;

          Drupal.ajax({
            url: url,
            base: false,
            element: false,
            progress: false
          }).execute();
        });
      });
    }
  }

})(jQuery, Drupal, drupalSettings);
