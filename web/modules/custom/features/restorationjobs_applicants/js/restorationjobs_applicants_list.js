(function($) {
  Drupal.behaviors.applicantsFilter = {
    attach: function attach(context, settings) {

      var search = settings.job_search;

      var url = window.location.pathname;

      // Filter toggle.
      $('.filter-toggle').on('click', function() {
        $(this).toggleClass('active');
        $('.header-filter-wrapper').slideToggle();
      });

      if ($('.job-application-list-header .active-filter').length) {
        $('.filter-toggle').toggleClass('active');
        $('.header-filter-wrapper').removeClass('no-filter');
      }

      $('.job-application-list-header').find('.header-filter').click(function() {
        var filter_name = $(this).data('filter-name');
        var filter_val = $(this).data('filter-id').toString();
        var action = 'add';
        if ($(this).hasClass('active-filter')) {
          action = 'remove';
        }
        if (action === 'add') {
          $(this).addClass('active-filter');
          search[filter_name].push(filter_val);
        } else {
          $(this).removeClass('active-filter');
          var index = search[filter_name].indexOf(filter_val);
          if (index !== -1) {
            search[filter_name].splice(index, 1);
          }
        }
        reload_page();
      });

      // search text
      $('.search-submit').on('click', function(e) {
        e.preventDefault();
        search_filter_submit();
      });

      $('.search-filter').on('keydown', function(e) {
        if (e.keyCode == 13) {
          e.preventDefault();
          search_filter_submit();
        }
      });

      function search_filter_submit() {
        search.combine = $('.search-filter').val();
        reload_page();
      }

      // sort
      $('.sort-filter').click(function() {
        search.sort_order = $(this).data('sort-order');
        search.sort_by = $(this).data('sort-by');
        reload_page();
      });

      // update query string and reload page
      function reload_page() {
        $('#page-content').addClass('loading').after($('<div class="filter-progress ajax-progress ajax-progress-throbber"><i class="glyphicon glyphicon-refresh glyphicon-spin"></i></div>'));
        queryString = $.param(search);
        window.location.href = url + '?' + queryString;
      }

    }
  };

})(jQuery);
