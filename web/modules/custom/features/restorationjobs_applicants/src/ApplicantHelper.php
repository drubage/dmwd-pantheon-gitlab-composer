<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\restorationjobs_applicants\Entity\SavedSearches;
use Drupal\restorationjobs_companies\Entity\JobPosting;
use Drupal\restorationjobs_applicants\Entity\JobApplication;

class ApplicantHelper {

  public static function getUserApplications($uid = NULL) {
    if ($uid == NULL) {
      $uid = \Drupal::currentUser()->id();
    }
    $query = \Drupal::entityQuery('job_application')
      ->condition('user_id', $uid);
    $results = $query->execute();
    return array_keys($results);
  }

  public static function ajaxDelete($saved_searches) {
    $response = new AjaxResponse();
    $search = SavedSearches::load($saved_searches);
    $search->delete();

    $response->addCommand(new RemoveCommand('.saved-search-row-' . $saved_searches));
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  public static function ajaxDeleteAccess($saved_searches) {
    $search = SavedSearches::load($saved_searches);
    $uid = \Drupal::currentUser()->id();

    if ($uid === $search->getOwnerId()) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }


  /**
   * Open modal submit.
   */
  public function deleteSearchModal($saved_searches) {
    $response = new AjaxResponse();
    $response->addCommand(
      new OpenModalDialogCommand(t('Hold Up'), $this->confirmModalContent($saved_searches))
    );
    return $response;
  }

  /**
   * Close modal submit.
   */
  public function closeModal() {
    $response = new AjaxResponse();
    $response->addCommand(
      new CloseModalDialogCommand()
    );
    return $response;
  }

  /**
   * Generate the confirmation modal.
   *
   * @return array
   *   Render array
   */
  protected function confirmModalContent($saved_searches) {
    $modal['disclaimer'] = [
      '#prefix' => '<div>',
      '#markup' => t('Do you want to cancel this alert?'),
      '#suffix' => '</div>',
    ];

    $modal['actions']['#weight'] = 10;
    $cancel_link = Link::fromTextAndUrl(
      t('Cancel'),
      Url::fromRoute('restorationjobs_applicants.saved_search_modal_close')
    )->toRenderable();
    $cancel_link['#attributes'] = ['class' => ['btn', 'btn-white-outline', 'btn-back', 'use-ajax']];
    $modal['actions']['edit'] = $cancel_link;
    $confirm_link = Link::fromTextAndUrl(
      t('Confirm'),
      Url::fromRoute('restorationjobs_applicants.saved_search_delete', [
        'saved_searches' => $saved_searches
      ]),
      ['attributes' => ['class' => ['btn', 'btn-primary']]]
    )->toRenderable();
    $confirm_link['#attributes'] = ['class' => ['btn', 'btn-white-primary', 'use-ajax']];
    $modal['actions']['confirm'] = $confirm_link;
    $modal['actions']['#prefix'] = '<div class="modal-actions">';
    $modal['actions']['#suffix'] = '</div>';
    $modal['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $modal;
  }


  /**
   * Withdraw application modal.
   */
  function withdrawModal($job_posting) {
    $job_posting = JobPosting::load($job_posting);

    $modal['disclaimer'] = [
      '#prefix' => '<div>',
      '#markup' => t('Are you sure you want to withdraw your application from @title?', [
        '@title' => $job_posting->getTitle()
      ]),
      '#suffix' => '</div>',
    ];

    $modal['actions']['#weight'] = 10;
    $cancel_link = Link::fromTextAndUrl(
      t('Cancel'),
      Url::fromRoute('restorationjobs_applicants.saved_search_modal_close')
    )->toRenderable();
    $cancel_link['#attributes'] = ['class' => ['btn', 'btn-white-outline', 'btn-back', 'use-ajax']];
    $modal['actions']['edit'] = $cancel_link;
    $confirm_link = Link::fromTextAndUrl(
      t('Confirm'),
      Url::fromRoute('restorationjobs_applicants.withdraw_application', [
        'job_posting' => $job_posting->id()
      ]),
      ['attributes' => ['class' => ['btn', 'btn-primary']]]
    )->toRenderable();
    $confirm_link['#attributes'] = ['class' => ['btn', 'btn-white-primary', 'use-ajax']];
    $modal['actions']['confirm'] = $confirm_link;
    $modal['actions']['#prefix'] = '<div class="modal-actions">';
    $modal['actions']['#suffix'] = '</div>';
    $modal['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $response = new AjaxResponse();
    $response->addCommand(
      new OpenModalDialogCommand(t('Hold Up'), $modal)
    );
    return $response;
  }

  /**
   * Withdraw application.
   */
  function withdrawApplication($job_posting) {
    $job_posting = JobPosting::load($job_posting);

    $url = Url::fromRoute('entity.job_posting.canonical', [
      'job_posting' => $job_posting->id()
    ]);

    $app_id = $job_posting->hasApplied(NULL, TRUE);
    if ($app_id != FALSE && $app_id != '') {
      $app = JobApplication::load($app_id);
      $app->delete();
    }

    drupal_set_message(t('You have successfully withdrawn your application from @title', [
      '@title' => $job_posting->getTitle()
    ]));

    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url->toString()));
    return $response;
  }

}
