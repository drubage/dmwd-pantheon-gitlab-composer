<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\restorationjobs_applicants\Entity\JobApplicationInterface;

/**
 * Class JobApplicationController.
 *
 *  Returns responses for Job application routes.
 */
class JobApplicationController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Job application  revision.
   *
   * @param int $job_application_revision
   *   The Job application  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($job_application_revision) {
    $job_application = $this->entityManager()->getStorage('job_application')->loadRevision($job_application_revision);
    $view_builder = $this->entityManager()->getViewBuilder('job_application');

    return $view_builder->view($job_application);
  }

  /**
   * Page title callback for a Job application  revision.
   *
   * @param int $job_application_revision
   *   The Job application  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($job_application_revision) {
    $job_application = $this->entityManager()->getStorage('job_application')->loadRevision($job_application_revision);
    return $this->t('Revision of %title from %date', ['%title' => $job_application->label(), '%date' => format_date($job_application->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Job application .
   *
   * @param \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface $job_application
   *   A Job application  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(JobApplicationInterface $job_application) {
    $account = $this->currentUser();
    $langcode = $job_application->language()->getId();
    $langname = $job_application->language()->getName();
    $languages = $job_application->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $job_application_storage = $this->entityManager()->getStorage('job_application');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $job_application->label()]) : $this->t('Revisions for %title', ['%title' => $job_application->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all job application revisions") || $account->hasPermission('administer job application entities')));
    $delete_permission = (($account->hasPermission("delete all job application revisions") || $account->hasPermission('administer job application entities')));

    $rows = [];

    $vids = $job_application_storage->revisionIds($job_application);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\restorationjobs_applicants\JobApplicationInterface $revision */
      $revision = $job_application_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $job_application->getRevisionId()) {
          $link = $this->l($date, new Url('entity.job_application.revision', ['job_application' => $job_application->id(), 'job_application_revision' => $vid]));
        }
        else {
          $link = $job_application->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.job_application.translation_revert', ['job_application' => $job_application->id(), 'job_application_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.job_application.revision_revert', ['job_application' => $job_application->id(), 'job_application_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.job_application.revision_delete', ['job_application' => $job_application->id(), 'job_application_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['job_application_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
