<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\restorationjobs_applicants\Controller\Employers;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class JobApplicationFlowController.
 */
class JobApplicationFlowController extends ControllerBase {
  /**
   * Render the first step of the job application flow
   * {@inheritdoc}
   */
  public function identity(JobPostingInterface $job_posting) {
    $user = \Drupal::currentUser();
    $session = \Drupal::request()->getSession();
    $session->set('job_application.job_id', $job_posting->id());

    if ($user->isAuthenticated()) {
      return $this->redirect('restorationjobs_applicants.job_application_flow_controller_basic', ['job_posting' => $job_posting->id()]);
    } else {
      $form = \Drupal::formBuilder()->getForm('\Drupal\restorationjobs_applicants\Form\ConfirmIdentityJobApplyForm', $job_posting);
    }

    $build = [
      '#theme' => 'social_auth_block',
      '#route_name' => \Drupal::routeMatch()->getRouteName(),
      '#type' => 'seeker',
      '#job_posting' => $job_posting->id(),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $social_login = render($build);

    $signin_link = \Drupal::url('user.login', [], [
      'query' => [
        'job' => $job_posting->id(),
      ]
    ]);

    return [
      '#theme' => 'confirm_identity_job_apply',
      '#social_login' => $social_login,
      '#signin_link' => $signin_link,
      '#form' => $form
    ];
  }

  /**
   * Render a custom register form for user's basic information
   * {@inheritdoc}
   */
  public function basic(JobPostingInterface $job_posting = NULL) {
    $session = \Drupal::request()->getSession();
    $user = \Drupal::currentUser();
    if (!isset($job_posting) && $session->get('job_application.job_id')) {
      $job_posting = \Drupal::entityTypeManager()->getStorage('job_posting')->load($session->get('job_application.job_id'));
      $url = \Drupal::url('restorationjobs_applicants.job_application_flow_controller_basic', ['job_posting' => $job_posting->id()]);
      return new RedirectResponse($url);
    }
    if (!isset($job_posting)) {
      $url = \Drupal::url('view.job_postings.page_1');
      return new RedirectResponse($url);
    }
    if ($job_posting->hasApplied($user->id())) {
      return $this->redirect('restorationjobs_applicants.job_application_flow_controller_complete', ['job_posting' => $job_posting->id()]);
    } elseif (!$user->isAuthenticated() && $session->get('job_application.email') && user_load_by_mail($session->get('job_application.email'))) {
      $form = \Drupal::formBuilder()->getForm('Drupal\user\Form\UserLoginForm');
      drupal_set_message(t('This email address already exists. Please login below.'));
    } else {
      if ($user->isAuthenticated()) {
        $account = \Drupal\user\Entity\User::load($user->id());
        if ($account->hasRole('email_verified')) {
          if (empty($account->get('field_profile_complete')->value)) {
            // User has account and it's logged in, but not filled the basic information yet.
            $formObject = \Drupal::entityTypeManager()
              ->getFormObject('user', 'step_2_job_applicant')
              ->setEntity($account);
          } else {
            // User has account, and basic information is filled, so creates the job apply
            $session->remove('job_application.job_id');
            restorationjobs_applicants_job_apply($job_posting, $account);
            return $this->redirect('restorationjobs_applicants.job_application_flow_controller_complete', ['job_posting' => $job_posting->id()]);
          }
        } else {
          _user_mail_notify('register_no_approval_required', $account);
          $url = \Drupal::url('restorationjobs_users.user_validate_email');
          return new RedirectResponse($url);
        }
      } else {
        $account = \Drupal::entityTypeManager()->getStorage('user')->create(array());
        $formObject = \Drupal::entityTypeManager()
          ->getFormObject('user', 'step_1_job_applicant')
          ->setEntity($account);
      }
      $form = \Drupal::formBuilder()->getForm($formObject);
    }
    return $form;
  }

  /**
   * Renders the job application confirmation page
   * {@inheritdoc}
   */
  public function complete(JobPostingInterface $job_posting = NULL) {
    $account = User::load(\Drupal::currentUser()->id());

    $session = \Drupal::request()->getSession();
    $session->remove('job_application.job_id');

    $fields_to_check = array(
      'field_certifications',
      'field_hard_skills',
      'field_soft_skills',
      'field_first_name',
      'field_last_name',
      'field_profile_description',
      'field_profile_photo',
      'field_resume',
      'field_user_location',
    );
    $profile_complete = TRUE;
    foreach($fields_to_check as $field) {
      if ($account->get($field)->isEmpty()) {
        $profile_complete = FALSE;
        break;
      }
    }
    if ($profile_complete) {
      $cta = [
        'url' => Url::fromRoute('restorationjobs_applicants.homepage')->toString(),
        'link_text' => 'Go to My Jobs'
      ];
    } else {
      $cta = [
        'url' => Url::fromRoute('restorationjobs_applicants.applicant_profile')->toString(),
        'link_text' => 'Go to Profile'
      ];
    }
    drupal_get_messages(NULL, TRUE); // unset profile save messages

    return [
      '#theme' => 'job_application_complete',
      '#cta' => $cta,
      '#profile_complete' => $profile_complete,
      '#attached' => array(
        'library' => array(
          'restorationjobs_applicants/custom'
        ),
      ),
    ];
  }

  /**
   * Renders the job application confirmation page
   * {@inheritdoc}
   */
  public function validate_email() {
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->hasRole('email_verified')) {
      $url = \Drupal::url('restorationjobs_applicants.applicants_step2');
      return new RedirectResponse($url);
    }
    $build = [
      '#theme' => 'employers_validate_email',
      '#cache' => [ // NEED TO REMOVE THIS WHEN GOING LIVE
        'max-age' => 0
      ],
    ];

    return $build;
  }

  /**
   * Deny access to anonymous and owner
   */
  public static function applyAccess(JobPostingInterface $job_posting) {
    $uid = \Drupal::currentUser()->id();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $uid]);

    if ($company) {
      return AccessResult::forbidden();
    }
    // todo prevent access if user has already applied
    return AccessResult::allowed();
  }

  /**
   * Check if user has already applied.
   */
  function hasApplied($job_id, $uid) {
    $query = \Drupal::entityTypeManager()->getStorage('job_application')->getQuery();
    $query->condition('field_job_posting', $job_id);
    if($uid != NULL){
      $query->condition('user_id', $uid,'=');
    }
    $application_number = $query->count()->execute();
    return $application_number >= 1 ? TRUE : FALSE;
  }
}
