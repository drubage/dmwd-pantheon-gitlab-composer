<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\restorationjobs_applicants\Entity\JobApplication;

/**
 * Class JobApplicationOwnerProfile.
 */
class JobApplicationOwnerProfile extends ControllerBase {

  /**
   * Job application owner profile page.
   */
  public function profile(JobPostingInterface $job_posting = NULL, User $user = NULL) {
    $app = [];

    if (!$user) {
      $user = User::load(\Drupal::currentUser()->id());
    }

    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $user->id()]);
    if ($company) {
      return \Drupal\restorationjobs_companies\Controller\Employers::profile();
    }

    if (!\Drupal::routeMatch()->getParameter('job_posting')) {
	    $formObject = \Drupal::entityTypeManager()
        ->getFormObject('user', 'profile_job_applicant')
        ->setEntity($user);
      return \Drupal::formBuilder()->getForm($formObject);
    }

    // User name.
    $app['name'] = $user->get('field_first_name')->getString() . ' ' . $user->get('field_last_name')->getString();

    if ($job_posting) {
      // Match.
      $job_application = \Drupal::entityManager()->getStorage('job_application')
        ->loadByproperties([
          'field_job_posting' => $job_posting->id(),
          'user_id' => $user->id()
        ]);
      $job_application = reset($job_application);

      // Status.
      $app['status'] = $job_application->get('field_status')->getString();

      // Match.
      $app['match'] = $job_application->get('field_applicant_score')->getString();

      // Ids.
      $app['appId'] = $job_application->id();
      $app['jobId'] = $job_posting->id();
    }

    // Location.
    $app['location'] = $user->get('field_user_location')->getString();

    $app['user'] = $user->id();

    // Resumé.
    if ($file = $user->get('field_resume')->first()) {
      $resume = $file->entity->getFileUri();
      $app['resume'] = file_create_url($resume);
    }

    // Profile image.
    if ($file = $user->get('field_profile_photo')->first()) {
      $imageUri = $file->entity->getFileUri();
      $style = ImageStyle::load('user_profile_photos');
      $app['profile_photo'] = $style->buildUrl($imageUri);
    }

    // Profile description.
    $app['description'] = $user->get('field_profile_description')->getString();
    $app['description'] = str_replace("\n", "<br />", $app['description']);

    // Photos.
    if ($photos = $user->get('field_photos')) {
      $photos = $photos->referencedEntities();
      $list = [];

      foreach ($photos as $photo) {
        $imageUri = $photo->getFileUri();
        $originalUrl = $photo->url();
        $style = ImageStyle::load('user_profile_photos');
        $url = $style->buildUrl($imageUri);
        $list[] = [
          'url' => $url,
          'original' => $originalUrl
        ];
      }
      $app['photos'] = [
        '#theme' => 'profile-photos',
        '#photos' => $list
      ];
    }

    $term_fields = [
      'hard_skills' => 'Matching Hard Skills',
      'soft_skills' => 'Matching Soft Skills',
      'certifications' => 'Preferred Certifications',
    ];

    foreach ($term_fields as $term_field => $term_field_title) {
      // Skills / Certs.
      $skills = $this->getTerms('field_' . $term_field, $job_posting, $user);
      if (!empty($skills['skills'])) {
        $theme = $term_field == 'certifications' ?
          'skill_listing_certifications' : 'skill_listing';

        $app[$term_field] = [
          '#theme' => $theme,
          '#skills' => [
            'title' => $this->t($term_field_title),
            'list' => $skills['skills']
          ]
        ];
        if ($job_posting) {
          $app[$term_field]['#skills']['count'] = [
            'total' => $skills['total'],
            'matching' => $skills['matching']
          ];
        }
      }
    }

    if ($job_posting && !empty($app['certifications'])) {
      // Ger required certifications.
      $required_certifications = $this->getTermName(
        $job_posting->get('field_required_certifications')
        ->referencedEntities());

      $total_required = 0;
      $total_required_match = 0;
      $required_list = [];

      foreach($app['certifications']['#skills']['list'] as $index => $skill) {
        if (in_array($skill['title'], $required_certifications)) {
           $total_required++;
           $required_list[] = $app['certifications']['#skills']['list'][$index];
           unset($app['certifications']['#skills']['list'][$index]);
           if ($skill['match']) {
             $total_required_match++;
           }
        }
      }

      $app['certifications']['#skills']['count']['matching'] -= $total_required_match;
      $app['certifications']['#skills']['count']['total'] -= $total_required;

      if (!empty($required_list)) {
        $app['required_certifications'] = [
          '#theme' => 'skill_listing_certifications',
          '#skills' => [
            'title' => t('Required Certifications'),
            'list' => $required_list,
            'count' => [
              'total' => $total_required,
              'matching' => $total_required_match
            ]
          ],

        ];
      }
    }

    return [
      '#theme' => 'job_applicantion_owner_profile',
      '#app' => $app,
      '#attached' => [
        'library' => [
          'restorationjobs_applicants/owner_profile'
        ]
      ]
    ];
  }

  /**
   * Update job posting status.
   */
  function updateStatus(JobPostingInterface $job_posting, JobApplication $job_application, User $user, $status) {
    $current = $job_application->get('field_status')->getString();
    // Toggle favorite.
    $message = 'Job application updated.';

    switch ($status) {
      case 'favorite':
        $message = 'Favorited';

        if ($current == 'favorite') {
          $status = 'new';
          $message = 'Unfavorited.';
        }

        break;

      case 'declined':
        $message = 'Declined';

        if ($current == 'declined') {
          $status = 'new';
          $message = 'Undeclined.';
        }

        break;
    }

    $job_application->set('field_status', $status);
    $job_application->save();

    drupal_set_message($this->t($message));

    $response = new AjaxResponse();
    $url = Url::fromRoute('restorationjobs_applicants.job_application_owner_profile', [
      'job_posting' => $job_posting->id(),
      'user' => $user->id()
    ])->toString();

    $response->addCommand(new RedirectCommand($url));
    return $response;
  }

  /**
   * Custom access callback for Job Application owner profile page.
   */
  function access(JobPostingInterface $job_posting = NULL, User $user = NULL) {
    $uid = \Drupal::currentUser()->id();
    if ($job_posting) {
      $hasAccess = $uid == $job_posting->getOwnerId();
    } else {
      $hasAccess = !\Drupal::currentUser()->isAnonymous();
    }
    return $hasAccess ? AccessResult::allowed() : AccessResult::forbidden();
  }

  /**
   * Get term list.
   */
  function getTerms($field, $job_posting = NULL, $user) {
    $terms = [];
    $matching = 0;
    $total = 0;
    $certFiles = [];

    if (in_array($field, ['field_certifications'])) {
      $user_terms = [];
      $user_values = $user->get('field_certifications')->getValue();
      foreach ($user_values AS $user_value) {
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($user_value['description']);
        $user_terms[] = $term->getName(); // description = tid
        $certFiles[] = $user_value['target_id'];
      }
    } else {
      $user_terms = $this->getTermName($user->get($field)->referencedEntities());
    }

    if ($job_posting) {
      $job_terms = $this->getTermName($job_posting->get($field)->referencedEntities());
      $intersect = array_intersect($user_terms, $job_terms);
      foreach ($user_terms as $index => $user_term) {
        $match = in_array($user_term, $job_terms);
        $item = [
          'match' => $match,
          'title' => $user_term
        ];

        if ($field == 'field_certifications') {
          $item['file'] = $certFiles[$index];
        }

        $terms[] = $item;
      }
      return [
        'skills' => $terms,
        'matching' => count($intersect),
        'total' => count($job_terms),
      ];
    } else {
      foreach ($user_terms as $index => $user_term) {
        $terms[] = [
          'title' => $user_term,
        ];

        if (!empty($certFiles[$index])) {
          $item['file'] = $certFiles[$index];
        }
      }
      return [
        'skills' => $terms,
      ];
    }
  }

  /**
   * Get term name.
   */
  function getTermName($terms) {
    $names = [];

    foreach($terms as $term) {
      $names[] = $term->label();
    }

    return $names;
  }
}
