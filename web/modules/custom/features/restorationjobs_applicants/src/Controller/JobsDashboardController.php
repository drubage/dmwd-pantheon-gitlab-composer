<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\restorationjobs_applicants\Entity\SavedSearches;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;

/**
 * Class JobsDashboardController.
 */
class JobsDashboardController extends ControllerBase {
  /**
   * My Jobs.
   *
   * @return array Return Hello string.
   *   Return Hello string.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function content() {
    $user = \Drupal::currentUser();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $user->id()]);

    if ($company) {
      return $this->companyContent($user);
    }
    else {
      return $this->jobSeekerContent($user);
    }
  }

  private function companyContent($user) {
    $conn = Database::getConnection();
    $sql = "SELECT COUNT(DISTINCT job_posting_field_data.id)
FROM
{job_posting_field_data} job_posting_field_data
WHERE ((job_posting_field_data.user_id = :uid)) AND (job_posting_field_data.state LIKE 'pending')";
    $pending_count = $conn->query($sql, [':uid' => $user->id()])->fetchField();
    // If the user has NO pending jobs then direct to open tab
    if ($pending_count == 0) {
      return $this->redirect('view.my_jobs_dashboard.page_1');
    }

    return [
      '#theme' => 'my_jobs_owner',
      '#dashboard' => []
    ];
  }

  private function jobSeekerContent($user) {
    $recently_viewed_entities = [];
    // Getting entities that user have recently viewed
    $entities_ids = array_column(restorationjobs_companies_get_entities_viewed_by_user('job_posting', $user->id()), 'entity_id');
    $entities = \Drupal::entityTypeManager()->getStorage('job_posting')->loadMultiple($entities_ids);
    $recently_viewed_entities = \Drupal::entityTypeManager()->getViewBuilder('job_posting')->viewMultiple($entities, 'recently_viewed');


    // Watch jobs form.
    $formObject = \Drupal::entityTypeManager()
      ->getFormObject('saved_searches', 'default')
      ->setEntity(SavedSearches::create());
    $watchForm = \Drupal::formBuilder()->getForm($formObject);

    // Recent searches.
    $storage = \Drupal::service('user.private_tempstore')->get('recent_searches');
    $searches = $storage->get('list');

    return [
      '#theme' => 'seeker_dashboard',
      '#recently_viewed' => $recently_viewed_entities,
      '#recent_searches' => $searches,
      '#form' => $watchForm,
      '#attached' => [
        'library' => [
          'restorationjobs_applicants/custom'
        ],
      ],
    ];
  }
}
