<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\restorationjobs_applicants\Entity\SavedSearches;
use Drupal\Core\Url;

/**
 * Class NotificationsController.
 */
class NotificationsController extends ControllerBase {
  /**
   * Manage notifications.
   */
  public function content() {
    $user = \Drupal::currentUser();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $user->id()]);

    if ($company) {
      return $this->companyContent($user);
    }
    else {
      return $this->jobSeekerContent($user);
    }
  }

  /**
   * Company notifications.
   */
  private function companyContent() {
    return [
      '#theme' => 'manage_notifications',
      '#page' => [
        'type' => 'company',
      ]
    ];
  }

  /**
   * Job Seeker notifications.
   */
  private function jobSeekerContent() {
    // Watch jobs form.
    $formObject = \Drupal::entityTypeManager()
      ->getFormObject('saved_searches', 'default')
      ->setEntity(SavedSearches::create());
    $watchForm = \Drupal::formBuilder()->getForm($formObject);

    return [
      '#theme' => 'manage_notifications',
      '#page' => [
        'type' => 'jobSeeker',
        'form' => $watchForm,
      ],
      '#attached' => [
        'library' => [
          'restorationjobs_applicants/custom'
        ],
      ],
    ];
  }
}
