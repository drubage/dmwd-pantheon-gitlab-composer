<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface;

/**
 * Class SavedSearchesController.
 *
 *  Returns responses for Saved searches routes.
 */
class SavedSearchesController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Saved searches  revision.
   *
   * @param int $saved_searches_revision
   *   The Saved searches  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($saved_searches_revision) {
    $saved_searches = $this->entityManager()->getStorage('saved_searches')->loadRevision($saved_searches_revision);
    $view_builder = $this->entityManager()->getViewBuilder('saved_searches');

    return $view_builder->view($saved_searches);
  }

  /**
   * Page title callback for a Saved searches  revision.
   *
   * @param int $saved_searches_revision
   *   The Saved searches  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($saved_searches_revision) {
    $saved_searches = $this->entityManager()->getStorage('saved_searches')->loadRevision($saved_searches_revision);
    return $this->t('Revision of %title from %date', ['%title' => $saved_searches->label(), '%date' => format_date($saved_searches->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Saved searches .
   *
   * @param \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface $saved_searches
   *   A Saved searches  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(SavedSearchesInterface $saved_searches) {
    $account = $this->currentUser();
    $langcode = $saved_searches->language()->getId();
    $langname = $saved_searches->language()->getName();
    $languages = $saved_searches->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $saved_searches_storage = $this->entityManager()->getStorage('saved_searches');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $saved_searches->label()]) : $this->t('Revisions for %title', ['%title' => $saved_searches->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all saved searches revisions") || $account->hasPermission('administer saved searches entities')));
    $delete_permission = (($account->hasPermission("delete all saved searches revisions") || $account->hasPermission('administer saved searches entities')));

    $rows = [];

    $vids = $saved_searches_storage->revisionIds($saved_searches);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\restorationjobs_applicants\SavedSearchesInterface $revision */
      $revision = $saved_searches_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $saved_searches->getRevisionId()) {
          $link = $this->l($date, new Url('entity.saved_searches.revision', ['saved_searches' => $saved_searches->id(), 'saved_searches_revision' => $vid]));
        }
        else {
          $link = $saved_searches->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.saved_searches.translation_revert', ['saved_searches' => $saved_searches->id(), 'saved_searches_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.saved_searches.revision_revert', ['saved_searches' => $saved_searches->id(), 'saved_searches_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.saved_searches.revision_delete', ['saved_searches' => $saved_searches->id(), 'saved_searches_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['saved_searches_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
