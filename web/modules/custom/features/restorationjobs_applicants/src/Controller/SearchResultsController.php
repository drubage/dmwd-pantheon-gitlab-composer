<?php

namespace Drupal\restorationjobs_applicants\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class SearchResultsController.
 */
class SearchResultsController extends ControllerBase
{

  /**
   * Search Results.
   *
   * @return string
   *   Return Hello string.
   */
  public function content($title = NULL, $location = NULL)
  {
    $render = [];
    try {
      $providers = ['googlemaps'];
      // Here is address submited by user
      $addressCollection = \Drupal::service('geocoder')->geocode($location, $providers);
      $lat = $addressCollection->first()->getLatitude();
      $lon = $addressCollection->first()->getLongitude();
      // Loading search form
      $form = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_applicants\Form\JobSearchForm');

      // Searching entities;
      $query = \Drupal::entityQuery("group")
        ->addTag("location_determine")
        ->addMetaData("location_latitude", $lat)
        ->addMetaData('location_longitude', $lon)
        ->addMetaData('location_distance', 50);
      $results = $query->execute();
      if (count($results) > 0) {
        // Getting job posting by companies
        $query_posts = \Drupal::entityQuery("job_posting")
          ->condition("field_company", $results, "IN")
          ->condition("field_job_title", $title, '=')
          ->execute();
        $entities = \Drupal::entityTypeManager()->getStorage("job_posting")->loadMultiple($query_posts);
        $user = \Drupal::currentUser();
        $render = restorationjobs_applicants_job_posting_render_array($entities, $user->id());
      }
    } catch (\Exception $ex) {
      $render = [];
    }
    return [
      '#theme' => 'search_results',
      '#job_entities' => $render,
      '#attached' => array(
        'library' => array(
          'restorationjobs_applicants/custom'
        ),
      )
    ];
  }


}
