<?php

namespace Drupal\restorationjobs_applicants\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\UserInterface;
use Drupal\restorationjobs_companies\Controller\Notifications;

/**
 * Defines the Job application entity.
 *
 * @ingroup restorationjobs_applicants
 *
 * @ContentEntityType(
 *   id = "job_application",
 *   label = @Translation("Job application"),
 *   handlers = {
 *     "storage" = "Drupal\restorationjobs_applicants\JobApplicationStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\restorationjobs_applicants\JobApplicationListBuilder",
 *     "views_data" = "Drupal\restorationjobs_applicants\Entity\JobApplicationViewsData",
 *     "translation" = "Drupal\restorationjobs_applicants\JobApplicationTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\restorationjobs_applicants\Form\JobApplicationForm",
 *       "add" = "Drupal\restorationjobs_applicants\Form\JobApplicationForm",
 *       "edit" = "Drupal\restorationjobs_applicants\Form\JobApplicationForm",
 *       "delete" = "Drupal\restorationjobs_applicants\Form\JobApplicationDeleteForm",
 *     },
 *     "access" = "Drupal\restorationjobs_applicants\JobApplicationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\restorationjobs_applicants\JobApplicationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "job_application",
 *   data_table = "job_application_field_data",
 *   revision_table = "job_application_revision",
 *   revision_data_table = "job_application_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer job application entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/job_application/{job_application}",
 *     "add-form" = "/job/{job_posting}/apply-form",
 *     "edit-form" = "/application/{job_application}/edit",
 *     "delete-form" = "/admin/structure/job_application/{job_application}/delete",
 *     "version-history" = "/admin/structure/job_application/{job_application}/revisions",
 *     "revision" = "/admin/structure/job_application/{job_application}/revisions/{job_application_revision}/view",
 *     "revision_revert" = "/admin/structure/job_application/{job_application}/revisions/{job_application_revision}/revert",
 *     "revision_delete" = "/admin/structure/job_application/{job_application}/revisions/{job_application_revision}/delete",
 *     "translation_revert" = "/admin/structure/job_application/{job_application}/revisions/{job_application_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/job_application",
 *   },
 *   field_ui_base_route = "job_application.settings"
 * )
 */
class JobApplication extends RevisionableContentEntityBase implements JobApplicationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if (!$update) {
      $options = [
        'applicant' => $this->getOwnerId(),
        'application' => $this->id(),
      ];
      Notifications::sendNotification($this->getJobPosting(), 'field_notifications_applicants', $options);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the job_application owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
    // todo make this more specific
    if (!$this->getName()) {
      $this->setName('Job Application');
    }
    $this->setNewScore();
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobPosting() {
    return $this->get('field_job_posting')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewScore() {
    $score = $this->getNewScore();
    if (isset($score)) {
      $this->set('field_applicant_score', $score);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNewScore() {
    $job_posting = $this->getJobPosting();
    if ($job_posting->get('state')->value == 'active') {
      $applicant = $this->getOwner();
      $points = 0;
      $points_available = 100;
      $fields_to_check = array(
        'field_certifications' => 50,
        'field_hard_skills' => 30,
        'field_soft_skills' => 20,
      );
      foreach ($fields_to_check AS $field => $max_points) {
        $applicant_tids = $job_posting_tids = [];
        $applicant_values = $applicant->get($field)->getValue();
        $job_posting_values = $job_posting->get($field)->getValue();
        if (!empty($job_posting_values)) {
          // if field empty for user, then user gets no points for field
          // if not empty, compare fields and calculate percentage * max_points
          $this_points = 0;
          if (!empty($applicant_values)) {
            foreach ($applicant_values AS $applicant_value) {
              if ($field == 'field_certifications') {
                $applicant_tids[] = $applicant_value['description'];
              } else {
                $applicant_tids[] = $applicant_value['target_id'];
              }
            }
            foreach ($job_posting_values AS $job_posting_value) {
              $job_posting_tids[] = $job_posting_value['target_id'];
            }
            if ($field == 'field_certifications') {
              $percentage = 1;
              $required_certifications_tids = [];
              $required_certifications_values = $job_posting->get('field_required_certifications')->getValue();

              // we handle field_certifications a little differently if we have required certifications
              foreach ($required_certifications_values AS $required_certifications_value) {
                $required_certifications_tids[] = $required_certifications_value['target_id'];
              }

              // remove required tids from the field_certifications values so they are distinct arrays
              $non_required_certification_tids = array_diff($job_posting_tids, $required_certifications_tids);

              if (!empty($required_certifications_tids) && !empty($non_required_certification_tids)) {
                $intersect = count(array_intersect($applicant_tids, $required_certifications_tids));
                $percentage = ($intersect / count($required_certifications_tids)) * .6; // required certs weighted at 60%
                $intersect = count(array_intersect($applicant_tids, $non_required_certification_tids));
                $percentage += ($intersect / count($non_required_certification_tids)) * .4; // non-required certs weighted at 40%
              } elseif (!empty($required_certifications_tids) && empty($non_required_certification_tids)) {
                $intersect = count(array_intersect($applicant_tids, $required_certifications_tids));
                $percentage = ($intersect / count($required_certifications_tids)); // required certs weighted at 100% if no non-required certs
              } else {
                $intersect = count(array_intersect($applicant_tids, $non_required_certification_tids));
                $percentage = ($intersect / count($non_required_certification_tids)); // non-required certs weighted at 100% if no certs required
              }
              $this_points = $percentage * $max_points;
            } else {
              $intersect = count(array_intersect($applicant_tids, $job_posting_tids));
              $this_points = ($intersect / count($job_posting_tids)) * $max_points;
            }
            $points += $this_points;
          }
        } else {
          $points_available -= $max_points; // if field blank for job posting, user gets max points
        }
      }
      return round(($points / $points_available) * 100);
    }
    return FALSE; // if job posting is not active
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Job application entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Job application entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Job application is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
