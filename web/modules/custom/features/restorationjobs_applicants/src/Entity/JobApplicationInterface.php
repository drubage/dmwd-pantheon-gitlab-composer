<?php

namespace Drupal\restorationjobs_applicants\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Job application entities.
 *
 * @ingroup restorationjobs_applicants
 */
interface JobApplicationInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Job application name.
   *
   * @return string
   *   Name of the Job application.
   */
  public function getName();

  /**
   * Sets the Job application name.
   *
   * @param string $name
   *   The Job application name.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface
   *   The called Job application entity.
   */
  public function setName($name);

  /**
   * Gets the Job application creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Job application.
   */
  public function getCreatedTime();

  /**
   * Sets the Job application creation timestamp.
   *
   * @param int $timestamp
   *   The Job application creation timestamp.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface
   *   The called Job application entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Job application published status indicator.
   *
   * Unpublished Job application are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Job application is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Job application.
   *
   * @param bool $published
   *   TRUE to set this Job application to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface
   *   The called Job application entity.
   */
  public function setPublished($published);

  /**
   * Gets the Job application revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Job application revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface
   *   The called Job application entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Job application revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Job application revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface
   *   The called Job application entity.
   */
  public function setRevisionUserId($uid);

}
