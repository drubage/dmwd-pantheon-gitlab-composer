<?php

namespace Drupal\restorationjobs_applicants\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Saved searches entities.
 *
 * @ingroup restorationjobs_applicants
 */
interface SavedSearchesInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Saved searches name.
   *
   * @return string
   *   Name of the Saved searches.
   */
  public function getName();

  /**
   * Sets the Saved searches name.
   *
   * @param string $name
   *   The Saved searches name.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface
   *   The called Saved searches entity.
   */
  public function setName($name);

  /**
   * Gets the Saved searches creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Saved searches.
   */
  public function getCreatedTime();

  /**
   * Sets the Saved searches creation timestamp.
   *
   * @param int $timestamp
   *   The Saved searches creation timestamp.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface
   *   The called Saved searches entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Saved searches published status indicator.
   *
   * Unpublished Saved searches are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Saved searches is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Saved searches.
   *
   * @param bool $published
   *   TRUE to set this Saved searches to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface
   *   The called Saved searches entity.
   */
  public function setPublished($published);

  /**
   * Gets the Saved searches revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Saved searches revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface
   *   The called Saved searches entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Saved searches revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Saved searches revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface
   *   The called Saved searches entity.
   */
  public function setRevisionUserId($uid);

}
