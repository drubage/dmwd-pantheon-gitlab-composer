<?php

namespace Drupal\restorationjobs_applicants\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class ConfirmIdentityJobApplyForm.
 */
class ConfirmIdentityJobApplyForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_identity_job_apply_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $parameters = \Drupal::routeMatch()->getParameters();
    $job_posting = $parameters->get('job_posting');

    $form_state->set('job_posting', $job_posting);
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Enter your primary email so that we can confirm your identity.'),
      '#required' => TRUE
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm Email'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $mail = trim($form_state->getValue('mail'));

    if (!\Drupal::service('email.validator')->isValid($mail)) {
      $form_state->setErrorByName('mail', t('Please enter a valid email address.'));
    } else {
      if (user_load_by_mail($mail)) {
        $form_state->setErrorByName('mail', t('This email address is already registered.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $params = [
      'name' => trim($form_state->getValue('mail')),
      'mail' => trim($form_state->getValue('mail')),
    ];
    $storage = $form_state->getStorage();
    $job_posting = $storage['job_posting'];

    $account = User::create($params);
    $account->enforceIsNew();
    $account->activate();
    $account->set('field_job_to_apply', $job_posting->id());
    $messenger = \Drupal::messenger();

    if ($account->save()) {
      $messenger->addMessage("User account created.");
      user_login_finalize($account);
      _user_mail_notify('register_no_approval_required', $account);

      $form_state->setRedirect('restorationjobs_users.user_validate_email', [], [
        'query' => [
          'email' => $form_state->getValue('mail'),
          'job' => $job_posting->id(),
        ]
      ]);
    }
  }

}
