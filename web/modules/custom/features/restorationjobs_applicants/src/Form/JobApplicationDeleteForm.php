<?php

namespace Drupal\restorationjobs_applicants\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Job application entities.
 *
 * @ingroup restorationjobs_applicants
 */
class JobApplicationDeleteForm extends ContentEntityDeleteForm {


}
