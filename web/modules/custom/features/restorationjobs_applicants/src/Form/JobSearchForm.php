<?php

namespace Drupal\restorationjobs_applicants\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SearchForm.
 */
class JobSearchForm extends FormBase
{


  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'job_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $terms = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree('job_titles');
    $jobs_options = [null => $this->t("Title")];
    foreach ($terms as $term) {
      $jobs_options[$term->tid] = $term->name;
    }
    $form['title'] = [
      '#type' => 'select',
      '#options' => $jobs_options
    ];
    $form['location'] = [
      '#type' => 'textfield',
      '#attributes' => ['placeholder' => $this->t("Location"), 'id' => 'location_autocomplete']
    ];
    $form['save_results'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify me about similar positions'),
      '#default_value' => FALSE
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Find a job'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $user = \Drupal::currentUser();
    $values = $form_state->getValues();
    $notify_save = $values['save_results'];
    // Saving search
    $saved_search = \Drupal::entityTypeManager()
      ->getStorage('saved_searches')
      ->create([
        'name' => 'User: '. $user->id().' Search: '.$values['location'],
        'field_job_location' => $values['location'],
        'field_job_title' => $values['title'],
        //'field_notifications' => $notify_save,
        'uid' => $user->id(),
      ])->save();
    $form_state->setRedirect("restorationjobs_applicants.search_results_controller_content", [
      'title' => $values['title'],
      'location' => $values['location']
    ]);

  }

}
