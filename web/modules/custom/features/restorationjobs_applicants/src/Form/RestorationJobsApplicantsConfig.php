<?php

namespace Drupal\restorationjobs_applicants\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RestorationJobsApplicantsConfig.
 */
class RestorationJobsApplicantsConfig extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'restoration_jobs_applicants_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('restorationjobs_applicants.settings');
    // URL to consume profile.
    $form['google_api_key'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('API Key from google project:'),
        '#default_value' => $config->get('restorationjobs_applicants.google_api_key'),
        '#required' => TRUE
    );

    $restricted_mode = $config->get('restorationjobs_applicants.restricted_mode') ?: 0;
    $form['restricted_mode'] = array(
        '#type' => 'radios',
        '#title' => $this->t('Restricted mode'),
        '#options' => [
          0 => t('No'),
          1 => t('Yes')
        ],
        '#default_value' => $restricted_mode,
        '#required' => TRUE
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('restorationjobs_applicants.settings');
    $config->set('restorationjobs_applicants.google_api_key', $form_state->getValue('google_api_key'));

    $restricted = $form_state->getValue('restricted_mode');
    $default_value = $config->get('restorationjobs_applicants.restricted_mode');
    if ($default_value != $restricted) {
      $config->set('restorationjobs_applicants.restricted_mode', $restricted);
      $config->save();
      drupal_flush_all_caches();
    }
    else {
      $config->save();
    }

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
      return [
          'restorationjobs_applicants.settings',
      ];
  }

}
