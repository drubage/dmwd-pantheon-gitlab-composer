<?php

namespace Drupal\restorationjobs_applicants\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Saved searches entities.
 *
 * @ingroup restorationjobs_applicants
 */
class SavedSearchesDeleteForm extends ContentEntityDeleteForm {


}
