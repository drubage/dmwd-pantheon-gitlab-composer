<?php

namespace Drupal\restorationjobs_applicants\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\restorationjobs_companies\CompanyHelper;

/**
 * Form controller for Saved searches edit forms.
 *
 * @ingroup restorationjobs_applicants
 */
class SavedSearchesForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\restorationjobs_applicants\Entity\SavedSearches */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $options = [
      '_none' => $this->t('Title')
    ];

    $options = array_merge($options, CompanyHelper::getJobTitles(TRUE));
    $form['field_job_title']['widget']['#options'] = $options;

    $form['name']['widget'][0]['value']['#default_value'] = 'Saved Search';
    $form['actions']['submit']['#value'] = $this->t('ADD NEW EMAIL ALERT');
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(REQUEST_TIME);
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);
  }

}
