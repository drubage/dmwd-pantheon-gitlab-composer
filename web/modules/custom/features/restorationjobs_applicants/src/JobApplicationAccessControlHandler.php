<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Job application entity.
 *
 * @see \Drupal\restorationjobs_applicants\Entity\JobApplication.
 */
class JobApplicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished job application entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published job application entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit job application entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete job application entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job application entities');
  }

}
