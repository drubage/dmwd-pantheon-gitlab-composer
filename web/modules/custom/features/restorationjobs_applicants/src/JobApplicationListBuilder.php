<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Job application entities.
 *
 * @ingroup restorationjobs_applicants
 */
class JobApplicationListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Job application ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\restorationjobs_applicants\Entity\JobApplication */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.job_application.edit_form',
      ['job_application' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
