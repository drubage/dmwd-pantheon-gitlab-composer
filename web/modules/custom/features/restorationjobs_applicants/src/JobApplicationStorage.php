<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\restorationjobs_applicants\Entity\JobApplicationInterface;

/**
 * Defines the storage handler class for Job application entities.
 *
 * This extends the base storage class, adding required special handling for
 * Job application entities.
 *
 * @ingroup restorationjobs_applicants
 */
class JobApplicationStorage extends SqlContentEntityStorage implements JobApplicationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(JobApplicationInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {job_application_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {job_application_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(JobApplicationInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {job_application_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('job_application_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
