<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\restorationjobs_applicants\Entity\JobApplicationInterface;

/**
 * Defines the storage handler class for Job application entities.
 *
 * This extends the base storage class, adding required special handling for
 * Job application entities.
 *
 * @ingroup restorationjobs_applicants
 */
interface JobApplicationStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Job application revision IDs for a specific Job application.
   *
   * @param \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface $entity
   *   The Job application entity.
   *
   * @return int[]
   *   Job application revision IDs (in ascending order).
   */
  public function revisionIds(JobApplicationInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Job application author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Job application revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\restorationjobs_applicants\Entity\JobApplicationInterface $entity
   *   The Job application entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(JobApplicationInterface $entity);

  /**
   * Unsets the language for all Job application with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
