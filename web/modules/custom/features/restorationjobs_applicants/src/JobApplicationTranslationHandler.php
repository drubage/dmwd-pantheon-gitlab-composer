<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for job_application.
 */
class JobApplicationTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
