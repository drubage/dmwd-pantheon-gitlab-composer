<?php

namespace Drupal\restorationjobs_applicants\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a 'JobApplicationListContentFilter' block.
 *
 * @Block(
 *  id = "job_application_list_content_filter_block",
 *  admin_label = @Translation("Job application list Content filter"),
 * )
 */
class JobApplicationListContentFilter extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $exposed = \Drupal::request()->query->all();

    $fields = [
      'field_certifications_target_id' => [],
      'field_hard_skills_target_id' => [],
      'field_soft_skills_target_id' => [],
      'combine' => '',
      'sort_by' => 'field_applicant_score_value',
      'sort_order' => 'DESC',
    ];
    foreach ($fields AS $field => $default_value) {
      if (!isset($exposed[$field])) {
        $exposed[$field] = $default_value;
      }
    }
    return [
      '#theme' => 'job_application_list_content_filter',
      '#filters' => $exposed,
      '#cache' => [
        'max-age' => 0,
      ],
      '#attached' => [
        'library' => [
          'restorationjobs_applicants/restorationjobs_applicants_applicants_list'
        ],
        'drupalSettings'=> [
          'job_search' => $exposed
        ],
      ]
    ];
  }

}
