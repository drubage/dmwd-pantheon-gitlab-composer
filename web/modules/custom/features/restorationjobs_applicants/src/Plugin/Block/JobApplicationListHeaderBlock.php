<?php

namespace Drupal\restorationjobs_applicants\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\views\Views;

/**
 * Provides a 'JobApplicationListHeaderBlock' block.
 *
 * @Block(
 *  id = "job_application_list_header_block",
 *  admin_label = @Translation("Job application list header block"),
 * )
 */
class JobApplicationListHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = Url::fromRoute('<current>')->toString();
    $jobPostingId = \Drupal::routeMatch()->getParameter('arg_0');

    if (!$jobPostingId) {
      return [];
    }

    $views = [
      'page_1' => [
        'type' => 'new',
        'tab' => 'New Candidates',
      ],
      'page_2' => [
        'type' => 'favorite',
        'tab' => 'Favorites',
      ],
      'page_3' => [
        'type' => 'decline',
        'tab' => 'Declined',
      ],
    ];
    $exposed = \Drupal::request()->query->all();

    $tabs = [];
    foreach ($views as $display => $page) {
      $params = ['arg_0' => $jobPostingId];
      $view = Views::getView('job_applicants');
      $view->setArguments($params);
      if (!empty($exposed)) {
        $view->setExposedInput($exposed);
      }
      $view->get_total_rows = TRUE;
      $view->execute($display);
      $rows = $view->total_rows;
      $link = Url::fromRoute('view.job_applicants.' . $display, $params)->toString();
      $query_string = \Drupal::request()->query->all();
      unset($query_string['page']);
      $params += $query_string;
      $link_with_filters = Url::fromRoute('view.job_applicants.' . $display, $params);
      $tabs[$page['type']] = [
        'type' => $page['type'],
        'link' => $link_with_filters,
        'text' => $this->t($page['tab']) . ' (' . $rows . ')',
        'active' => $url == $link ? 'active' : ''
      ];
    }

    $job = \Drupal::entityTypeManager()
      ->getStorage('job_posting')
      ->load($jobPostingId);

    $title = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->load($job->get('field_job_title')->getString())
      ->getName();

    $myJobsUrl = Url::fromRoute('restorationjobs_applicants.homepage');

    return [
      '#theme' => 'job_application_list_header',
      '#header' => [
        'title' => $title,
        'tabs' => $tabs,
        'jib' => $jobPostingId,
        'filters' => $this->getFilters($job, $exposed),
        'myJobs' => $myJobsUrl
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Get filters.
   */
  function getFilters($job, $exposed) {
    $filters = [
      'certifications' => [],
      'skills' => []
    ];

    $fields = [
      [
        'name' => 'field_certifications',
        'type' => 'certifications',
      ],
      [
        'name' => 'field_hard_skills',
        'type' => 'skills',
      ],
      [
        'name' => 'field_soft_skills',
        'type' => 'skills',
      ],
    ];
    $hasFilter = FALSE;
    foreach($fields AS $field) {
      $machine_name = $field['name'] . '_target_id';
      if ($job->get($field['name'])) {
        $hasFilter = TRUE;
        $terms = $job->get($field['name'])->referencedEntities();
        foreach($terms AS $term) {
          $term_id = $term->id();
          $active = (!empty($exposed[$machine_name]) && in_array($term_id, $exposed[$machine_name]));
          $filters[$field['type']][] = [
            'name' => $term->getName(),
            'id' => $term_id,
            'vocabulary' => $term->getVocabularyId(),
            'active' => $active,
            'filter_name' => $machine_name,
          ];
        }
      }
    }
    return $hasFilter ? $filters : FALSE;
  }

}
