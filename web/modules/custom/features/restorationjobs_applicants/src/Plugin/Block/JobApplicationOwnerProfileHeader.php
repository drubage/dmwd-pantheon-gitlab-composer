<?php

namespace Drupal\restorationjobs_applicants\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'JobApplicationOwnerProfileHeader' block.
 *
 * @Block(
 *  id = "job_application_owner_profile_header",
 *  admin_label = @Translation("Job application owner header block"),
 * )
 */
class JobApplicationOwnerProfileHeader extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $myJobsUrl = Url::fromRoute('restorationjobs_applicants.homepage');
    $jobPosting = \Drupal::routeMatch()->getParameter('job_posting');
    $user = \Drupal::routeMatch()->getParameter('user');
    $name = $user->get('field_first_name')->getString() . ' ' . $user->get('field_last_name')->getString();

    $job_application = \Drupal::entityManager()->getStorage('job_application')
      ->loadByproperties([
        'field_job_posting' => $jobPosting->id(),
        'user_id' => $user->id()
      ]);
    $job_application = reset($job_application);
    $status = $job_application->get('field_status')->getString();
    $status_arg = ($status == 'favorite' ? 'favorites' : $status);
    $url = '/job/' . $jobPosting->id() . '/applicants/' . $status_arg;

    $title = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->load($jobPosting->get('field_job_title')->getString())
      ->getName();



    return [
      '#theme' => 'job_application_owner_header',
      '#header' => [
        'myJobs' => $myJobsUrl,
        'jobTitle' => $title,
        'url' => $url,
        'name' => $name
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $job_posting = \Drupal::routeMatch()->getParameter('job_posting');
    return $job_posting ? AccessResult::allowed() : AccessResult::forbidden();
  }
}
