<?php

namespace Drupal\restorationjobs_applicants\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;


/**
 * Provides a 'JobApplyHeaderBlock' block.
 *
 * @Block(
 *  id = "job_apply_header_block",
 *  admin_label = @Translation("Job apply header block"),
 * )
 */
class JobApplyHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $parameters = \Drupal::routeMatch()->getParameters();
    $job_posting = $parameters->get('job_posting');
    $build = entity_view($job_posting, 'job_apply_header');
    return [
      '#type' => 'markup',
      '#markup' => drupal_render($build),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $job_posting = \Drupal::routeMatch()->getParameter('job_posting');
    return $job_posting ? AccessResult::allowed() : AccessResult::forbidden();
  }
}
