<?php

namespace Drupal\restorationjobs_applicants\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Database;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Provides a 'MyJobsSeekerHeader' block.
 *
 * @Block(
 *  id = "my_jobs_seeker_header",
 *  admin_label = @Translation("My Jobs Seeker Header"),
 * )
 */
class MyJobsSeekerHeader extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()
      ->getForm(\Drupal\restorationjobs_companies\Form\FindAJobForm::class);

    $form['title']['#options'][''] = t('Title');
    $form['location']['#description'] = NULL;


    $uid = \Drupal::currentUser()->id();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $uid]);
    $tabs = [];

    if ($company) {
      $url = Url::fromRoute('<current>')->toString();
      $conn = Database::getConnection();
      $sql = "SELECT COUNT(DISTINCT job_posting_field_data.id)
FROM
{job_posting_field_data} job_posting_field_data
WHERE ((job_posting_field_data.user_id = :uid)) AND (job_posting_field_data.state IN (:state[]))";
      $pending_count = $conn->query($sql, [':uid' => $uid, ':state[]' => ['pending']])->fetchField();
      $open_count = $conn->query($sql, [':uid' => $uid, ':state[]' => ['active']])->fetchField();
      $closed_count = $conn->query($sql, [':uid' => $uid, ':state[]' => ['expired', 'rejected']])->fetchField();
      $default_url = Url::fromRoute('restorationjobs_applicants.homepage');
      $links = [
        'pending' =>  Url::fromRoute('view.my_jobs_dashboard.page_3'),
        'open' =>  Url::fromRoute('view.my_jobs_dashboard.page_1'),
        'closed' =>  Url::fromRoute('view.my_jobs_dashboard.page_2'),
      ];

      $tabs = [
        'pending' => [
          'type' => 'pending',
          'link' => $links['pending'],
          'text' => $this->t('PENDING JOBS') . " ({$pending_count})",
          'class' => 'col-sm-4',
          'active' => $url == $links['pending']->toString() || $url == $default_url->toString() ? 'active' : ''
        ],
        'open' => [
          'type' => 'open',
          'link' => $links['open'],
          'text' => $this->t('OPEN JOBS') . " ({$open_count})",
          'class' => 'col-sm-4',
          'active' => $url == $links['open']->toString() ? 'active' : ''
        ],
        'closed' => [
          'type' => 'closed',
          'link' => $links['closed'],
          'text' => $this->t('CLOSED JOBS') . " ({$closed_count})",
          'class' => 'col-sm-4',
          'active' => $url == $links['closed']->toString() ? 'active' : ''
        ]
      ];
    }

    $config = \Drupal::config('restorationjobs_applicants.settings');
    $isRestricted = $config->get('restorationjobs_applicants.restricted_mode');

    return [
      '#theme' => 'my_jobs_seeker_header_block',
      '#header' => [
        'form' => $form,
        'tabs' => $tabs,
        'restricted' => $isRestricted
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
