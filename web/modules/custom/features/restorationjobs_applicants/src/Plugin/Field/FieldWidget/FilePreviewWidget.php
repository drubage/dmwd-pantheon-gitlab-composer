<?php

namespace Drupal\restorationjobs_applicants\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\webform\Element\WebformToggle;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Plugin implementation of the 'file_preview_widget' widget.
 *
 * @FieldWidget(
 *   id = "file_preview_widget",
 *   label = @Translation("File preview widget"),
 *   field_types = {
 *     "file",
 *     "image"
 *   }
 * )
 */
class FilePreviewWidget extends FileWidget {
  /**
   * Overrides
   * \Drupal\file\Plugin\Field\FieldWidget\FileWidget::formMultipleElements().
   *
   * Special handling for draggable multiple widgets and 'add more' button.
   *
   * @param FieldItemListInterface $items
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);
    $field_settings = $this->getFieldSettings();

    // Determine the number of widgets to display.
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    if ($cardinality == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      $elements['#theme'] = 'file_preview_widget';
      $elements['#settings'] = $field_settings;
      $elements['#attached']['library'][] = 'restorationjobs_applicants/filepreview';
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Determine the number of widgets to display.
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    if ($cardinality != FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      $element['#theme'] = 'file_preview_widget';
      $element['#attached']['library'][] = 'restorationjobs_applicants/filepreview';
    }

    return $element;
  }

}
