<?php
/**
 * @file
 * Contains \Drupal\restorationjobs_applicants\Plugin\QueueWorker\SavedSearchesQueue.
 */
namespace Drupal\restorationjobs_applicants\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\restorationjobs_applicants\SavedSearchNotifications;

/**
 * @QueueWorker(
 *   id = "saved_searches_queue",
 *   title = @Translation("Send saved search alerts."),
 *   cron = {"time" = 60}
 * )
 */
class SavedSearchesQueue extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $notifyController = new SavedSearchNotifications();
    $notifyController->notify($data->body, $data->to);
  }
}
