<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\views\Views;
use Drupal\core\Url;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;

/**
 * class SavedSearchNotifications.
 *
 * Send notifications os job posting creation for matching
 * saved searches.
 */
class SavedSearchNotifications {
  /**
   * Send notifications on job creation.
   */
  public function sendNotififications(JobPostingInterface $job) {
    $titleId = $job->get('field_job_title')->first()->getString();
    $geocode = $job->get('field_latitude_longitude')->first();
    $latitude = $geocode->get('lat')->getString();
    $longitude = $geocode->get('lon')->getString();

    $ranges = [
      100,
      75,
      50,
      25
    ];

    $users = [];
    foreach($ranges as $range) {
      $view = Views::getView('notifications_saved_searches');
      $view->setExposedInput([
        'field_job_title_target_id' => $titleId,
        'field_latitude_and_longitude_proximity' => [
          'value' => $range,
          'source_configuration' => [
            'origin' => [
              'lat' => $latitude,
              'lon' => $longitude,
            ]
          ]
        ]
      ]);

      $result = $view->execute();

      foreach ($view->result as $key => $value) {
        $owner = $value->_entity->getOwner();
        $users[$owner->id()] = [
          'range' => $range,
          'email' => $owner->getEmail()
        ];
      }
    }

    foreach($users as $user) {
      $this->addToQueue($job, $user['email']);
    }
  }

  /**
   * Add notification to queue.
   */
  function addToQueue(JobPostingInterface $job_posting, $to) {
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('saved_searches_queue');
    $queue->createQueue();

    $jobUrl = $job_posting->toUrl('canonical', ['absolute' => TRUE]);
    $titleTerm = $job_posting->get('field_job_title')->getString();
    $title = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($titleTerm)->getName();
    $location = $job_posting->get('field_location')->first()->getValue();
    $company = \Drupal::entityTypeManager()->getStorage('group')->load($job_posting->get('field_company')->getString());

    $job = [
      'title' => $title,
      'location' => $location,
      'url' => $jobUrl,
      'company' => $company->label()
    ];

    $notificationsUrl = Url::fromRoute('restorationjobs_applicants.notifications')->toString();

    $theme = [
      '#theme' => 'notification_applicant_alert',
      '#email' => [
        'notificationsUrl' => $notificationsUrl,
        'job' => $job
      ]
    ];

    $body = \Drupal::service('renderer')->render($theme);

    $item = new \stdClass();
    $item->body = $body;
    $item->to = $to;
    $queue->createItem($item);
  }

  /**
   * Send notification.
   */
  public function notify($body, $to) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $key = 'rj-applicant-alert';

    $params['body'] = $body;
    $params['subject'] = 'New Job Posting in Your Area';

    $mailManager->mail('restorationjobs_applicants', $key, $to, $langcode, $params, NULL, TRUE);
  }
}
