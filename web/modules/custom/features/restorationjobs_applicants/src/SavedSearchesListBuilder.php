<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Saved searches entities.
 *
 * @ingroup restorationjobs_applicants
 */
class SavedSearchesListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Saved searches ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\restorationjobs_applicants\Entity\SavedSearches */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.saved_searches.edit_form',
      ['saved_searches' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
