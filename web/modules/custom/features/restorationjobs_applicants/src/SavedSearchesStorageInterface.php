<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface;

/**
 * Defines the storage handler class for Saved searches entities.
 *
 * This extends the base storage class, adding required special handling for
 * Saved searches entities.
 *
 * @ingroup restorationjobs_applicants
 */
interface SavedSearchesStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Saved searches revision IDs for a specific Saved searches.
   *
   * @param \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface $entity
   *   The Saved searches entity.
   *
   * @return int[]
   *   Saved searches revision IDs (in ascending order).
   */
  public function revisionIds(SavedSearchesInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Saved searches author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Saved searches revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\restorationjobs_applicants\Entity\SavedSearchesInterface $entity
   *   The Saved searches entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(SavedSearchesInterface $entity);

  /**
   * Unsets the language for all Saved searches with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
