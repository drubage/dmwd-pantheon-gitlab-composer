<?php

namespace Drupal\restorationjobs_applicants;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for saved_searches.
 */
class SavedSearchesTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
