<?php

/**
 * @file
 * Contains job_posting.page.inc.
 *
 * Page callback for Job Posting entities.
 */

use Drupal\Core\Render\Element;
use Drupal\restorationjobs_companies\AccessChecks;
use Drupal\Core\Url;

/**
 * Prepares variables for Job Posting templates.
 *
 * Default template: job_posting.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_job_posting(array &$variables) {
  $job_posting = $variables['elements']['#job_posting'];
  $user = \Drupal::currentUser();
  $variables['job_posting'] = $job_posting;
  $variables['url'] = $job_posting->toUrl()->toString();
  $is_company = AccessChecks::hasAccessToCompany()->isAllowed();
  $is_owner = $job_posting->getOwnerId() === $user->Id();

  $card_style = 'default';
  $orig_view_mode = $variables['elements']['#view_mode'];

  // todo for recently_viewed show the timestamp it was viewed
  // todo for recently_applied show the timestamp it was applied

  // view modes and respective card style
  $teaser_view_modes = array(
    'job_apply_header' => 'header', // header style is transparent
    'job_edit_header' =>  'header',
    'teaser' => 'default',
    'recently_viewed' => 'default',
    'recently_applied' => 'default',
  );

  if (isset($teaser_view_modes[$orig_view_mode])) {
    $variables['elements']['#view_mode'] = 'teaser'; // set it back to teaser
    $variables['card_style'] = $teaser_view_modes[$orig_view_mode];
    $variables['ctas'] = [];
    $variables['#cache']['max-age'] = 0;

    $external_url = $job_posting->get('field_external_application_url')->uri;

    if ($is_company) {
      $variables['user_type'] = 'employer';
      $applicant_count = $job_posting->getApplicationsForJob(TRUE);
      if ($orig_view_mode == 'job_edit_header') {

        // Get licence remaining days.
        $payment_method = $job_posting->getPaymentMethod();
        $expires_at = $job_posting->getExpiration();

        if ($applicant_count > 0) {
          $variables['ctas'][] = array(
            'url' => $job_posting->toUrl()->toString() . '/applicants/new',
            'title' => 'Applicants',
            'class' => 'card-cta-btn-small btn btn-white-primary',
          );
        }
        if ($job_posting->isPublished()) {
          $variables['#attached']['library'][] = 'core/drupal.dialog.ajax';
          if ($expires_at) {
            $variables['meta'] = 'You will be billed in ' . $expires_at . ' days';
          }
          $variables['ctas'][] = [
            'url' => Url::fromRoute('restorationjobs_companies.job_edit',
              ['job_posting' => $job_posting->id()],
              ['query' => [\Drupal::destination()->getAsArray()],
              ]),
            'title' => 'Edit Post',
            'class' => 'card-cta-btn-small btn btn-white-primary',
          ];
          $variables['ctas'][] = array(
            'url' => '/ajax/job/' . $job_posting->id() . '/close',
            'title' => 'Close Post',
            'class' => 'use-ajax card-cta-btn-small btn btn-white-primary',
          );
        }
      } else {

        if ($external_url && !$is_owner) {
          $variables['ctas'][] = array(
            'title' => 'External Apply',
            'class' => 'card-cta-btn-grey external-apply',
          );
        } else {
          // Job view/apply.
          if ($orig_view_mode == 'job_apply_header') {
            // Hide on job view.
            if (\Drupal::routeMatch()->getRouteName() != 'entity.job_posting.canonical') {
              $variables['applied'] = FALSE;
              $variables['ctas'] = [[
                'url' => $job_posting->toUrl()->toString(),
                'title' => 'View',
                'class' => 'card-cta-btn-small btn btn-white-primary',
              ]];
            }
            else if ($is_owner) {
              $variables['ctas'] = [[
                'url' => Url::fromRoute('restorationjobs_companies.job_edit',
                  ['job_posting' => $job_posting->id()],
                  ['query' => [\Drupal::destination()->getAsArray()],
                ]),
                'title' => 'Edit',
                'class' => 'card-cta-btn-small btn btn-white-primary',
              ]];
            }
          }
          else if ($job_posting->getOwnerId() === $user->Id()) {
            // new applicants page
            if ($job_posting->get('state')->getString() == 'pending') {
              $variables['ctas'][] = array(
                'url' => FALSE,
                'title' => 'Pending Approval',
                'class' => 'card-cta-btn-default',
              );
            }
            else {
              $text = $applicant_count != 1 ? ' applicants' : ' applicant';
              $variables['ctas'][] = array(
                'url' => Url::fromRoute('view.job_applicants.page_1', ['arg_0' => $job_posting->id()]),
                'title' => $applicant_count . $text,
                'class' => 'card-cta-btn-default',
              );
            }
          }
          else {
            $variables['ctas'][] = array(
              'url' => $job_posting->toUrl(),
              'title' => t('VIEW JOB'),
              'class' => 'card-cta-btn-default',
            );
          }
        }
      }
    } else {
      $variables['user_type'] = 'seeker';
      $isJobView = \Drupal::routeMatch()->getRouteName() == 'entity.job_posting.canonical';

      $variables['applied'] = $job_posting->hasApplied();
      if ($orig_view_mode == 'job_apply_header') {
        if (!$isJobView) {
          $variables['ctas'][] = array(
            'url' => $job_posting->toUrl(),
            'title' => t('VIEW'),
            'class' => 'card-cta-btn-small btn btn-white-primary',
          );
          return;
        }
        elseif ($variables['applied']) {
          $variables['#attached']['library'][] = 'core/drupal.dialog.ajax';
          // Allow users to withdraw applications.
          $variables['applied'] = FALSE;
          $variables['ctas'][] = array(
            'url' => Url::fromRoute('restorationjobs_applicants.withdraw_modal', [
              'job_posting' => $job_posting->id()
            ]),
            'title' => t('Withdraw Application'),
            'class' => 'use-ajax card-cta-btn-small btn btn-white-primary',
          );
          return;
        }
      }

      if ($external_url) {
        $class = \Drupal::routeMatch()->getRouteName() ===
          "entity.job_posting.canonical" ?
          'card-cta-btn-small btn btn-white-primary' :
          'card-cta-btn-grey external-apply';

        $variables['ctas'][] = array(
          'url' => $external_url,
          'title' => 'External Apply',
          'class' => $class,
          'target' => '_blank',
        );
      } else {

        $lightning_class = 'card-cta-btn-lightning-apply';
        if ($orig_view_mode == 'job_apply_header') {
          $lightning_class = 'card-cta-btn-lightning-apply-small card-cta-btn-small btn btn-white-primary';
        }


        $route_name = \Drupal::routeMatch()->getRouteName();
        if ($route_name == 'entity.job_posting.canonical') {
          $variables['ctas'][] = [
            'url' => Url::fromRoute('restorationjobs_applicants.job_application_flow_controller_identity', ['job_posting' => $job_posting->id()]),
            'title' => 'Lightning Apply',
            'class' => $lightning_class,
          ];
        } else {
          $variables['ctas'][] = [
            'url' => Url::fromRoute('entity.job_posting.canonical', ['job_posting' => $job_posting->id()]),
            'title' => 'Lightning Apply',
            'class' => $lightning_class,
          ];
        }
      }
    }
  }

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Randomise skills and certifications.
  $shuffle_fields = [
    [
      'row' => 'row1_second',
      'field' => 'field_certifications',
    ],
    [
      'row' => 'row1_second',
      'field' => 'field_required_certifications',
    ],
    [
      'row' => 'row1_first',
      'field' => 'field_hard_skills',
    ],
    [
      'row' => 'row1_first',
      'field' => 'field_soft_skills',
    ],
  ];

  foreach($shuffle_fields as $info) {
    $field = &$variables['content']['_field_layout'][$info['row']][$info['field']];
    if (!empty($field['#items'])) {
      $count = $field['#items']->count();
      $list = [];

      for($i = 0; $i < $count; $i++) {
        if (!empty($field[$i])) {
          $list[] = $field[$i];
        }
      }

      shuffle($list);

      foreach($list as $index => $item) {
        $field[$index] = $item;
      }
    }
  }
}
