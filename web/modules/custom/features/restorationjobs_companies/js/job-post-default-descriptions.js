(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.job_post_default_descriptions = {
    attach: function (context, settings) {
      var revert_text = '';
      $('#suggested-description').click(function(e) {
        revert_text = $('textarea[name="field_job_description[0][value]"]').val();
        e.preventDefault();
        if ($('textarea[name="field_job_description[0][value]"]').val() != settings.default_description) {
          if (confirm("Are you sure you want to use the default description?") == true) {
            $('textarea[name="field_job_description[0][value]"]').val(settings.default_description);
            $('#revert-description').show();
            $(this).hide();
          }
        }
      });
      $('#revert-description').click(function(e) {
        e.preventDefault();
        if ($('textarea[name="field_job_description[0][value]"]').val() != revert_text) {
          if (confirm("Are you sure you want to revert to the previous description?") == true) {
            $('textarea[name="field_job_description[0][value]"]').val(revert_text);
            $(this).hide();
            $('#suggested-description').show();
          }
        }
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
