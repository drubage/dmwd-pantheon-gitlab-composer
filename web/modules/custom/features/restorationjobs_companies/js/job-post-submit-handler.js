(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.job_post_submit_handler = {
    attach: function (context, settings) {
      let $form = $('#job-posting-wizard-form').once('rj-job-post-submit-handler');
      $($form, context).on('submit', function(event, options) {
        let $submit = $(event.target).find(':input.button--primary');
        event.preventDefault();
        options = options || {};
        if ($('.braintree-form').length && !options.tokenized) {
          return;
        }
        $submit.trigger('rj:submit');
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
