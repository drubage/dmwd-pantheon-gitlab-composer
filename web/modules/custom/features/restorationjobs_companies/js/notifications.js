(function($) {
  Drupal.behaviors.manageNotifications = {
    attach: function (context, settings) {
      $('.rj-switch input').once().each(function(event) {
        $(this).on('change', function () {
          let $this = $(this);
          let id = $this.data('jobid');
          let type = $this.data('type');
          let enabled = $this.is(":checked") ?
            'enabled' : 'disabled';

          $this.closest('.switch-wrapper').toggleClass('checked').addClass('blocked');

          let url = `/ajax/job-notifications/${ id }/${ type }/${ enabled }`;
          Drupal.ajax({
            url: url,
            base: false,
            element: false,
            progress: false
          }).execute();

        });
      });
    }
  };

  $.fn.unblockSwitch = function(jobId) {
    $(`input[data-jobid="${ jobId }"]`).parent()
      .parent().removeClass('blocked');
  };
})(jQuery);
