/**
 * @file
 */
(function ($) {

  Drupal.behaviors.billingToggle = {
    attach: function (context, settings) {
      $('.full-history .title').each(function() {
        $(this).on('click', function() {
          const context = $(this).parent().parent();
          $('.second-column', context)
            .toggleClass('show-all');
          $('.title span', context).toggleClass('hidden');
        });
      });
    }
  };

}(jQuery));
