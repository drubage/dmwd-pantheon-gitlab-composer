/**
 * @file
 */
(function($){

  Drupal.behaviors.scripts = {
    attach: function (context, settings) {
      $('.selectpicker').each(function() {
        $(this).closest('.select-wrapper').addClass('has-selectpicker');
      });
    }
  };

}(jQuery));
