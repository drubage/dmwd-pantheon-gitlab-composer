<?php

namespace Drupal\restorationjobs_companies;

use Drupal\Core\Access\AccessResult;

class AccessChecks {

  public static function hasAccessToCompany() {
    // @todo - Don't need static caching here, the access check layer does it.
    static $createJobPosting;

    $uid = \Drupal::currentUser()->id();

    if (!isset($createJobPosting[$uid])) {
      if ($uid == 0) {
        $createJobPosting[$uid] = AccessResult::forbidden();
      } else {
        if (count(CompanyHelper::getUserCompanies($uid)) == 0) {
          $createJobPosting[$uid] = AccessResult::forbidden();
          // @todo - More meaningful
          $createJobPosting[$uid]->setCacheMaxAge(0);
        } else {
          $createJobPosting[$uid] = AccessResult::allowed();
          $createJobPosting[$uid]->setCacheMaxAge(0);
        }
      }
    }
    return $createJobPosting[$uid];
  }

}
