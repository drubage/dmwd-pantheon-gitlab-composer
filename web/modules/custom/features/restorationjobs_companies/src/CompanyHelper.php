<?php

namespace Drupal\restorationjobs_companies;

class CompanyHelper {

  /**
   * @param integer $uid
   *
   * Returns an array of group IDs that this user created.
   *
   * returns array()
   */
  public static function getUserCompanies($uid = NULL) {
    static $getUserCompanies;
    if ($uid == NULL) {
      $uid = \Drupal::currentUser()->id();
    }
    if (isset($getUserCompanies[$uid])) {
      return $getUserCompanies[$uid];
    }
    $query = \Drupal::entityQuery('group')
      ->condition('uid', $uid);
    $results = $query->execute();
    $getUserCompanies[$uid] = array_keys($results);
    return $getUserCompanies[$uid];
  }

  public static function getPostingsForCompany($company_id) {
    $query = \Drupal::entityQuery('job_posting')
      ->condition('field_company', $company_id);
    $results = $query->execute();
    return array_keys($results);
  }

  /**
   * Get a list of job titles on the site
   */
  public static function getJobTitles($useId = FALSE) {
    $options = $subterms = [];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('job_titles');
    foreach ($terms AS $term) {
      $options[$term->name] = [];
      $sub_terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('job_titles', $term->tid);
      foreach ($sub_terms AS $sub_term) {
        $termIndex = $useId ? $sub_term->tid : $sub_term->name;
        $options[$term->name][$termIndex] = $sub_term->name;
        $subterms[] = $sub_term->name;
      }
    }
    foreach($subterms AS $sub_term_name) {
      unset($options[$sub_term_name]);
    }
    return $options;
  }

  /**
   * Get a list of job titles on the site indexed by tid.
   */
  public static function getJobTitlesWithId() {
    $options = $subterms = [];
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('job_titles');
    foreach ($terms AS $term) {
      $options[$term->name] = [];
      $sub_terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('job_titles', $term->tid);
      foreach ($sub_terms AS $sub_term) {
        $options[$term->name][$sub_term->tid] = $sub_term->name;
        $subterms[] = $sub_term->name;
      }
    }
    foreach($subterms AS $sub_term_name) {
      unset($options[$sub_term_name]);
    }
    return $options;
  }

}
