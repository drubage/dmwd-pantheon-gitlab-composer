<?php

namespace Drupal\restorationjobs_companies\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use \Drupal\commerce_order\Entity\OrderInterface;
use Drupal\restorationjobs_companies\Entity\JobPosting;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class CommerceController.
 */
class CommerceController extends ControllerBase {

  public static function order_details($user, OrderInterface $commerce_order, $currently_checking_out = FALSE) {
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    if ($currently_checking_out) {
      $order_timestamp = time();
    } else {
      $order_timestamp = $commerce_order->getCompletedTime();
    }
    $order_time = \Drupal::service('date.formatter')->format($order_timestamp, 'medium', '', NULL, $langcode);

    $items = $commerce_order->getItems();
    $new_items = [];
    foreach ($items AS $item) {
      $job_posting = JobPosting::load($item->get('field_job_posting')->target_id);


      $address = $job_posting->get('field_location')->getValue();
      $url = Url::fromRoute('entity.job_posting.canonical', [
        'job_posting' => $job_posting->id()
      ],['absolute' => TRUE]);
      $job_title = Link::fromTextAndUrl(t("{$job_posting->get('field_job_title')->entity->getName()}<br />{$address[0]['locality']}, {$address[0]['administrative_area']}"), $url)->toString();

      $item->setTitle(Markup::create("{$item->getTitle()} - {$order_time}<br />{$job_title}"));
      
      //$license_id = $item->get('license')->target_id;
      //$license = \Drupal\commerce_license\Entity\License::load($license_id);
      //$subscription = \Drupal\commerce_recurring\Entity\Subscription::load($license_id);
      $new_items[] = $item;
    }
    $commerce_order->setItems($new_items);

    $build = [
      '#theme' => 'commerce_order_receipt',
      '#order_entity' => $commerce_order,
      //'#totals' => $this->orderTotalSummary->buildTotals($commerce_order),
      '#totals' => [
        'total' => $commerce_order->getTotalPrice(),
        'subtotal' => $commerce_order->getSubtotalPrice(),
        'adjustments' => $commerce_order->getAdjustments(),
      ]
    ];

    if ($billing_profile = $commerce_order->getBillingProfile()) {
      $profile_view_builder = \Drupal::entityTypeManager()->getViewBuilder('profile');;
      $build['#billing_information'] = $profile_view_builder->view($billing_profile);
    }

    return $build;
  }

  public static function order_access($user, OrderInterface $commerce_order) {
    $account = \Drupal::currentUser();
    $isOwner = $commerce_order->get('uid')->target_id == $account->id();
    return (in_array('administrator', $account->getRoles()) || $isOwner ? AccessResult::allowed() : AccessResult::forbidden());
  }

}