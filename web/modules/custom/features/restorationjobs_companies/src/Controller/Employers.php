<?php

namespace Drupal\restorationjobs_companies\Controller;

use Drupal\block\entity\Block;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\block_content\Entity\BlockContent;

/**
 * Class Employers.
 */
class Employers extends ControllerBase {

  /*
   * Sets a session variable before redirecting to LinkedIn
   */
  public function linkedin_link() {
    if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'seeker' && isset($_REQUEST['job']) && $_REQUEST['job'] <> '' && is_numeric($_REQUEST['job'])) {
      $_SESSION['job_apply'] = $_REQUEST['job'];
    } elseif (!isset($_REQUEST['type']) || $_REQUEST['type'] != 'seeker') {
      $_SESSION['employer_signup_linked_in_link'] = time();
    }
    return new RedirectResponse(\Drupal\Core\Url::fromRoute('social_auth_linkedin.redirect_to_linkedin')->toString());
  }

  /**
   * Employers page.
   *
   * @return string
   */
  public function page() {
    $employer_signup_form = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_companies\Form\EmployerSignupForm');

    $blocks =  \Drupal::entityTypeManager()->getStorage('block_content')
      ->loadByProperties(['type' => 'generic_content']);

    if (empty($blocks)) {
      module_load_include('install', 'restorationjobs_companies');
      restorationjobs_companies_add_employer_blocks();
      $blocks =  \Drupal::entityTypeManager()->getStorage('block_content')
        ->loadByProperties(['type' => 'generic_content']);
    }

    $employer_signup_content = \Drupal::entityTypeManager()->
      getViewBuilder('block_content')->view(reset($blocks));

    $employer_signup_form['employer_signup_content'] = $employer_signup_content;

    $build = [
      '#theme' => 'employers',
      '#employer_signup_form' => $employer_signup_form,
      '#cache' => [ // NEED TO REMOVE THIS WHEN GOING LIVE
        'max-age' => 0
      ],
    ];

    return $build;
  }

  /**
   * Employers validate email.
   *
   * @return string
   */
  public function validate_email() {
    $account = User::load(\Drupal::currentUser()->id());
    if ($account->hasRole('email_verified')) {
      $url = \Drupal::url('restorationjobs_companies.employers_step2');
      return new RedirectResponse($url);
    }

    $build = [
      '#theme' => 'employers_validate_email',
      '#cache' => [ // NEED TO REMOVE THIS WHEN GOING LIVE
        'max-age' => 0
      ],
    ];

    return $build;
  }

  /**
   * Employers signup step 2.
   *
   * @return string
   */
  public function step2() {
    $account = User::load(\Drupal::currentUser()->id());
    if (!$account->hasRole('email_verified')) {
      $url = \Drupal::url('restorationjobs_users.user_validate_email');
      return new RedirectResponse($url);
    }

    $employer_complete_profile_form = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_companies\Form\EmployerCompleteProfileForm');

    $build = [
      '#theme' => 'employers_signup_step_3',
      '#employer_complete_profile_form' => $employer_complete_profile_form,
    ];

    return $build;
  }

  /**
   * Employer profile page.
   */
  public static function profile() {
    $page = [];
    $uid = \Drupal::currentUser()->id();
    // Active posts.
    $page['active_posts_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => t('Active Job Posts'),
    ];

    $page['active_posts_wrapper']['active_posts'] = [
      '#type' => 'view',
      '#name' => 'my_jobs_dashboard',
      '#display_id' => 'active_posts_profile',
    ];

    $myJobsUrl = Url::fromRoute('restorationjobs_applicants.homepage');

    $page['active_posts_wrapper']['my_jobs_link'] = [
      '#prefix' => '<div class="row"><div class="my-jobs-link col-xs-12 col-sm-6 orange-link-button">',
      '#type' => 'link',
      '#title' => t('VIEW ALL JOB POSTS'),
      '#url' =>  $myJobsUrl,
      '#suffix' => '</div>',
    ];

    $postJobUrl = Url::fromRoute('entity.job_posting.add_form');

    $page['active_posts_wrapper']['post_jobs_link'] = [
      '#prefix' => '<div class="my-jobs-link col-xs-12 col-sm-6 orange-link-button">',
      '#type' => 'link',
      '#title' => t('POST NEW JOB'),
      '#url' =>  $postJobUrl,
      '#suffix' => '</div></div>',
    ];

    // Credit cards.
    $payment_list = \Drupal::entityTypeManager()->
      getStorage('commerce_payment_method')->loadByProperties(['uid' => $uid]);

    if ($payment_list) {
      $page['cards_wrapper'] = [
        '#type' => 'fieldset',
        '#title' => t('Credit Cards'),
        '#attributes' => [
          'class' => [
            'cards-wrapper',
          ]
        ],
      ];

      foreach($payment_list as $method) {
        $preferred = ($method->isDefault() || count($payment_list) == 1);
        $page['cards_wrapper']['card'][$method->id()] = [
          '#theme' => 'credit_card',
          '#credit_card' => [
            'edit' => "/credit-card/" . $method->id() . "/edit",
            'type' => $method->get('card_type')->getString(),
            'number' => $method->get('card_number')->getString(),
            'preferred' => $preferred,
          ]
        ];
      }

    }

    // Active jobs Billing
    $page['billing_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => t('Billing'),
      '#attributes' => [
        'class' => [
          'billing-wrapper',
        ]
      ],
      '#attached' => [
        'library' => [
          'core/drupal.ajax',
          'core/drupal.dialog.ajax',
          'restorationjobs_companies/employer_profile'
        ],
      ]
    ];

    $page['billing_wrapper']['billing_list'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => [
          'col-md-8',
          'billing-list'
        ]
      ]
    ];

    // Get jobs
    $params = [
      'state' => 'active',
      'user_id' => \Drupal::currentUser()->id()
    ];

    $jobs = \Drupal::entityTypeManager()->getStorage('job_posting')
    ->loadByProperties($params);

    $job_summary = [];
    foreach($jobs as $job) {
      $payment_method = $job->getPaymentMethod();
      $card = '';
      if ($payment_method) {
        $card = $payment_method->card_number->getString();
      } else {
        $card = "N/A";
      }
      // Get payment history.
      $logs = \Drupal::entityTypeManager()->getStorage('job_posting_order_log')
        ->loadByProperties(['job_id' => $job->id()]);

      // Sorting logs.
      usort($logs, function($a, $b) {
        return $a->id > $b->id();
      });
      // Only shows the view full history button if there are more
      // than 4 logs.
      $view_full_history = count($logs) > 4;

      $payment_history = [];
      foreach ($logs as $log) {
        $card = $log->get('card_number')->getString();
        $type = $log->get('log_type')->getString();

        $price_info = $log->get('price')->first();
        if (!$price_info->isEmpty()) {
          $price_info = $log->get('price')->first()->toPrice();
          $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
          $price = $currency_formatter->format($price_info->getNumber(), $price_info->getCurrencyCode());
          $price = preg_replace('~\.0+$~', '', $price); // remove .00 if even number
        }
        else {
          $price = NULL;
        }

        $created = $log->get('changed')->getString();
        $created = \Drupal::service('date.formatter')->format($created, 'custom', 'm/d/y');

        if (!isset($payment_history[$card])) {
          $payment_history[$card] = [
            'card' => $card,
            'history' => [
              [
                'type' => $type,
                'price' => $price,
                'created' => $created
              ]
            ]
          ];
        }
        else {
          $payment_history[$card]['history'][] = [
            'type' => $type,
            'price' => $price,
            'created' => $created
          ];
        }
      }
      // Add payment history element.
      $history_container['container'] = ['#type' => 'container'];
      foreach ($payment_history as $log) {
        $history_container['container'][$log['card']] = [
          '#theme' => 'job_posting_order_history',
          '#card' => $log['card'],
          '#history' => $log['history'],
        ];
      }

      $job_title = $job->get('field_job_title')->entity->getName();
      $page['billing_wrapper']['billing_list']['job_' . $job->id()] = [
        '#theme' => 'billing_list_item',
        '#job_title' => $job_title,
        '#card_number' => $card,
        '#job_id' => $job->id(),
        '#payment_history' => $history_container,
        '#view_full_history' => $view_full_history
      ];

    }

    if (!count($jobs)) {
      $page['billing_wrapper']['billing_list']['empty'] = [
        '#markup' => '<div class="billing-card billing-card-empty">' . t('You have no job postings.') . '</div>'
      ];
    }

    $page['billing_wrapper']['billing_list']['billings_link'] = [
      '#prefix' => '<div class="billings-link col-xs-12 orange-link-button">',
      '#type' => 'link',
      '#title' => t('MANAGE ALL JOB POSTINGS'),
      '#url' =>  $myJobsUrl,
      '#suffix' => '</div>',
    ];

    // Get orders.
    $query =  \Drupal::entityTypeManager()->getStorage('commerce_order')->getQuery();
    $query->condition('uid', $uid);
    $query->sort('order_id', 'DESC');
    $ids = $query->execute();
    $orders = \Drupal::entityTypeManager()->getStorage('commerce_order')->loadMultiple($ids);

    $billing_total = 0;

    foreach($orders as $order) {
      foreach($order->getItems() as $item) {
        if ($item->hasField('field_job_posting') && $item->get('license')->entity) {
          $state = $item->get('license')->entity->getState()->getString();
          if ($state == 'active') {
            $job = $item->get('field_job_posting')->entity;
            $job_title = $job->get('field_job_title')->entity->getName();

            $price = $order->getTotalPrice();
            $job_summary[$job->id()] = [
              'title' => $job_title,
              'price' => $price,
            ];
            // Calculate Billing total.
            if (!$billing_total) {
              $billing_total = $price;
            }
            else {
              $billing_total = $billing_total->add($price);
            }
          }
        }
      }
    }

    // Monthly billing details.
    $page['billing_wrapper']['billing_summary'] = [
      '#theme' => 'billing_summary',
      '#billing' => [
        'month' => date('F'),
        'jobs' => $job_summary,
        'total' => $billing_total,
        'list_link' => "/profile/" . $uid . "/orders"
      ],
    ];

    return $page;
  }

  /**
   * Custom access for company profile editing.
   */
  public function accessEdit() {
    $user = \Drupal::currentUser();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $user->id()]);
    return $company ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
