<?php

namespace Drupal\restorationjobs_companies\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\restorationjobs_companies\AccessChecks;

/**
 * Class Homepage.
 */
class Homepage extends ControllerBase {

  /**
   * Homepage.
   *
   * @return string
   */
  public function homepage() {

    $find_a_job_form = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_companies\Form\FindAJobForm');

    $user = \Drupal::currentUser();
    if ($user->isAuthenticated()) {
      $user_type = (AccessChecks::hasAccessToCompany()->isAllowed() ? 'employer' : 'seeker');
    } else {
      $user_type = 'anonymous';
    }

    $find_a_job_form_build = [
      '#theme' => 'homepage_search_box_block',
      '#find_a_job_form' => render($find_a_job_form),
      '#user_type' => $user_type,
      '#cache' => [ // NEED TO REMOVE THIS WHEN GOING LIVE
        'max-age' => 0
      ],
    ];

    $build = [
      '#theme' => 'homepage',
      '#search_box' => render($find_a_job_form_build),
      '#user_type' => $user_type,
      '#cache' => [ // NEED TO REMOVE THIS WHEN GOING LIVE
        'max-age' => 0
      ],
    ];

    // Add compelling statement blocks if they don't exist.
    $blocks =  \Drupal::entityTypeManager()->getStorage('block_content')
      ->loadByProperties(['type' => 'compelling_statement']);
    if (empty($blocks)) {
      module_load_include('install', 'restorationjobs_companies');
      restorationjobs_companies_create_compelling_statements();
    }

    return $build;
  }
}
