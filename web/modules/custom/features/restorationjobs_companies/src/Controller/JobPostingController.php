<?php

namespace Drupal\restorationjobs_companies\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\restorationjobs_job_posting_order_log\Entity\JobPostingOrderLog;
use Drupal\restorationjobs_applicants\SavedSearchNotifications;

/**
 * Class JobPostingController.
 *
 *  Returns responses for Job Posting routes.
 */
class JobPostingController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Job Posting revision.
   *
   * @param int $job_posting_revision
   *   The Job Posting  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($job_posting_revision) {
    $job_posting = $this->entityManager()->getStorage('job_posting')->loadRevision($job_posting_revision);
    $view_builder = $this->entityManager()->getViewBuilder('job_posting');

    return $view_builder->view($job_posting);
  }

  /**
   * Page title callback for a Job Posting  revision.
   *
   * @param int $job_posting_revision
   *   The Job Posting  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($job_posting_revision) {
    $job_posting = $this->entityManager()->getStorage('job_posting')->loadRevision($job_posting_revision);
    return $this->t('Revision of %title from %date', ['%title' => $job_posting->label(), '%date' => format_date($job_posting->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Job Posting .
   *
   * @param \Drupal\restorationjobs_companies\Entity\JobPostingInterface $job_posting
   *   A Job Posting  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(JobPostingInterface $job_posting) {
    $account = $this->currentUser();
    $langcode = $job_posting->language()->getId();
    $langname = $job_posting->language()->getName();
    $languages = $job_posting->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $job_posting_storage = $this->entityManager()->getStorage('job_posting');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $job_posting->label()]) : $this->t('Revisions for %title', ['%title' => $job_posting->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all job posting revisions") || $account->hasPermission('administer job posting entities')));
    $delete_permission = (($account->hasPermission("delete all job posting revisions") || $account->hasPermission('administer job posting entities')));

    $rows = [];

    $vids = $job_posting_storage->revisionIds($job_posting);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\restorationjobs_companies\JobPostingInterface $revision */
      $revision = $job_posting_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $job_posting->getRevisionId()) {
          $link = $this->l($date, new Url('entity.job_posting.revision', ['job_posting' => $job_posting->id(), 'job_posting_revision' => $vid]));
        }
        else {
          $link = $job_posting->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.job_posting.translation_revert', ['job_posting' => $job_posting->id(), 'job_posting_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.job_posting.revision_revert', ['job_posting' => $job_posting->id(), 'job_posting_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.job_posting.revision_delete', ['job_posting' => $job_posting->id(), 'job_posting_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['job_posting_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * User confirmation callback; sets job posting to pending.
   *
   * Is protected by CSRF as this is a GET request from the confirmation modal.
   *
   * @param \Drupal\restorationjobs_companies\Entity\JobPostingInterface $job_posting
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function confirm(JobPostingInterface $job_posting) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
    $state = $job_posting->get('state')->first();
    if (!array_key_exists('submit', $state->getTransitions())) {
      throw new BadRequestHttpException('Confirmation not allowed.');
    }
    $state->applyTransitionById('submit');
    $job_posting->save();

    $search = new SavedSearchNotifications();
    $search->sendNotififications($job_posting);

    return new RedirectResponse($job_posting->toUrl()->toString());
  }

  /**
   * Custom access validation for job editing form.
   */
  public function accessEdit() {
    $job = \Drupal::routeMatch()->getParameter('job_posting');
    if (!is_object($job)) {
      $job = \Drupal::entityManager()->getStorage('job_posting')->load($job);
    }
    $job_owner = $job->getOwnerId();
    $user = \Drupal::currentUser();

    // Check if user is admin.
    if (in_array('administrator', $user->getRoles()) ) {
      return AccessResult::allowed();
    }

    $user_id = $user->id();
    return $job_owner == $user_id ? AccessResult::allowed() : AccessResult::forbidden();
  }

  /**
   * Shows job closing modal.
   */
  public function closeJob() {
    $response = new AjaxResponse();

    $options = [
      'dialogClass' => 'job-close-modal',
    ];
    $response->addCommand(
      new OpenModalDialogCommand($this->t('Hold Up'), $this->confirmModalContent(), $options)
    );
    return $response;
  }

  /**
   * Cancel a job close action.
   */
  public function cancelCloseJob() {
    $command = new CloseModalDialogCommand();
    $response = new AjaxResponse();
    $response->addCommand($command);
    return $response;
  }

  /**
   * Confirm a job close action.
   */
  public function confirmCloseJob() {
    $job_posting = \Drupal::routeMatch()->getParameter('job_posting');
    if (!is_object($job_posting)) {
      $job_posting = $this->entityManager()->getStorage('job_posting')->load($job_posting);
    }

    $state = $job_posting->get('state')->first();
    if (array_key_exists('expire', $state->getTransitions())) {
      $state->applyTransitionById('expire');
      $job_posting->save();
    }

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());

    $url = Url::fromRoute('entity.job_posting.canonical', [
      'job_posting' => $job_posting->id(),
    ])->toString();
    $response->addCommand(new RedirectCommand($url));

    // Log job closing.
    // Get order id from latest log.
    $logs = \Drupal::entityManager()->getStorage('job_posting_order_log')
      ->loadByProperties(['job_id' => $job_posting->id()]);
    $latest_log = end($logs);

    $uid = \Drupal::currentUser()->id();
    $log = JobPostingOrderLog::create([
      'uid' => $uid,
      'job_id' => $job_posting->id(),
      'card_number' => $latest_log->get('card_number')->getString(),
      'log_type' => 'Closed',
    ]);
    $log->save();

    drupal_set_message(t('Your job has been closed.'));

    return $response;
  }

  /**
   * Generate the confirmation modal.
   *
   * @return array
   *   Render array
   */
  protected function confirmModalContent() {

    $modal['disclaimer'] = [
      '#prefix' => '<div>',
      '#markup' => $this->t('Are you sure you want to close this job posting?'),
      '#suffix' => '</div>',
      '#weight' => 1,
    ];
    $modal['actions']['#weight'] = 10;
    // Confirm button.
    $job_id = \Drupal::routeMatch()->getParameter('job_posting');
    $modal['actions']['confirm'] = Link::fromTextAndUrl(
      $this->t('Confirm'),
      Url::fromRoute('restorationjobs_companies.job_confirm_close', [
        'job_posting' => $job_id,
      ])
    )->toRenderable();
    $modal['actions']['confirm']['#attributes'] = [
      'class' => ['use-ajax', 'btn', 'btn-white-primary', 'btn-back']
    ];

    // Cancel button.
    $modal['actions']['cancel'] = Link::fromTextAndUrl(
      $this->t('Cancel'),
      Url::fromRoute('restorationjobs_companies.job_cancel_close')
    )->toRenderable();
    $modal['actions']['cancel']['#attributes'] = [
      'class' => ['use-ajax', 'btn', 'btn-white-primary']
    ];
    $modal['actions']['#prefix'] = '<div class="modal-actions">';
    $modal['actions']['#suffix'] = '</div>';

    $modal['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $modal;
  }

  public static function getStructuredData(JobPostingInterface $job_posting) {
    $data = [];
    $data['@context'] = 'http://schema.org';
    $data['@type'] = 'JobPosting';
    $data['datePosted'] = date('Y-m-d', $job_posting->getCreatedTime());
    $data['description'] = $job_posting->get('field_job_description')->value;
    $data['title'] = $job_posting->get('field_job_title')->entity->getName();
    $data['employmentType'] = 'temporary';
    $data['industry'] = 'Building Restoration';

    $payment_method = $job_posting->getPaymentMethod();
    if ($payment_method) {
      $expire_timestamp = $payment_method->getExpiresTime();
      $data['validThrough'] = date(DATE_ISO8601, $expire_timestamp);
    }

    if ($job_posting->get('field_reveal_payment')->value == 1) {
      $salary = $job_posting->get('field_salary_range')->getValue();
      $data['baseSalary'] = [
        '@type' => 'MonetaryAmount',
        'currency' => 'USD',
        'value' => [
          '@type' => 'QuantitativeValue',
          'minValue' => $salary[0]['from'],
          'maxValue' => $salary[0]['to'],
          'unitText' => 'JOB',
        ],
      ];
    }

    $company = \Drupal::entityTypeManager()->getStorage('group')->load($job_posting->get('field_company')->getString());
    $company_address = $company->get('field_office_address')->first()->getValue();
    $data['hiringOrganization'] = [
      '@type' => 'Organization',
      'name' => $company->label(),
      'address' => [
        '@type' => 'PostalAddress',
        'streetAddress' => $company_address['address_line1'],
        'addressLocality' => $company_address['locality'],
        'addressRegion' => $company_address['administrative_area'],
        'postalCode' => $company_address['postal_code'],
        'addressCountry' => $company_address['country_code'],
      ],
    ];
    $address = $job_posting->get('field_location')->getValue();
    $data['jobLocation'] = [
      '@type' => 'Place',
      'name' => $company->label(),
      'address' => [
        '@type' => 'PostalAddress',
        'addressLocality' => $address[0]['locality'],
        'addressRegion' => $address[0]['administrative_area'],
        'addressCountry' => $address[0]['country_code'],
      ],
    ];

    return $data;
  }

}
