<?php

namespace Drupal\restorationjobs_companies\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Url;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Render\Markup;

/**
 * Class Notifications.
 */
class Notifications extends ControllerBase {

/**
 * Update notification configuration.
 */
  public static function configureNotification(JobPostingInterface $job_posting, $type, $enable = 'enabled') {
    $response = new AjaxResponse();
    $enable = $enable == 'enabled' ? TRUE : FALSE;

    $job_posting->set($type, $enable);
    $job_posting->save();
    $title = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($job_posting->get('field_job_title')->getString())->getName();

    drupal_get_messages();
    drupal_set_message(t('Notification preferences updated'));

    $status_messages = array('#type' => 'status_messages');
    $messages = \Drupal::service('renderer')->renderRoot($status_messages);

    if (!empty($messages)) {
      $response->addCommand(new RemoveCommand('div[data-drupal-messages]'));
      $response->addCommand(new PrependCommand('.view-notifications', $messages));
    }

    $response->addCommand(
      new InvokeCommand(NULL, 'unblockSwitch', [$job_posting->id()])
    );
    return $response;
  }

  /**
   * Custom access check.
   */
  public static function access(JobPostingInterface $job_posting, $type, $enable = TRUE) {
    $isOwner = $job_posting->getOwnerId() == \Drupal::currentUser()->id();
    return $isOwner ? AccessResult::allowed() : AccessResult::forbidden();
  }

  /**
   * Send notification.
   */
  public static function sendNotification(JobPostingInterface $job_posting, $type, $options, $send = FALSE) {
    // Check access and preferences.
    if (!$send) {
      $send = (boolean) $job_posting->get($type)->getString();
    }

    if ($send) {
      $mailManager = \Drupal::service('plugin.manager.mail');
      $isMessageMail = ($type == 'field_notifications_messages');

      $job_poster = $job_posting->getOwner();
      $to = $job_poster->getEmail();
      $langcode = $job_poster->getPreferredLangcode();

      $titleTerm = $job_posting->get('field_job_title')->getString();
      $title = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($titleTerm)->getName();

      $location = $job_posting->get('field_location')->first()->getValue();
      $subject = $title . ' - ' . $location['locality'];

      $notificationsUrl = Url::fromRoute('restorationjobs_applicants.notifications')->toString();

      if (!empty($options['application'])) {
        $application = \Drupal::entityTypeManager()
          ->getStorage('job_application')->load($options['application']);
      }

      $key = 'rj-owner-';

      switch($type) {
        case 'field_notifications_messages':
          $key .= 'message';

          $current_uid = \Drupal::currentUser()->id();
          $job_seeker = $application->getOwner();

          if ($job_poster->id() == $current_uid) {
            $to = $job_seeker->getEmail();
            $langcode = $job_seeker->getPreferredLangcode();
            $name = $job_poster->get('field_first_name')->getString() . ' ' . $job_poster->get('field_last_name')->getString();
          } else {
            $to = $job_poster->getEmail();
            $langcode = $job_poster->getPreferredLangcode();
            $name = $job_seeker->get('field_first_name')->getString() . ' ' . $job_seeker->get('field_last_name')->getString();
          }

          $conversationUrl = Url::fromRoute('restorationjobs_messaging.messaging_controller_messages', [], [
            'query' => [
              'job_id' => $job_posting->id(),
              'app_id' => $options['application'],
            ]
          ])->toString();

          $theme = [
            '#theme' => 'notification_owner_message',
            '#email' => [
              'name' =>  $name,
              'notificationsUrl' => $notificationsUrl,
              'message' => $options['message'],
              'conversationUrl' => $conversationUrl,
            ]
          ];

          $params['body'] = \Drupal::service('renderer')->renderRoot($theme);
          $params['subject'] = 'New Message Regarding: '  . $subject;
          break;

        case 'field_notifications_applicants':
          $key .= 'new-applicant';

          // Applicant fields.
          $user = \Drupal::entityTypeManager()
            ->getStorage('user')->load($options['applicant']);
          // Profile image.
          $photoUrl = '';
          if ($file = $user->get('field_profile_photo')->first()) {
            $resume = $file->entity->getFileUri();
            $photoUrl = file_create_url($resume);
          }

          // Name
          $name = $user->get('field_first_name')->getString() . ' ' . $user->get('field_last_name')->getString();
          // Location.
          $location = $user->get('field_user_location')->getString();

          // Job application fields.
          $score = $application->get('field_applicant_score')->getString();

          // Applicant profile url.
          $profileUrl = Url::fromRoute('restorationjobs_applicants.job_application_owner_profile', [
            'job_posting' => $job_posting->id(),
            'user' => $user->id()
          ])->toString();

          $theme = [
            '#theme' => 'notification_owner_new_applicant',
            '#email' => [
              'photoUrl' => $photoUrl,
              'name' => $name,
              'location' => $location,
              'score' => $score,
              'profileUrl' => $profileUrl,
              'title' => $subject,
              'notificationsUrl' => $notificationsUrl,
            ]
          ];

          $params['body'] = \Drupal::service('renderer')->renderRoot($theme);
          $params['subject'] = 'New Applicant: ' . $subject;
          break;

        case 'job-verified':
          $key .= 'job-verified';

          $theme['intro'] = [
            '#theme' => 'notification_owner_job_verified',
            '#body' => [
              'text' => 'Your job post has been approved.',
              'job_url' => $job_posting->toUrl(),
            ]
          ];

          if (isset($options['order'])) {
            $receipt = CommerceController::order_details($job_poster, $options['order'], TRUE);
            $renderer = \Drupal::service('renderer');
            $theme['intro']['#body']['receipt'] = $renderer->renderPlain($receipt);
          }

          $params['body'] = \Drupal::service('renderer')->renderRoot($theme);

          $params['subject'] = 'Your job post has been approved';
          break;

        case 'job-pending':
          $key .= 'job-pending';

          $theme = [
            '#theme' => 'notification_owner_job_pending',
            '#body' => [
              'text' => 'Your new job post is pending review.',
              'job_url' => $job_posting->toUrl()
            ]
          ];

          $params['body'] = \Drupal::service('renderer')->renderRoot($theme);
          $params['subject'] = 'Your new job post is pending review';
          break;

        case 'job-rejected':
          $key .= 'job-rejected';

          $theme = [
            '#theme' => 'notification_owner_job_rejected',
            '#body' => [
              'text' => 'Your job post has been rejected.',
              'job_url' => $job_posting->toUrl()
            ]
          ];

          $params['body'] = \Drupal::service('renderer')->renderRoot($theme);
          $params['subject'] = 'Your job post has been rejected';
          break;

	case 'payment_fail':
          $key .= 'payment-fail';
          $params['body'] = t('Your payment for job @job has been rejected.', [
            '@job' => $job_posting->link($title)
          ]);
          $params['subject'] = t('Your payment for job @job has been rejected', [
            '@job' => $title
          ]);

          break;

        case 'payment_accepted':
          $key .= 'payment-accepted';
          $body = t('Your payment for job @job has been accepted.', [
            '@job' => $job_posting->link($title)
          ]);
          if (isset($options['order'])) {
            $receipt = CommerceController::order_details($job_poster, $options['order'], TRUE);
            $renderer = \Drupal::service('renderer');
            $receipt = $renderer->renderPlain($receipt);
            $body .= $receipt;
          }
          $params['body'] = Markup::create($body);
          $params['subject'] = t('Your payment for job @job has been accepted', [
            '@job' => $title
          ]);

          break;
      }

      $mailManager->mail('restorationjobs_companies', $key, $to, $langcode, $params, NULL, TRUE);
    }
  }
}
