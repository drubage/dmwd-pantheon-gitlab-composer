<?php

namespace Drupal\restorationjobs_companies\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\restorationjobs_seo\GoogleIndexApi;

/**
 * Defines the Job Posting entity.
 *
 * @ingroup restorationjobs_companies
 *
 * @ContentEntityType(
 *   id = "job_posting",
 *   label = @Translation("Job Posting"),
 *   handlers = {
 *     "storage" = "Drupal\restorationjobs_companies\JobPostingStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\restorationjobs_companies\JobPostingListBuilder",
 *     "views_data" = "Drupal\restorationjobs_companies\Entity\JobPostingViewsData",
 *     "translation" = "Drupal\restorationjobs_companies\JobPostingTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\restorationjobs_companies\Form\JobPostingForm",
 *       "add" = "Drupal\restorationjobs_companies\Form\PostAJobWizardForm",
 *       "wizard" = "Drupal\restorationjobs_companies\Form\PostAJobWizardForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "employer_edit" = "Drupal\restorationjobs_companies\Form\JobEditForm",
 *       "delete" = "Drupal\restorationjobs_companies\Form\JobPostingDeleteForm",
 *     },
 *     "access" = "Drupal\restorationjobs_companies\JobPostingAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\restorationjobs_companies\JobPostingHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "job_posting",
 *   data_table = "job_posting_field_data",
 *   revision_table = "job_posting_revision",
 *   revision_data_table = "job_posting_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer job posting entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/job/{job_posting}",
 *     "add-form" = "/post-job",
 *     "confirm" = "/post-job/{job_posting}/confirm",
 *     "wizard-form" = "/post-job/{job_posting}/{step}",
 *     "edit-form" = "/admin/structure/job_posting/{job_posting}/edit",
 *     "delete-form" = "/admin/structure/job_posting/{job_posting}/delete",
 *     "version-history" = "/admin/structure/job_posting/{job_posting}/revisions",
 *     "revision" = "/admin/structure/job_posting/{job_posting}/revisions/{job_posting_revision}/view",
 *     "revision_revert" = "/admin/structure/job_posting/{job_posting}/revisions/{job_posting_revision}/revert",
 *     "revision_delete" = "/admin/structure/job_posting/{job_posting}/revisions/{job_posting_revision}/delete",
 *     "translation_revert" = "/admin/structure/job_posting/{job_posting}/revisions/{job_posting_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/job_posting",
 *   },
 *   field_ui_base_route = "job_posting.settings"
 * )
 */
class JobPosting extends RevisionableContentEntityBase implements JobPostingInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the job_posting owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
    // todo make this more specific
    if (!$this->getName()) {
      $this->setName('Job Post');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $original_published = (is_object($this->original) && $this->original->get('state')->value == 'active');
    $published = ($this->get('state')->value == 'active');
    $url = $this->toUrl('canonical')->toString();
    if (getenv('ENVIRONMENT') == 'PRODUCTION') {
      if ($published) { // job is published
        \Drupal::service('google_index_api.client')->updateUrl($url);
      }
      elseif ($original_published && !$published) { // job was unpublished
        \Drupal::service('google_index_api.client')->deleteUrl($url);
      }
    }
    if ($published) {
      // if any of these fields change, update all job_applications for this job_posting
      $fields_to_check = array(
        'field_certifications',
        'field_required_certifications',
        'field_hard_skills',
        'field_soft_skills',
      );
      $update_applications = FALSE;
      foreach ($fields_to_check AS $field) {
        if (!($this->get($field)->equals($this->original->get($field)))) {
          $update_applications = TRUE;
          break;
        }
      }
      if ($update_applications) {
        $applications = $this->getApplicationsForJob();
        foreach ($applications as $result) {
          $job_application = \Drupal::entityTypeManager()->getStorage('job_application')->load($result);
          $job_application->save();
        }
      }
    }
  }

  /**
   * @inheritDoc
   */
  public function getPaymentMethod() {
    $method = $this->get('initial_payment_method')->first();
    return $method ? $method->entity : NULL;
  }

  /**
   * @inheritDoc
   */
  public function getExpiration($timestamp_only = FALSE) {
    $payment_method = $this->getPaymentMethod();
    if ($payment_method) {
      $expire_timestamp = $payment_method->getExpiresTime();
      if ($timestamp_only) {
        return $expire_timestamp;
      }
      $expire = new \DateTime();
      $expire->setTimestamp($expire_timestamp);
      $now = new \DateTime();
      return $now->diff($expire, true)->days;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return $this->get('state')->first()->getValue()['value'] == 'active';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Job Posting entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['initial_payment_method'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Initial payment method'))
      ->setDescription(t('The initial payment method.'))
      ->setSetting('target_type', 'commerce_payment_method');
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Job Posting entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('The workflow state.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'state_transition_form',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('workflow_callback', ['\Drupal\restorationjobs_companies\Entity\JobPosting', 'getWorkflowId']);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Callback to get the approval workflow.
   *
   * @param \Drupal\restorationjobs_companies\Entity\JobPostingInterface $jobPosting
   *   The job posting.
   *
   * @return string
   */
  static public function getWorkflowId(JobPostingInterface $jobPosting) {
    // Future proofing: Check if the user can post without approval.
    return 'job_posting_validation';
  }

  public function getStore() {
    /** @var \Drupal\commerce_store\Resolver\DefaultStoreResolver $resolver */
    $resolver = \Drupal::service('commerce_store.default_store_resolver');
    return $resolver->resolve();
  }

  /**
   * Check if a user has applied to a job posting
   *
   * @param null $uid
   */
  public function hasApplied($uid = NULL, $return_id = FALSE) {
    if ($uid == NULL) {
      $uid = \Drupal::currentUser()->id();
    }
    $query = \Drupal::entityQuery("job_application")
      ->condition("user_id", $uid)
      ->condition("field_job_posting", $this->id());
    $results = $query->execute();
    if ($return_id && count($results) > 0) {
      return reset($results);
    }
    return ($uid <> 0 && count($results) > 0) ? TRUE : FALSE;
  }

  /**
   * Get applications for job posting
   *
   * @param false $count
   */
  public function getApplicationsForJob($count = FALSE) {
    $query = \Drupal::entityQuery("job_application")
      ->condition("field_job_posting", $this->id());
    $results = $query->execute();
    if ($count) {
      return count($results);
    } else {
      return $results;
    }
  }

  /**
   * Get job posting title.
   */
  function getTitle() {
    return $this->get('field_job_title')->entity->getName();
  }

}
