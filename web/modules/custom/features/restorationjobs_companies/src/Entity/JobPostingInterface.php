<?php

namespace Drupal\restorationjobs_companies\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Job Posting entities.
 *
 * @ingroup restorationjobs_companies
 */
interface JobPostingInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Job Posting name.
   *
   * @return string
   *   Name of the Job Posting.
   */
  public function getName();

  /**
   * Sets the Job Posting name.
   *
   * @param string $name
   *   The Job Posting name.
   *
   * @return \Drupal\restorationjobs_companies\Entity\JobPostingInterface
   *   The called Job Posting entity.
   */
  public function setName($name);

  /**
   * Gets the Job Posting creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Job Posting.
   */
  public function getCreatedTime();

  /**
   * Sets the Job Posting creation timestamp.
   *
   * @param int $timestamp
   *   The Job Posting creation timestamp.
   *
   * @return \Drupal\restorationjobs_companies\Entity\JobPostingInterface
   *   The called Job Posting entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Job Posting published status indicator.
   *
   * Unpublished Job Posting are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Job Posting is published.
   */
  public function isPublished();

  /**
   * Gets the Job Posting revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Job Posting revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\restorationjobs_companies\Entity\JobPostingInterface
   *   The called Job Posting entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Job Posting revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Job Posting revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\restorationjobs_companies\Entity\JobPostingInterface
   *   The called Job Posting entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Get the payment method specified on creation.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentMethodInterface
   */
  public function getPaymentMethod();

  /**
   * Get the job expiration timestamp.
   *
   * @return string
   *   Expiration of the Job Posting.
   */
  public function getExpiration();

}
