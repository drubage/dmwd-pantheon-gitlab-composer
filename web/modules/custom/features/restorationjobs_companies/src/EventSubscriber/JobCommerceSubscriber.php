<?php

namespace Drupal\restorationjobs_companies\EventSubscriber;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Resolver\OrderTypeResolverInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\restorationjobs_job_posting_order_log\Entity\JobPostingOrderLog;
use Drupal\restorationjobs_job_posting_order_log\JobOrderLogger;
use Drupal\restorationjobs_companies\Controller\Notifications;

class JobCommerceSubscriber implements EventSubscriberInterface {

  /**
   * The job posting product variation from default_content module.
   */
  const POSTING_PRODUCT_VARIATION_UUID = '2b15de2c-c8c2-4eb3-a3ce-89eb5f46de9c';

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order type resolver.
   *
   * @var \Drupal\commerce_order\Resolver\OrderTypeResolverInterface
   */
  protected $orderTypeResolver;

  /**
   * @inheritDoc
   */
  public function __construct(
    CartManagerInterface $cartManager,
    CartProviderInterface $cartProvider,
    EntityTypeManagerInterface $entityTypeManager,
    OrderTypeResolverInterface $orderTypeResolver
  ) {
    $this->cartManager = $cartManager;
    $this->cartProvider = $cartProvider;
    $this->entityTypeManager = $entityTypeManager;
    $this->orderTypeResolver = $orderTypeResolver;
  }

  /**
   * React to transition to verified state; this is after either programmatic
   * or manual verification. The card information is stored on the posting.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   */
  public function onJobVerify(WorkflowTransitionEvent $event) {
    /** @var \Drupal\restorationjobs_companies\Entity\JobPostingInterface $jobPosting */
    $jobPosting = $event->getEntity();
    $productVariationStorage = $this
      ->entityTypeManager->getStorage('commerce_product_variation');
    if (!$productVariation = $productVariationStorage->loadByProperties(['uuid' => self::POSTING_PRODUCT_VARIATION_UUID])) {
      throw new \Exception('Cannot load job posting product variation.');
    }
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation */
    $productVariation = reset($productVariation);
    $orderItem = $this->cartManager->createOrderItem($productVariation);
    $orderType = $this->orderTypeResolver->resolve($orderItem);
    // Create a cart-order so we can attach payment info and programmatically
    // check out.
    $order = $this->cartProvider->getCart($orderType, NULL, $jobPosting->getOwner());
    if (!$order) {
      $order = $this->cartProvider->createCart($orderType, NULL, $jobPosting->getOwner());
    }
    $orderItem
      ->set('field_job_posting', $jobPosting)
      ->save();
    $this->cartManager->addOrderItem($order, $orderItem);
    // Programmatically check out.
    $this->cartProvider->finalizeCart($order, FALSE);
    $transition = $order->getState()->getWorkflow()->getTransition('place');
    $order->getState()->applyTransition($transition);
    $order->save();

    $fromId = $event->getFromState()->getId();
    $logType = $fromId == 'expired' ? JobOrderLogger::JOB_ORDER_TYPE_REOPENED :
      JobOrderLogger::JOB_ORDER_TYPE_OPENED;

    // Creates a log entry.
    $paymentMethod = $jobPosting->getPaymentMethod();
    $uid = \Drupal::currentUser()->id();
    $log = JobPostingOrderLog::create([
      'uid' => $uid,
      'job_id' => $jobPosting->id(),
      'order_id' => $order->id(),
      'card_number' => ($paymentMethod ? $paymentMethod->card_number->getString() : 'N/A'),
      'log_type' => $logType,
      'price' => $order->getTotalPrice()
    ]);
    $log->save();

    $options = [
      'order' => $order,
    ];
    Notifications::sendNotification($jobPosting, 'job-verified', $options, TRUE);
  }

  public function onJobSubmit(WorkflowTransitionEvent $event) {
    $jobPosting = $event->getEntity();
    Notifications::sendNotification($jobPosting, 'job-pending', [], TRUE);

  }

  public function onJobReject(WorkflowTransitionEvent $event) {
    $jobPosting = $event->getEntity();
    Notifications::sendNotification($jobPosting, 'job-rejected', [], TRUE);
  }

  /**
   * Configure the license prior to Commerce License activating it.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   */
  public function onCartPlace(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($order->bundle() == 'recurring') {
      return;
    }
    /** @var \Drupal\commerce_license\LicenseStorageInterface $licenseStorage */
    $licenseStorage = $this->entityTypeManager->getStorage('commerce_license');
    // Skip recurring orders.

    foreach ($order->getItems() as $item) {
      if (!$item->hasField('field_job_posting')) {
        \Drupal::logger('onCartPlace')->error('Error on job recurring ' . $order->id());
      }

      try {
        $license = $licenseStorage->createFromOrderItem($item);
        $license->set(
          'license_job_posting',
          $item->get('field_job_posting')->first()->entity
        );
        $license->save();
        $item->set('license', $license);
        // Set the entity property so the default subscriber picks up on it.
        // @see LicenseOrderSyncSubscriber::107.
        $item->license->entity = $license;
        $item->save();
      }
      catch(\Exception $ex) {
        \Drupal::logger('onCartPlace')->error('Error on license set');
      }
    }
    $this->addPayment($order, $event);
  }

  /**
   * Add payment method to order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   */
  public function addPayment($order, $event) {
    if ($order->getBalance()->isPositive()) {
      if ($jobPosting = $this->getJobPostingFromOrder($order)) {
        if ($paymentMethod = $jobPosting->getPaymentMethod()) {
          $gatewayConfig = $paymentMethod->getPaymentGateway();
          // Save payment info to the order.
          $order->set('payment_gateway', $gatewayConfig);
          $order->set('payment_method', $paymentMethod);
          $order->setBillingProfile($paymentMethod->getBillingProfile());

          /** @var \Drupal\commerce_payment\Entity\PaymentInterface $charge */
          $charge = Payment::create([
            'order_id' => $order->id(),
            'payment_method' => $paymentMethod->id(),
            'amount' => $order->getBalance(),
            'payment_gateway' => $gatewayConfig->id(),
          ]);
          /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface $gateway */
          $gateway = $gatewayConfig->getPlugin();
          try {
            $gateway->createPayment($charge);
          } catch (\Throwable $e) {
            \Drupal::logger('Payment error')
              ->notice(t('Order: @id, error: @error'), [
                'error' => $e->getMessage(),
                'id' => $order->id()
              ]);

            \Drupal::messenger()
              ->addMessage(t('There was an error while trying to charge job @job',
                [
                  '@job' => $jobPosting->id()
                ]
              ), 'error');

            $state = $jobPosting->get('state')->first();
            if (array_key_exists('payment_fail', $state->getTransitions())) {
              $state->applyTransitionById('payment_fail');
              $jobPosting->save();
            }
            // Sending notification.

            Notifications::sendNotification($jobPosting, 'payment_fail', [], TRUE);
            return;
          }

          $options = [];
          $options['order'] = $order;
          //Notifications::sendNotification($jobPosting, 'payment_accepted', $options, TRUE);
        }
      }
    }
  }

  /**
   * Convenience method for finding job posting from an order.
   *
   * Finds only the first job posting; right now we know there's only one.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return \Drupal\restorationjobs_companies\Entity\JobPostingInterface|null
   *   The job posting, or NULL if not found.
   */
  protected function getJobPostingFromOrder(OrderInterface $order) {
    foreach ($order->getItems() as $orderItem) {
      if ($orderItem->hasField('field_job_posting')) {
        return $orderItem->get('field_job_posting')->first()->entity;
      }
    }
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    $events['job_posting.verify.post_transition'] = 'onJobVerify';
    $events['job_posting.submit.post_transition'] = 'onJobSubmit';
    $events['job_posting.reject.post_transition'] = 'onJobReject';
    // License's subscriber is 100, we need to configure first.
    $events['commerce_order.place.pre_transition'] = ['onCartPlace', 110];
    return $events;
  }

}
