<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CreditCardForm.
 */
class CreditCardForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'credit_card_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $method_id = \Drupal::routeMatch()->getParameter('commerce_payment_method');
    $payment_method = \Drupal::entityTypeManager()->getStorage('commerce_payment_method')
      ->load($method_id);

    $form['add_payment_method'] = [
      '#type' => 'commerce_payment_gateway_form',
      '#operation' => 'add-payment-method',
      '#default_value' => $payment_method,
    ];

    $config = \Drupal::config('commerce_payment.commerce_payment_gateway.braintree');
    $braintree_merchant_id = $config->get('configuration.merchant_id');

    $form['submit'] = [
      '#prefix' => '<div class="col-xs-12"><div class="col-md-offset-3 col-md-6 col-xs-12">',
      '#type' => 'submit',
      '#value' => t('Save Changes'),
      '#attributes' => ['class' => ['button-full-orange']],
      '#suffix' => '</div></div><div class="text-center"><br /><br /><br /></div>'
    ];

    $form['#attributes']['class'][] = 'col-md-8 col-md-offset-2';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('Your payment method has been updated.'));
    $form_state->setRedirect('restorationjobs_users.account_edit');
  }

  /**
   * Custom access for credit card editing.
   */
  public function accessEdit() {
    $user =  \Drupal::currentUser();
    if ($user->hasPermission("update credit cards on a users behalf")) {
      return AccessResult::allowed();
    }

    $method_id = \Drupal::routeMatch()->getParameter('commerce_payment_method');
    $method = \Drupal::entityTypeManager()->getStorage('commerce_payment_method')
      ->load($method_id);

    return $method->getOwnerId() == $user->id() ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
