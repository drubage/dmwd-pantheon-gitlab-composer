<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\restorationjobs_companies\CompanyHelper;
use CommerceGuys\Addressing\AddressFormat\AddressField;

/**
 * Class EmployerCompleteProfileForm.
 */
class EmployerCompleteProfileForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'employer_complete_profile_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $group = NULL;
    $gids = CompanyHelper::getUserCompanies(\Drupal::currentUser()->id());
    if (count($gids) > 0) {
      $group = Group::load($gids[0]);
    }

    $form['group'] = [
      '#type' => 'value',
      '#value' => $group
    ];

    $form['name'] = [
      '#title' => $this->t('Company Name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Company Name'),
      ],
      '#default_value' => ($group <> NULL ? $group->label() : ''),
    ];

    //ADDRESS FIELDS
    $form['address'] = [
      '#type' => 'address',
      '#default_value' => [
        'country_code' => 'US',
      ],
      '#used_fields' => [
        //AddressField::GIVEN_NAME,
        //AddressField::FAMILY_NAME,
        AddressField::ADDRESS_LINE1,
        AddressField::ADDRESS_LINE2,
        AddressField::ADMINISTRATIVE_AREA,
        AddressField::LOCALITY,
        AddressField::POSTAL_CODE,
      ],
      '#available_countries' => ['US'],
    ];

    if ($group <> NULL) {
      $default_address = $group->get('field_office_address')->getValue();
      if (isset($default_address[0])) {
        $form['address']['#default_value']['country_code'] = $default_address[0]['country_code'];
        $form['address']['#default_value']['address_line1'] = $default_address[0]['address_line1'];
        $form['address']['#default_value']['administrative_area'] = $default_address[0]['administrative_area'];
        $form['address']['#default_value']['locality'] = $default_address[0]['locality'];
        $form['address']['#default_value']['postal_code'] = $default_address[0]['postal_code'];
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('COMPLETE PROFILE'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    if ($values['group'] <> '') {
      $group = $values['group'];
    } else {
      $group = Group::create([
        'type' => 'company',
      ]);
    }

    $group->set('label', $values['name']);
    $group->set('field_office_address', $values['address']);
    $group->save();

    $messenger = \Drupal::messenger();
    $messenger->addMessage("Company profile updated.");

    //$form_state->setRedirect('entity.group.canonical', ['group' => $group->id()]);
    $form_state->setRedirect('entity.job_posting.add_form');
  }

}
