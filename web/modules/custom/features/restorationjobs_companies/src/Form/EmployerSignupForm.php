<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\group\Entity\Group;
use Drupal\core\Url;

/**
 * Class EmployerSignupForm.
 */
class EmployerSignupForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'employer_signup_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['mail'] = [
      '#title' => $this->t('Email'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];

    $form['company'] = [
      '#title' => $this->t('Company Name'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('START POSTING JOBS'),
      '#attributes' => ['class' => ['btn', 'btn-primary']],
      '#prefix' => '<div class="form-actions">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $mail = trim($form_state->getValue('mail'));

    if (!\Drupal::service('email.validator')->isValid($mail)) {
      $form_state->setErrorByName('mail', t('Please enter a valid email address.'));
    } else {
      if (user_load_by_mail($mail)) {
        $form_state->setErrorByName('mail', t('This email address is already registered.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $params = [
      'name' => trim($form_state->getValue('mail')),
      'mail' => trim($form_state->getValue('mail')),
    ];

    $account = User::create($params);
    $account->enforceIsNew();
    $account->activate();
    $messenger = \Drupal::messenger();

    if ($account->save()) {
      $messenger->addMessage("User account created.");
      user_login_finalize($account);
      _user_mail_notify('register_no_approval_required', $account);

      // Create the company for this user

      $group = Group::create([
        'type' => 'company',
        'label' => trim($form_state->getValue('company')),
      ]);
      $group->enforceIsNew();
      $group->setOwner($account);
      $group->save();

    } else {
      $messenger->addMessage("Error creating account.");
    }

  }

}
