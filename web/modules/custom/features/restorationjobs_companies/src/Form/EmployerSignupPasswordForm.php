<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class EmployerSignupPasswordForm.
 */
class EmployerSignupPasswordForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'employer_signup_password_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['pass'] = [
      //'#title' => $this->t('Password'),
      '#type' => 'password',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Password'),
      ]
    ];

    $form['pass2'] = [
      //'#title' => $this->t('Confirm Password'),
      '#type' => 'password',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Confirm Password'),
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sign In'),
      '#attributes' => ['class' => ['btn', 'btn-primary']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();

    if ($values['pass'] <> $values['pass2']) {
      $form_state->setErrorByName('pass2', t('Make sure that passwords match.'));
    }
    if (strlen($values['pass']) < 6) {
      $form_state->setErrorByName('pass', t('Passwords must be at least 6 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $account = User::load(\Drupal::currentUser()->id());

    // Set the new password
    $account->setPassword($form_state->getValue('pass'));

    // Save the user
    $account->save();
    $messenger = \Drupal::messenger();
    $messenger->addMessage("Your password has been set.");
    $form_state->setRedirect('restorationjobs_companies.employers_step2');
  }

}