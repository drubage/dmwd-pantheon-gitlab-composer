<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\restorationjobs_companies\CompanyHelper;

/**
 * Class FindAJobForm.
 */
class FindAJobForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'find_a_job_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = CompanyHelper::getJobTitles();

    $form['title'] = [
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => '- Job Title -',
      '#attributes' => [
        'title' => $this->t('Title'),
      ],
      '#default_value' => (isset($_REQUEST['title']) ? $_REQUEST['title'] : ''),
    ];

    $form['location'] = [
      '#description' => $this->t("Enter any location like 'Denver, CO' or 'Manhattan'"),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => [$this->t('Location')]
      ],
      '#default_value' => (isset($_REQUEST['location']) ? $_REQUEST['location'] : ''),
    ];

    $form['latitude'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'edit-latitude',
      ],
      '#default_value' => (isset($_REQUEST['field_latitude_longitude_proximity']['source_configuration']['origin']['lat']) ? $_REQUEST['field_latitude_longitude_proximity']['source_configuration']['origin']['lat'] : '')
    ];

    $form['longitude'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'edit-longitude',
      ],
      '#default_value' => (isset($_REQUEST['field_latitude_longitude_proximity']['source_configuration']['origin']['lon']) ? $_REQUEST['field_latitude_longitude_proximity']['source_configuration']['origin']['lon'] : '')
    ];

    $form['last_search'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'edit-last-search',
      ],
      '#default_value' => (isset($_REQUEST['location']) ? $_REQUEST['location'] : '')
    ];

    $route_name = \Drupal::routeMatch()->getRouteName();
    if ($route_name == 'view.job_postings.page_1') {
      $form['radius'] = [
        '#type' => 'select',
        '#default_value' => (isset($_REQUEST['field_latitude_longitude_proximity']['value']) ? $_REQUEST['field_latitude_longitude_proximity']['value'] : 50),
        '#options' => [
          10 => '10 miles',
          25 => '25 miles',
          50 => '50 miles',
          75 => '75 miles',
          100 => '100 miles',
        ],
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('FIND A JOB'),
      '#attributes' => [
        'class' => [
          'hidden-xs'
        ]
      ]
    ];

    if ($route_name == 'view.job_postings.page_1') {
      $form['field_years_of_experience'] = [
        '#type' => 'checkboxes',
        '#options' => [
         0 => 'Entry level jobs only',
        ],
        '#default_value' => (
          isset($_REQUEST['field_years_of_experience_value']) ?
          [$_REQUEST['field_years_of_experience_value']] : []
        ),
      ];
    }

    $form['submit-mobile'] = [
      '#type' => 'submit',
      '#value' => $this->t('FIND A JOB'),
      '#attributes' => [
        'class' => [
          'hidden-sm',
          'hidden-md',
          'hidden-lg',
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $search_text = trim($form_state->getValue('location'));
    $title = '';
    if (!empty($form_state->getValue('title'))) {
      $title = $form_state->getValue('title');
    }

    [$lat, $lon] = restorationjobs_companies_geocode_string($search_text);

    $options = [
      'query' => [
        'title' => $title,
        'location' => $search_text,
        'field_latitude_longitude_proximity[value]' => ($form_state->getValue('radius') != NULL ? $form_state->getValue('radius') :  '50'),
        'field_latitude_longitude_proximity[source_configuration][origin][lat]' => $lat,
        'field_latitude_longitude_proximity[source_configuration][origin][lon]' => $lon,
      ]
    ];

    if (isset($_REQUEST['field_years_of_experience'])) {
      $options['query']['field_years_of_experience_value'] = $form_state->getValue('field_years_of_experience')[0];
    }

    $form_state->setRedirect('view.job_postings.page_1', [], $options);

    // Recent Searches.
    $storage = \Drupal::service('user.private_tempstore')->get('recent_searches');
    $list = $storage->get('list');
    // Keep list with only 3 members.
    if ($list && count($list) == 3) {
      array_shift($list);
    }

    $hash = md5($title . '-' . $search_text);
    // Avoid duplicates but refresh the position.
    if ($list && array_key_exists($hash, $list)) {
      unset($list[$hash]);
    }

    $url = Url::fromRoute('view.job_postings.page_1', [], $options);

    $list[$hash] = [
      'location' => $search_text,
      'title' => $title,
      'url' => $url
    ];

    $storage->set('list', $list);
  }

}
