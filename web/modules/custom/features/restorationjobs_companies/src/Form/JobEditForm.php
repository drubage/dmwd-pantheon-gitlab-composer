<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;
use Drupal\Core\Render\Markup;

/**
 * Class JobEditForm.
 */
class JobEditForm extends ContentEntityForm {

  /**
   * The form builder service.
   *
   * Used to build the confirmation form on step 4.
   *
   * @var FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The form mode.
   *
   * @var string
   */
  protected $formMode;

  /**
   * @inheritDoc
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    FormBuilderInterface $formBuilder,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->formBuilder = $formBuilder;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('form_builder'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * @inheritDoc
   */
  protected function init(FormStateInterface $form_state) {
    parent::init($form_state);
    $form_display = EntityFormDisplay::load('job_posting.job_posting.job_edit');
    $this->setFormDisplay($form_display, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, JobPostingInterface $job_posting = NULL) {
    $form = parent::form($form, $form_state);
    $entity = $this->getEntity();
    // Pay information.
    $form['field_reveal_payment']['#attributes']['class'][] = 'col-md-6 col-sm-6 col-xs-12';
    $form['field_reveal_payment']['#suffix'] = '</div>';
    // Job Description box.
    $job_titles = $this->entity->get('field_job_title')->referencedEntities();
    $term = $job_titles[0];
    $default_description = $term->get('field_default_description')->value;
    if ($default_description) {
      $form['field_job_description']['widget'][0]['value']['#suffix'] = Markup::create('<div class="suggested-description"><button id="revert-description" class="btn btn-primary-outline">Revert</button> <button id="suggested-description" class="btn btn-primary">Suggested Description</button></div>');
      $form['field_job_description']['widget'][0]['value']['#attached'] = [
        'library' => [
          'restorationjobs_companies/job_post_default_descriptions',
        ],
        'drupalSettings' => array(
          'default_description' => $default_description,
        ),
      ];
    }

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    $element['submit']['#value'] = t('SAVE CHANGES');
    $element['submit']['#attributes'] = ['class' => ['btn', 'btn-primary']];
    return $element;
  }

  /**
   * @inheritDoc
   */
  public function save(array $form, FormStateInterface $form_state) {
    $returnValue = parent::save($form, $form_state);
    return $returnValue;
  }
}
