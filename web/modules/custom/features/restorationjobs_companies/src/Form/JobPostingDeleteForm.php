<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Job Posting entities.
 *
 * @ingroup restorationjobs_companies
 */
class JobPostingDeleteForm extends ContentEntityDeleteForm {


}
