<?php

namespace Drupal\restorationjobs_companies\Form;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\restorationjobs_companies\CompanyHelper;
use Drupal\group\Entity\Group;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\Element\StatusMessages;

/**
 * Class PostAJobWizardForm.
 *
 * This form is subclassed in order to allow for edit functions to be
 * handled by the default form and form mode; this form utilizes form modes
 * corresponding to various pages, as well as adding pseudo-preview and payment
 * method pages on the same route.
 */
class PostAJobWizardForm extends ContentEntityForm {

  /**
   * The form builder service.
   *
   * Used to build the confirmation form on step 4.
   *
   * @var FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Step to create custom form, e.g., skip standard form generation.
   *
   * @var integer
   */
  protected $step;

  /**
   * The form mode.
   *
   * @var string
   */
  protected $formMode;

  /**
   * Whether the employer has a payment method defined.
   *
   * @var boolean
   */
  protected $hasPaymentMethod;

  /**
   * @inheritDoc
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    FormBuilderInterface $formBuilder,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->formBuilder = $formBuilder;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('form_builder'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * @inheritDoc
   */
  protected function init(FormStateInterface $form_state) {
    // Avoid employers posting jobs while his company data is not filled.
    if (!$this->checkCompanyInfo()) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage(t('Please complete your company profile before posting a job.'));
      $url = \Drupal::url('restorationjobs_companies.employers_step2');
      $response = new RedirectResponse($url);
      $response->send();
      exit;
    }

    $this->setHasPaymentMethod();

    // Ensure we act on the translation object corresponding to the current form
    // language.
    $this->initFormLangcodes($form_state);
    $langcode = $this->getFormLangcode($form_state);
    $this->entity = $this->entity->hasTranslation($langcode)
      ? $this->entity->getTranslation($langcode)
      : $this->entity->addTranslation($langcode);

    // If there is a valid form mode specified, use it.
    $step = $this->getRouteMatch()->getParameter('step');
    if (!$form_display = EntityFormDisplay::load('job_posting.job_posting.step_' . $step)) {
      // Required for upstream functions, e.g. ::extractFormValues()
      $form_display = EntityFormDisplay::collectRenderDisplay($this->entity, $this->getOperation());
      $this->step = $step;
    }
    else {
      $this->formMode = $form_display->getMode();
    }
    $this->setFormDisplay($form_display, $form_state);
  }

  /**
   * Checks if the employer has a payment method.
   */
  private function setHasPaymentMethod() {
    $user = \Drupal::currentUser();
    $userInterface = $this->entityTypeManager->getStorage('user')->load($user->id());

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager
       ->getStorage('commerce_payment_gateway');
    $payment_method_storage = $this->entityTypeManager
      ->getStorage('commerce_payment_method');
    $payment_gateway = $payment_gateway_storage->loadForUser($userInterface);
    $payment_methods = $payment_method_storage->loadReusable($userInterface, $payment_gateway);

    $this->hasPaymentMethod = !empty($payment_methods);
  }

  /**
   * Check if user company information is filled.
   */
  private function checkCompanyInfo() {
    $group = NULL;
    $gids = CompanyHelper::getUserCompanies(\Drupal::currentUser()->id());
    if (count($gids) > 0) {
      $group = Group::load($gids[0]);
    }
    else {
      // No company attached.
      return FALSE;
    }

    // Check if company info is filled.
    $errors = $group->validate()->count();
    if ($errors) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Enforce link generation with nojs flag.
    if ($this->getRouteMatch()->getParameter('nojs') == 'undefined') {
      throw new BadRequestHttpException('Must generate route with nojs flag.');
    }
    if (!$this->step) {
      // Render the entity form as per usual.
      $form = parent::form($form, $form_state);
      if ($this->formMode == 'step_1') {
        $company = CompanyHelper::getUserCompanies();
        $form['field_company']['widget'][0]['target_id']['#default_value'] = \Drupal::entityTypeManager()->getStorage('group')->load($company[0]);
        $form['field_company']['#access'] = FALSE;
        $form_state->setValue('step', 1);
      }
      if ($this->formMode == 'step_2') {
        $job_titles = $this->entity->get('field_job_title')->referencedEntities();
        $term = $job_titles[0];
        $default_description = $term->get('field_default_description')->value;
        if ($default_description) {
          $form['field_job_description']['#suffix'] = Markup::create('<div class="suggested-description"><button id="revert-description" class="btn btn-primary-outline">Revert</button> <button id="suggested-description" class="btn btn-primary">Suggested Description</button></div>');
          $form['field_job_description']['#attached'] = [
            'library' => [
              'restorationjobs_companies/job_post_default_descriptions',
            ],
            'drupalSettings' => array(
              'default_description' => $default_description,
            ),
          ];
        }
        $form_state->setValue('step', 2);
      }
      return $form;
    }
    else {
      $form_state->setValue('step', $this->step);
      switch ($this->step) {
        case 3:
          $form = $this->step3($form, $form_state);
          if (!$this->hasPaymentMethod) {
            // Remove the submit button.
            $form['#process'][] = [self::class, 'removeActions'];
            $this->step3($form, $form_state);
          }

          return $form;
        case 4:
          return $this->step4($form, $form_state);
        default:
          throw new AccessDeniedHttpException('Invalid step.');
      }
    }
  }

  /**
   * Process function to remove the actions, e.g. if they're neutered.
   */
  public static function removeActions($element, FormStateInterface $form_state, $form) {
    $element['actions']['#access'] = FALSE;
    return $element;
  }

  /**
   * @inheritDoc
   */
  public function save(array $form, FormStateInterface $form_state) {
    $returnValue = parent::save($form, $form_state);
    if ($returnValue == SAVED_NEW) {
      // Send to step 2.
      $form_state->setRedirect(
        'entity.job_posting.wizard_form',
        ['job_posting' => $this->entity->id(), 'step' => 2]
      );
    }
    else if ($returnValue == SAVED_UPDATED && $this->formMode == 'step_1') {
      $form_state->setRedirect(
        'entity.job_posting.wizard_form',
        ['job_posting' => $this->entity->id(), 'step' => 2]
      );
    }
    else if ($returnValue == SAVED_UPDATED && $this->formMode == 'step_2') {
      $form_state->setRedirect(
        'entity.job_posting.wizard_form',
        ['job_posting' => $this->entity->id(), 'step' => 3]
      );
    }
    // Step 3 has its own action links.
    // Step 4 has an ajax submit.
    return $returnValue;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $skipAddPayment = $this->hasPaymentMethod && ($this->step == 3);
    // The entity will get built above; if we're on step 4, store the payment
    // method, which only gets passed to the form state, it's not a widget.
    if ($this->step == 4) {
      $this->processPaymentInput($form, $form_state, $this->entity);
    }
    elseif ($skipAddPayment) {
      $methods = \Drupal::entityTypeManager()
        ->getStorage('commerce_payment_method')
        ->loadByProperties(['uid' => \Drupal::currentUser()->id()]);
      $payment_method = reset($methods);
        // Get the first payment method as the user will only have one.
      $this->entity->set('initial_payment_method', $payment_method);
    }
  }

  /**
   * Process the payment input.
   */
  protected function processPaymentInput(array $form, FormStateInterface $form_state, ContentEntityInterface $entity) {
    $values = $form_state->getValue('payment_information');
    $selected_option = $form['payment_information']
      ['payment_method']
      [$values['payment_method']];
    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this
      ->entityTypeManager->getStorage('commerce_payment_gateway');
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $payment_gateway_storage->load($selected_option['#payment_gateway']);
    if (!$payment_gateway) {
      return;
    }

    $payment_method = NULL;
    if (!empty($selected_option['#payment_method_type']) && !empty($values['add_payment_method'])) {
      // The payment method was just created.
      $payment_method = $values['add_payment_method'];
    }
    else {
      /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
      $payment_method_storage = $this
        ->entityTypeManager->getStorage('commerce_payment_method');
      $payment_method = $payment_method_storage->load($selected_option['#payment_method']);
    }
    // @todo - Set payment gateway when we support more than one.
    $entity->set('initial_payment_method', $payment_method);
  }

  /**
   * Render "form" for step 3, which is really just a preview.
   */
  protected function step3(array $form, FormStateInterface $formState) {
    // @todo - Implement preview.
    $entity = \Drupal::entityTypeManager()->getStorage('job_posting')->load($this->entity->id());
    $build = \Drupal::entityTypeManager()->getViewBuilder('job_posting')->view($entity);
    $form['preview'] = [
      '#markup' => render($build)
    ];
    $form['links'] = [
      '#prefix' => '<div class="form-actions">',
      'previous' => $this->getStepLink($this->t('Back to Edit'), 1, 'prev')->toRenderable(),
      '#suffix' => '</div>',
    ];

    if (!$this->hasPaymentMethod) {
      $form['links']['next'] = $this->getStepLink($this->t('Continue'), 4, 'next')->toRenderable();
    }
    return $form;
  }

  /**
   * Render form for Step 4, which is a payment element combined with a modal
   * call for finalization.
   */
  protected function step4(array $form, FormStateInterface $formState) {
    if ($this->hasPaymentMethod) {
      $url = \Drupal\Core\Url::fromRoute('entity.job_posting.wizard_form', [
        'job_posting' => $this->entity->id(),
        'step' => 3
      ])->toString();
      $response = new RedirectResponse($url);
      $response->send();
    }
    else {
      $this->addPaymentMethodElement($form, $formState);
    }

    $form['#prefix'] = '<div id="add-payment-method-wrapper"><div class="payment-description">
      For a limited time your postings are free!</div>';
    $form['#suffix'] = '</div>';
    return $form;
  }

  /**
   * @inheritDoc
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    $skipAddPayment = $this->hasPaymentMethod && ($this->step == 3);

    if ($this->step == 4 || $skipAddPayment) {
      // The submit element on the final step fires a modal/alternative save
      // pathway.
      // Remove save handler; we'll handle it on our confirmation form.
      $element['submit']['#ajax'] = [
        'callback' => [$this, 'ajaxSubmit'],
        'event' => 'rj:submit',
      ];
      $element['submit']['#attached']['library'][] = 'restorationjobs_companies/job_post_submit_handler';
      // Overwrite default submit button.
      if ($skipAddPayment) {
        $element['submit']['#value'] = t('Post Job and Go Live');
        $element['submit']['#attributes']['class'][] = 'btn-primary-blue';
      }
    }
    return $element;
  }

  /**
   * Attach a payment method element on to the form.
   *
   * Thanks Big Island Fish :-)
   */
  protected function addPaymentMethodElement(array &$form, FormStateInterface $form_state) {
    return;
    $element = ['#weight' => 0];
    /** @var \Drupal\restorationjobs_companies\Entity\JobPostingInterface $entity */
    $entity = $this->entity;
    $billing_countries = $entity->getStore()->getBillingCountries();
    /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
    $payment_method_storage = $this->entityTypeManager
      ->getStorage('commerce_payment_method');
    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager
      ->getStorage('commerce_payment_gateway');
    $payment_gateway = $payment_gateway_storage->loadForUser($entity->getOwner());
    if (!($payment_gateway->getPlugin() instanceof SupportsStoredPaymentMethodsInterface)) {
      throw new \Exception('Default payment gateway not supported.');
    }
    $payment_methods = $payment_method_storage->loadReusable($entity->getOwner(), $payment_gateway, $billing_countries);

    foreach ($payment_methods as $payment_method_id => $payment_method) {
      $option_id = $payment_method_id;
      $options[$option_id] = [
        'id' => $option_id,
        'label' => $payment_method->label(),
        'payment_gateway' => $payment_gateway->id(),
        'payment_method' => $payment_method_id,
      ];
    }
    $payment_method_type_counts = [];
    $payment_method_types = $payment_gateway->getPlugin()->getPaymentMethodTypes();
    foreach ($payment_method_types as $payment_method_type_id => $payment_method_type) {
      $previous_count = 0;
      if (isset($payment_method_type_counts[$payment_method_type_id])) {
        $previous_count = $payment_method_type_counts[$payment_method_type_id];
      };
      $payment_method_type_counts[$payment_method_type_id] = $previous_count + 1;
    }
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $payment_method_types = $payment_gateway_plugin->getPaymentMethodTypes();
    foreach ($payment_method_types as $payment_method_type_id => $payment_method_type) {
      $option_id = 'new--' . $payment_method_type_id . '--' . $payment_gateway->id();
      $option_label = $payment_method_type->getCreateLabel();
      if ($payment_method_type_counts[$payment_method_type_id] > 1) {
        // Append the payment gateway label to avoid duplicate labels.
        $option_label = $this->t('@payment_method_label (@payment_gateway_label)', [
          '@payment_method_label' => $payment_method_type->getCreateLabel(),
          '@payment_gateway_label' => $payment_gateway_plugin->getDisplayLabel(),
        ]);
      }

      $options[$option_id] = [
        'id' => $option_id,
        'label' => $option_label,
        'payment_gateway' => $payment_gateway->id(),
        'payment_method_type' => $payment_method_type_id,
      ];
    }
    $values = $form_state->getUserInput();
    $default_option = NULL;
    if (!empty($values['payment_information']['payment_method'])) {
      // The form was rebuilt via AJAX, use the submitted value.
      $default_option = $values['payment_information']['payment_method'];
    }
    else {
      $default_option = count($options) ? array_keys($options)[0] : NULL;
    }
    // Prepare the form for ajax.
    $element['#tree'] = TRUE;
    $element['#wrapper_id'] = Html::getUniqueId('payment-information-wrapper');
    $element['#prefix'] = '<div id="' . $element['#wrapper_id'] . '">';
    $element['#suffix'] = '</div>';
    // Core bug #1988968 doesn't allow the payment method add form JS to depend
    // on an external library, so the libraries need to be preloaded here.
    if ($js_library = $payment_gateway->getPlugin()->getJsLibrary()) {
      $element['#attached']['library'][] = $js_library;
    }
    $element['payment_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment method'),
      '#options' => array_column($options, 'label', 'id'),
      '#default_value' => $default_option,
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxRefresh'],
        'wrapper' => $element['#wrapper_id'],
      ],
    ];

    // Store the values for submission.
    foreach ($options as $option_id => $option) {
      $element['payment_method'][$option_id]['#payment_gateway'] = $option['payment_gateway'];
      if (isset($option['payment_method'])) {
        $element['payment_method'][$option_id]['#payment_method'] = $option['payment_method'];
      }
      if (isset($option['payment_method_type'])) {
        $element['payment_method'][$option_id]['#payment_method_type'] = $option['payment_method_type'];
      }
    }

    $selected_option = $element['payment_method'][$default_option];
    if (!empty($selected_option['#payment_method_type'])) {
      $payment_method = $payment_method_storage->create([
        'type' => $selected_option['#payment_method_type'],
        'payment_gateway' => $selected_option['#payment_gateway'],
        'uid' => $entity->getOwner(),
      ]);

      $element['add_payment_method'] = [
        '#type' => 'commerce_payment_gateway_form',
        '#operation' => 'add-payment-method',
        '#default_value' => $payment_method,
      ];
    }

    $form['payment_information'] = $element;
  }

  /**
   * Uniform method to get a link to a step.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   *   Title.
   * @param $step
   *   The desired step.
   *
   * @return \Drupal\Core\Link
   */
  protected function getStepLink(TranslatableMarkup $title, $step, $type = 'next') {
    if ($type == 'next') {
      $btn_class = ['btn', 'btn-primary', 'btn-next'];
    } else {
      $btn_class = ['btn', 'btn-default', 'btn-prev'];
    }
    return Link::createFromRoute(
      $title,
      'entity.job_posting.wizard_form',
      ['step' => $step, 'job_posting' => $this->entity->id()],
      ['attributes' => ['class' => $btn_class]]
    );
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Ajax submit callback.
   */
  public function ajaxSubmit(array $form, FormStateInterface $form_state) {
    // @todo - Do some validation for completeness?
    $response = new AjaxResponse();

    $this->validateForm($form, $form_state);
    if ($errors = $form_state->getErrors()) {
      \Drupal::logger('form errors')->notice('<pre>' . print_r($errors, TRUE));
      //add-payment-method-wrapper
      $messages = StatusMessages::renderMessages('error');

      $response->addCommand(
        new PrependCommand('.region-content', $messages)
      );

      $response->addCommand(
        new HtmlCommand('#add-payment-method-wrapper', $form)
      );
    }
    else {
      $this->save($form, $form_state);
      $response->addCommand(
        new OpenModalDialogCommand($this->t('Hold Up'), $this->confirmModalContent())
      );
    }

    return $response;
  }

  /**
   * Generate the confirmation modal.
   *
   * @return array
   *   Render array
   */
  protected function confirmModalContent() {
    $modal['prefix'] = [
      '#markup' => '<p>' . $this->t('You have entered the following job information:') . '</p>',
      '#weight' => -1,
    ];
    $viewBuilder = $this->entityTypeManager->getViewBuilder($this->entity->getEntityTypeId());
    $modal['preview'] = [
      'entity' => $viewBuilder->view($this->entity, 'confirm_preview'),
      '#weight' => 0,
    ];
    $modal['disclaimer'] = [
      '#prefix' => '<div>',
      '#markup' => $this->t('Once you post the job, you will not be able to edit this information.'),
      '#suffix' => '</div>',
      '#weight' => 1,
    ];
    $modal['actions']['#weight'] = 10;
    $edit_link = Link::fromTextAndUrl(
      $this->t('Edit'),
      $this->entity->toUrl('wizard-form')->setRouteParameter('step', 1)
    )->toRenderable();
    $edit_link['#attributes'] = ['class' => ['btn', 'btn-white-outline', 'btn-back']];
    $modal['actions']['edit'] = $edit_link;
    $confirm_link = Link::fromTextAndUrl(
      $this->t('Confirm Posting'),
      // This path is protected by CSRF.
      $this->entity->toUrl('confirm'),
      ['attributes' => ['class' => ['btn', 'btn-primary']]]
    )->toRenderable();
    $confirm_link['#attributes'] = ['class' => ['btn', 'btn-white-primary']];
    $modal['actions']['confirm'] = $confirm_link;
    $modal['actions']['#prefix'] = '<div class="modal-actions">';
    $modal['actions']['#suffix'] = '</div>';
    $modal['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $modal;
  }
}
