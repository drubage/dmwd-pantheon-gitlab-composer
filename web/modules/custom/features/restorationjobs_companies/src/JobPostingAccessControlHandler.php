<?php

namespace Drupal\restorationjobs_companies;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Job Posting entity.
 *
 * @see \Drupal\restorationjobs_companies\Entity\JobPosting.
 */
class JobPostingAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\restorationjobs_companies\Entity\JobPostingInterface $entity */
    switch ($operation) {
      case 'view':
        $user_ip = \Drupal::request()->getClientIp();
        $user = \Drupal::currentUser();
        // Log the entering of the user on a job posting page
        restorationjobs_companies_save_entity_log($user->id(),'job_posting',$entity->id(),$user_ip);
        if (!$entity->isPublished()) {
          if ($user->id() == $entity->getOwnerId()) {
            return AccessResult::allowed();
          } else {
            return AccessResult::allowedIfHasPermission($account, 'view unpublished job posting entities');
          }
        }
        // Check that job is verified
        if ($entity->get('state')->getString() != 'active') {
          if ($user->id() != $entity->getOwnerId()) {
            return AccessResult::allowedIfHasPermission($account, 'view unpublished job posting entities');
          } else {
            return AccessResult::forbidden();
          }
        }
        return AccessResult::allowedIfHasPermission($account, 'view published job posting entities');

      case 'update':
        $isOwner = $entity->getOwnerId() === $account->id();
        if ($isOwner) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIfHasPermission($account, 'administer job posting entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete job posting entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job posting entities');
  }

}
