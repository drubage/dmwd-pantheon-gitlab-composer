<?php

namespace Drupal\restorationjobs_companies;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Job Posting entities.
 *
 * @ingroup restorationjobs_companies
 */
class JobPostingListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Job Posting ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\restorationjobs_companies\Entity\JobPosting */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.job_posting.edit_form',
      ['job_posting' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
