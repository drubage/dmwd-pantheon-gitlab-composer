<?php

namespace Drupal\restorationjobs_companies;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;

/**
 * Defines the storage handler class for Job Posting entities.
 *
 * This extends the base storage class, adding required special handling for
 * Job Posting entities.
 *
 * @ingroup restorationjobs_companies
 */
class JobPostingStorage extends SqlContentEntityStorage implements JobPostingStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(JobPostingInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {job_posting_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {job_posting_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(JobPostingInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {job_posting_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('job_posting_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
