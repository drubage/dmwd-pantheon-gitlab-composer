<?php

namespace Drupal\restorationjobs_companies;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\restorationjobs_companies\Entity\JobPostingInterface;

/**
 * Defines the storage handler class for Job Posting entities.
 *
 * This extends the base storage class, adding required special handling for
 * Job Posting entities.
 *
 * @ingroup restorationjobs_companies
 */
interface JobPostingStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Job Posting revision IDs for a specific Job Posting.
   *
   * @param \Drupal\restorationjobs_companies\Entity\JobPostingInterface $entity
   *   The Job Posting entity.
   *
   * @return int[]
   *   Job Posting revision IDs (in ascending order).
   */
  public function revisionIds(JobPostingInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Job Posting author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Job Posting revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\restorationjobs_companies\Entity\JobPostingInterface $entity
   *   The Job Posting entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(JobPostingInterface $entity);

  /**
   * Unsets the language for all Job Posting with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
