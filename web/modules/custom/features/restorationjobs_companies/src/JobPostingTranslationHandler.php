<?php

namespace Drupal\restorationjobs_companies;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for job_posting.
 */
class JobPostingTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
