<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'CreditCardHeaderBlock' block.
 *
 * @Block(
 *  id = "credit_card_header_block",
 *  admin_label = @Translation("Credit Card Header Block"),
 * )
 */
class CreditCardHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();
    $paymentMethod = \Drupal::entityTypeManager()
      ->getStorage('commerce_payment_method')
      ->loadByProperties(['uid' => $uid]);
    $paymentMethod = reset($paymentMethod);
    $card = $paymentMethod->get('card_number')->getString();

    return [
      '#theme' => 'credit_card_header_block',
      '#card' => [
        'number' => $card,
        'type' => 'Visa'
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
