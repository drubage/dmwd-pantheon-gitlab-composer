<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'EmployerAccountSummaryHeaderBlock' block.
 *
 * @Block(
 *  id = "employer_account_summary_header",
 *  admin_label = @Translation("Employer Account Summary Header Block"),
 * )
 */
class EmployerAccountSummaryHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
    $name = $user->get('field_first_name')->getString() . ' ' . $user->get('field_last_name')->getString();

    return [
      '#theme' => 'employer_account_summary_header',
      '#user' => [
        'name' => $name
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
