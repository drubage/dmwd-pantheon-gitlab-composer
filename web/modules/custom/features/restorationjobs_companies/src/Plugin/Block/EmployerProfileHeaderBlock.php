<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'EmployerProfileHeaderBlock' block.
 *
 * @Block(
 *  id = "employer_profile_header_block",
 *  admin_label = @Translation("Employer Profile Header block"),
 * )
 */
class EmployerProfileHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
    $name = $user->get('field_first_name')->getString() . ' ' . $user->get('field_last_name')->getString();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $uid]);
    $company = reset($company);

    $profile = [
      'name' => $name,
    ];

    $route = \Drupal::routeMatch()->getRouteName();
    if ($route == 'restorationjobs_applicants.applicant_profile') {
      $profile['edit'] = TRUE;
    }
    else if ($route == 'restorationjobs_users.account_edit' && !empty($company)) {
      $profile['employer_account_edit'] = TRUE;
    }

    if (!empty($company)) {
      $profile['company'] = $company->label();
    }

    return [
      '#theme' => 'employer_profile_header',
      '#profile' => $profile,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowed();
  }

}
