<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'JobEditHeaderBlock' block.
 *
 * @Block(
 *  id = "job_edit_header_block",
 *  admin_label = @Translation("Job edit header block"),
 * )
 */
class JobEditHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $parameters = \Drupal::routeMatch()->getParameters();
    $job_posting = $parameters->get('job_posting');
    $build = entity_view($job_posting, 'job_edit_header');
    return [
      '#type' => 'markup',
      '#markup' => drupal_render($build),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return \Drupal::routeMatch()->getParameter('job_posting') ?
      AccessResult::allowed() : AccessResult::forbidden();
  }

}
