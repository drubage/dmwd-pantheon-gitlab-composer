<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;

/**
 * Provides a 'JobPostHeaderBlock' block.
 *
 * @Block(
 *  id = "job_create_header_block",
 *  admin_label = @Translation("Job post header block"),
 * )
 */
class JobPostHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'job_post_header_block',
      '#header' => $this->getHeader(),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Return header contents based on current step.
   */
  public function getHeader() {
    $header = [];
    $jobPosting = \Drupal::routeMatch()->getParameter('job_posting');

    if (!$jobPosting) {
      $header = $this->getStep1();
    }
    else {
      $header = $this->getByStep($jobPosting);
    }

    return $header;
  }

  /**
   * First step that doen't have the job posting yet.
   */
  private function getStep1() {
    $myJobs = Url::fromRoute('restorationjobs_applicants.homepage');

    return [
      'myJobs' => $myJobs,
      'step' => 1
    ];
  }

  /**
   * Get header content by step.
   */
  private function getByStep($jobPosting) {
    $step = \Drupal::routeMatch()->getParameter('step');
    $build = entity_view($jobPosting, 'job_edit_header');

    return [
      'step' => $step,
      'content' => drupal_render($build),
    ];
  }

}
