<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;

/**
 * Provides a 'JobSearchHeaderBlock' block.
 *
 * @Block(
 *  id = "job_search_header_block",
 *  admin_label = @Translation("Job search header block"),
 * )
 */
class JobSearchHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'job_search_header_block',
      '#header' => $this->getHeader(),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Return header contents.
   */
  public function getHeader() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\restorationjobs_companies\Form\FindAJobForm');

    return [
      'form' => $form
    ];
  }
}
