<?php

namespace Drupal\restorationjobs_companies\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'JobViewHeaderBlock' block.
 *
 * @Block(
 *  id = "job_view_header_block",
 *  admin_label = @Translation("Job view header block"),
 * )
 */
class JobViewHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $parameters = \Drupal::routeMatch()->getParameters();
    $job_posting = $parameters->get('job_posting');
    $user = \Drupal::currentUser();
    $is_owner = $job_posting->getOwnerId() === $user->Id();
    if ($is_owner) {
      $view_mode = 'job_edit_header';
    } else {
      $view_mode = 'job_apply_header';
    }
    $build = entity_view($job_posting, $view_mode);
    return [
      '#type' => 'markup',
      '#markup' => drupal_render($build),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $routeName = \Drupal::routeMatch()->getRouteName();
    $isApplicationPage = $routeName == 'restorationjobs_applicants.job_application_owner_profile';
    $isJobPage = \Drupal::routeMatch()->getParameter('job_posting');

    return $isJobPage && !$isApplicationPage ?
      AccessResult::allowed() : AccessResult::forbidden();
  }

}
