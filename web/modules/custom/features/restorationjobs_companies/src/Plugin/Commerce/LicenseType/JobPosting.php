<?php

namespace Drupal\restorationjobs_companies\Plugin\Commerce\LicenseType;

use Drupal\commerce_license\Entity\LicenseInterface;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\GrantedEntityLockingInterface;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\LicenseTypeBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides a license type which authorizes a job posting.
 *
 * @CommerceLicenseType(
 *   id = "job_posting",
 *   label = @Translation("Job Posting"),
 * )
 */
class JobPosting extends LicenseTypeBase implements GrantedEntityLockingInterface {

  /**
   * {@inheritDoc}
   */
  public function alterEntityOwnerForm(&$form, FormStateInterface $form_state, $form_id, LicenseInterface $license, EntityInterface $form_entity) {
    if ($form_entity->getEntityTypeId() != 'job_posting') {
      // Only act on a job posting form.
      return;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function buildLabel(LicenseInterface $license) {
    $args = [];
    if ($license->license_job_posting->entity) {
      $args['@posting'] = $license->license_job_posting->entity->label();
    } else {
      $args['@posting'] = "Job Posting Deleted";
    }
    return $this->t('@posting job posting license', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function grantLicense(LicenseInterface $license) {
    // TODO: Bump job status.
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLicense(LicenseInterface $license) {
    // TODO: Bump job status.
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['license_job_posting'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Job Posting'))
      ->setDescription(t('The job posting.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'job_posting')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 1,
        'settings' => [
          'link' => TRUE,
        ],
      ]);

    return $fields;
  }

}
