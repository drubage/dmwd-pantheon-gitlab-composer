<?php

namespace Drupal\restorationjobs_companies\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\Core\Render\Markup;

/**
 * Custom FindAJobFormViewsHeader form for views headers.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("find_a_job_form_views_header")
 */
class FindAJobFormViewsHeader extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {

    $find_a_job_form = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_companies\Form\FindAJobForm');

    $script = "<script>
    function initialize() {
        var input = document.getElementById('edit-location');
        autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('edit-latitude').value = place.geometry.location.lat();
            document.getElementById('edit-longitude').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>";

    return [
      '#type' => 'markup',
      '#markup' => render($find_a_job_form),
      '#suffix' => Markup::create($script),
    ];
  }

}
