<?php
namespace Drupal\restorationjobs_companies\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Access\AccessResult;
use Drupal\user\UserInterface;
use Drupal\Core\Routing\RoutingEvents;

/**
 * Overwrite default user routing access check.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.job_posting.edit_form')) {
      $route->setRequirement('_custom_access',
      'Drupal\restorationjobs_companies\Routing\RouteSubscriber::restrictedAccess');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }

  /**
   * Restricted mode access check.
   */
  public static function restrictedAccess() {
    $user = \Drupal::currentUser();

    if ($user->hasPermission('administer site configuration')) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }
}
