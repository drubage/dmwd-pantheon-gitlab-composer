<?php

namespace Drupal\restorationjobs_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use CommerceGuys\Addressing\AddressFormat\AddressField;

/**
 * Class AccountUpdate.
 */
class AccountUpdate extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function profile() {
    $user = User::load(\Drupal::currentUser()->id());
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $user->id()]);

    $formObject = \Drupal::entityTypeManager()
      ->getFormObject('user', 'account_update')
      ->setEntity($user);

    return \Drupal::formBuilder()->getForm($formObject);
  }

}
