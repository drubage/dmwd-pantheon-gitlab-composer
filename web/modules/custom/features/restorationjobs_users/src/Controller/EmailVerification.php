<?php

namespace Drupal\restorationjobs_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\User;


/**
 * Class EmailVerification.
 */
class EmailVerification extends ControllerBase {

  /**
   *
   * @return array
   */
  public function send_verification_page() {
    return [
      '#theme' => 'email_verification_page',
    ];
  }

  public function finalize_registration() {
    $form = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_companies\Form\FinalizeRegistration');
    return [
      '#theme' => 'finalize_registration_page',
      '#form' => $form,
      '#cache' => [
        'max-age' => 0
      ]
    ];
  }

  public function resend_verification() {
    if ($id = \Drupal::currentUser()->id()) {
      $account = User::load($id);
    }
    else {
      $query = \Drupal::request()->query->all();
      $account = user_load_by_mail($query['email']);
    }

    _user_mail_notify('register_no_approval_required', $account);
    $url = \Drupal::url('restorationjobs_users.user_validate_email', [], [
      'query' => [
        'email' => $account->getEmail()
      ]
    ]);
    $messenger = \Drupal::messenger();
    $messenger->addMessage(t('Verification email sent.'));
    return new RedirectResponse($url);
  }

}
