<?php

namespace Drupal\restorationjobs_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Link;

/**
 * Class JobApplicationFlowController.
 */
class RegistrationFlowController extends ControllerBase {

  /**
   * Render the first step of the job application flow
   * {@inheritdoc}
   */
  public function register() {
    $user = \Drupal::currentUser();
    $build = '';
    if ($user->isAuthenticated()) {
      $account = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    } else {
      $account = \Drupal::entityTypeManager()->getStorage('user')->create(array());
    }
    if (!$user->isAuthenticated()) {
      if (isset($_REQUEST['type'])) {
        $type = $_REQUEST['type'];
        if ($type == 'applicant') {
          $formObject = \Drupal::entityTypeManager()
            ->getFormObject('user', 'applicant_register')
            ->setEntity($account);
          $build = \Drupal::formBuilder()->getForm($formObject);
        } else {
          $build = \Drupal::formBuilder()->getForm('Drupal\restorationjobs_companies\Form\EmployerSignupForm');
        }
      } else {
        $link = Link::fromTextAndUrl('some text', Url::fromUri('http://google.com', array('attributes' => array('target' => '_blank'))));

        $attributes = ['attributes' => ['class' => ['btn btn-primary']]];

        $options['attributes']['class'] = array('button', 'icon', 'icon-pdf');

        $markup = '<p>I want to:</p>';
        $options = ['attributes' => ['class' => ['btn btn-primary registration-type']]];
        $query = ['type' => 'applicant'];
        $markup .= Link::fromTextAndUrl(t('Find a Job'), Url::fromRoute('<current>', $query, $options))->toString();
        $query = ['type' => 'applicant'];
        $query = ['type' => 'employer'];
        $markup .= Link::fromTextAndUrl(t('Post a Job'), Url::fromRoute('<current>', $query, $options))->toString();

        $build = [
          '#type' => 'markup',
          '#markup' => Markup::create($markup),
          '#cache' => [
            'max-age' => 0
          ]
        ];
      }
    }
    return $build;
  }
}
