<?php

namespace Drupal\restorationjobs_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\restorationjobs_companies\AccessChecks;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\User;
use Drupal\Core\Url;

/**
 * Class UserValidateController.
 */
class UserValidateController extends ControllerBase {

  /**
   * Build.
   *
   * @return string
   *   Return Build string.
   */
  public function build() {
    $account = User::load(\Drupal::currentUser()->id());
    $user_type = 'applicant';
    if (AccessChecks::hasAccessToCompany()->isAllowed()) {
      $user_type = 'employer';
    }
    if ($account->hasRole('email_verified')) {
      if ($user_type == 'employer') {
        $url = \Drupal::url('restorationjobs_companies.employers_step2');
      } else {
        $url = \Drupal::url('restorationjobs_applicants.applicants_step2');
      }
      return new RedirectResponse($url);
    }

    $url = Url::fromRoute('restorationjobs_users.resend_verification', [], [
      'query' => [
        'email' => \Drupal::request()->query->get('email')
      ]
    ]);

    $build = [
      '#theme' => 'validate_email',
      '#user_type' => $user_type,
      '#resend_url' => $url,
      '#cache' => [ // NEED TO REMOVE THIS WHEN GOING LIVE
        'max-age' => 0
      ],
    ];

    return $build;
  }

}
