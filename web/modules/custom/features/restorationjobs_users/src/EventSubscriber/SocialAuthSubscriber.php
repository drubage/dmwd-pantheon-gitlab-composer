<?php

namespace Drupal\restorationjobs_users\EventSubscriber;

use Drupal\social_auth\AuthManager\OAuth2ManagerInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Event\UserFieldsEvent;
use Drupal\social_auth\Event\UserEvent;
use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth\SocialAuthDataHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts on Social Auth events.
 */
class SocialAuthSubscriber implements EventSubscriberInterface {

  /**
   * The data handler.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  private $dataHandler;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  private $networkManager;

  /**
   * The provider auth manager.
   *
   * @var \Drupal\social_auth\AuthManager\OAuth2ManagerInterface
   */
  private $providerAuth;

  /**
   * SocialAuthSubscriber constructor.
   *
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   Used to manage session variables.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of the social auth implementer network plugin.
   * @param \Drupal\social_auth\AuthManager\OAuth2ManagerInterface $providerAuth
   *   Used to get the provider auth manager.
   */
  public function __construct(SocialAuthDataHandler $data_handler,
                              NetworkManager $network_manager,
                              OAuth2ManagerInterface $providerAuth) {

    $this->dataHandler = $data_handler;
    $this->networkManager = $network_manager;
    $this->providerAuth = $providerAuth;
  }

  /**
   * {@inheritdoc}
   *
   * Returns an array of event names this subscriber wants to listen to.
   * For this case, we are going to subscribe for user creation and login
   * events and call the methods to react on these events.
   */
  public static function getSubscribedEvents() {
    $events[SocialAuthEvents::USER_FIELDS] = ['onUserFields'];
    $events[SocialAuthEvents::USER_CREATED] = ['onUserCreated'];

    return $events;
  }

  /**
   * Set first and last name when registering with LinkedIn.
   *
   * @param \Drupal\social_auth\Event\UserFieldsEvent $event
   *   The Social Auth user fields event object.
   */
  public function onUserFields(UserFieldsEvent $event) {

    $userInfo = $this->providerAuth->getUserInfo();
    $fields = $event->getUserFields();

    $fields['field_first_name'] = $userInfo->getFirstName();
    $fields['field_last_name'] = $userInfo->getLastName();
    $event->setUserFields($fields);
  }

  /**
   * Grant the 'Email Verified' role when registering with LinkedIn.
   *
   * @param \Drupal\social_auth\Event\UserEvent $event
   *   The Social Auth user event object.
   */
  public function onUserCreated(UserEvent $event) {

    /*
     * @var \Drupal\user\UserInterface $user
     *
     * For all available methods, see User class
     * @see https://api.drupal.org/api/drupal/core!modules!user!src!Entity!User.php/class/User
     */
    $user = $event->getUser();
    $user->addRole('email_verified');
    $user->save();
    $_SESSION['social_registration'] = TRUE;

    $messenger = \Drupal::messenger();
    $messenger->addMessage("Your Restoration Jobs account has been created.");
  }

}