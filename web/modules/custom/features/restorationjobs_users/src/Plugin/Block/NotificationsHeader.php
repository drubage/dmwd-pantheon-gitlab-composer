<?php

namespace Drupal\restorationjobs_users\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'NotificationsHeader' block.
 *
 * @Block(
 *  id = "notifications_header",
 *  admin_label = @Translation("Notifications Header Block"),
 * )
 */
class NotificationsHeader extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = \Drupal::currentUser();
    $company = \Drupal::entityTypeManager()->getStorage('group')->loadByProperties(['uid' => $user->id()]);

    if ($company) {
      $text = t('Profile');
      $link = Url::fromRoute('restorationjobs_applicants.applicant_profile');
    }
    else {
      $text = t('My Jobs');
      $link = Url::fromRoute('restorationjobs_applicants.homepage');
    }

    return [
      '#theme' => 'notifications_header',
      '#header' => [
        'text' => $text,
        'link' => $link,
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
