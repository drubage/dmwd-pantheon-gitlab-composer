<?php

namespace Drupal\restorationjobs_users\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SocialAuthBlock' block.
 *
 * @Block(
 *  id = "social_auth_block",
 *  admin_label = @Translation("Social Auth Block"),
 * )
 */
class SocialAuthBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'social_auth_block',
      '#route_name' => \Drupal::routeMatch()->getRouteName(),
      '#job_posting' => (isset($_REQUEST['job']) ? $_REQUEST['job'] : ''),
      '#type' => (isset($_REQUEST['type']) ? $_REQUEST['type'] : ''),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
