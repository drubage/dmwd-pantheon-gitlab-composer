<?php
namespace Drupal\restorationjobs_users\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Access\AccessResult;
use Drupal\user\UserInterface;
use Drupal\Core\Routing\RoutingEvents;

/**
 * Overwrite default user routing access check.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.register')) {
      $route->setDefault('_controller', 'Drupal\restorationjobs_users\Controller\RegistrationFlowController::register');
    }

    if ($route = $collection->get('entity.user.canonical')) {
      $route->setRequirement('_custom_access',
      'Drupal\restorationjobs_users\Routing\RouteSubscriber::access');
    }

    // Job application listing.
    $list = [
      'view.job_applicants.page_1',
      'view.job_applicants.page_2',
      'view.job_applicants.page_3',
    ];

    foreach($list as $page) {
      if ($route = $collection->get($page)) {
        $route->setRequirement('_custom_access',
          'Drupal\restorationjobs_users\Routing\RouteSubscriber::appAccess');
      }
    }

    $this->restricted($collection);
  }

  /**
   * Allow only users and admin to see users profile page.
   */
  public static function access(UserInterface $user) {
    if (\Drupal::currentUser()->id() == $user->id()) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Application list access.
   */
  public static function appAccess() {
    $jib = \Drupal::routeMatch()->getParameter('arg_0');
    $job_posting = \Drupal::entityTypeManager()
      ->getStorage('job_posting')->load($jib);

    if (\Drupal::currentUser()->id() === $job_posting->getOwnerId()) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }

  /**
   * Restricted view mode.
   */
  private function restricted(RouteCollection $collection) {
    $config = \Drupal::config('restorationjobs_applicants.settings');
    $isRestricted = $config->get('restorationjobs_applicants.restricted_mode');
    if ($isRestricted) {
      // Bypass custom access check for those routes.
      $bypassRoutes = [
        // Login and register.
        '/user/register',
        '/user/login',
        '/user/logout',
        '/user/reset/{uid}/{timestamp}/{hash}/login',
        '/user/reset/{uid}',
        '/user/reset/{uid}/{timestamp}/{hash}',
        '/user/password',
        '/user/{user}',
        '/validate-account',
        // Pages
        '/home',
        '/my-jobs',
        '/my-jobs/pending/{arg_0}',
        '/my-jobs/closed/{arg_0}',
        '/profile',
        '/profile/edit',
        '/post-job',
        '/post-job/{job_posting}/{step}',
        '/post-job/{job_posting}/confirm',
        '<front>',
        '/jobs'
      ];

      $routes = $collection->all();

      foreach($routes as $route) {
        if (!in_array($route->getPath(), $bypassRoutes)) {
          $route->setRequirement('_custom_access',
          'Drupal\restorationjobs_users\Routing\RouteSubscriber::restrictedAccess');
        }
      }
    }
  }

  /**
   * Restricted mode access check.
   */
  public static function restrictedAccess() {
    $user = \Drupal::currentUser();
    $route = \Drupal::routeMatch();
    $currentPath = $route->getRouteObject()->getPath();
    $currentUser = \Drupal::currentUser();
    $userId = $currentUser->id();

    if ($user->hasPermission('administer site configuration')) {
      return AccessResult::allowed();
    }

    switch($currentPath) {
      // only owner can see.
      case '/job/{job_posting}':
      case '/job/{job_posting}/edit':
        $jobPosting = $route->getParameter('job_posting');
        return $userId == $jobPosting->getOwnerId() ?
          AccessResult::allowed() : AccessResult::forbidden();
    }

    return AccessResult::forbidden();
  }
}
