<?php

namespace Drupal\restorationjobs_job_posting_order_log\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\restorationjobs_job_posting_order_log\JobPostingOrderLogInterface;
use Drupal\user\UserInterface;

/**
 * Defines the job posting order log entity class.
 *
 * @ContentEntityType(
 *   id = "job_posting_order_log",
 *   label = @Translation("Job Posting Order Log"),
 *   label_collection = @Translation("Job Posting Order Logs"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\restorationjobs_job_posting_order_log\JobPostingOrderLogListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\restorationjobs_job_posting_order_log\Form\JobPostingOrderLogForm",
 *       "edit" = "Drupal\restorationjobs_job_posting_order_log\Form\JobPostingOrderLogForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "job_posting_order_log",
 *   admin_permission = "administer job posting order log",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/job-posting-order-log/add",
 *     "canonical" = "/job_posting_order_log/{job_posting_order_log}",
 *     "edit-form" = "/admin/content/job-posting-order-log/{job_posting_order_log}/edit",
 *     "delete-form" = "/admin/content/job-posting-order-log/{job_posting_order_log}/delete",
 *     "collection" = "/admin/content/job-posting-order-log"
 *   },
 *   field_ui_base_route = "entity.job_posting_order_log.settings"
 * )
 */
class JobPostingOrderLog extends ContentEntityBase implements JobPostingOrderLogInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new job posting order log entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the job posting order log author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the job posting order log was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the job posting order log was last edited.'));

    $fields['job_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Job Posting'))
      ->setRequired(TRUE)
      ->setDescription(t('The job posting id related with this log'))
      ->setSetting('target_type', 'job_posting');

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Commerce order'))
      ->setDescription(t('Commerce order id.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'commerce_order');

    $fields['card_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Credit card number'))
      ->setDescription(t('Credit card number (last four).'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ]);

    $fields['log_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Log type'))
      ->setDescription(t('Available types: Opened, Renewed, Closed, Reopened'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ]);

    $fields['price'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Order price'))
      ->setDescription(t('The total price of the order'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'commerce_price_default',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'commerce_price_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
