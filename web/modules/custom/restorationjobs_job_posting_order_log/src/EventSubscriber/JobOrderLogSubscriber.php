<?php

namespace Drupal\restorationjobs_job_posting_order_log\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_recurring\Event\RecurringEvents;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Resolver\OrderTypeResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\restorationjobs_job_posting_order_log\Entity\JobPostingOrderLog;
use Drupal\restorationjobs_job_posting_order_log\JobOrderLogger;

class JobOrderLogSubscriber implements EventSubscriberInterface {

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order type resolver.
   *
   * @var \Drupal\commerce_order\Resolver\OrderTypeResolverInterface
   */
  protected $orderTypeResolver;

  /**
   * @inheritDoc
   */
  public function __construct(
    CartManagerInterface $cartManager,
    CartProviderInterface $cartProvider,
    EntityTypeManagerInterface $entityTypeManager,
    OrderTypeResolverInterface $orderTypeResolver
  ) {
    $this->cartManager = $cartManager;
    $this->cartProvider = $cartProvider;
    $this->entityTypeManager = $entityTypeManager;
    $this->orderTypeResolver = $orderTypeResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RecurringEvents::SUBSCRIPTION_UPDATE] = 'orderRenew';
    return $events;
  }

  /**
   * Add new log on a job being renewed.
   */
  public function orderRenew($event) {
    $subscription = $event->getSubscription();
    // Skip first order as it gets a Opened log.
    if (count($subscription->getOrderIds()) > 1) {
      $order = $subscription->getCurrentOrder();
      $initialOrder = $subscription->getInitialOrder();

      $job = NULL;
      foreach($initialOrder->getItems() as $item) {
        if ($item->hasField('field_job_posting')) {
          $job = $item->get('field_job_posting')->entity;
        }
      }

      if ($job) {
        $paymentMethod = $job->getPaymentMethod();
        $uid = \Drupal::currentUser()->id();
        $log = JobPostingOrderLog::create([
          'uid' => $uid,
          'job_id' => $job->id(),
          'order_id' => $order->id(),
          'card_number' => $paymentMethod->card_number->getString(),
          'log_type' => JobOrderLogger::JOB_ORDER_TYPE_RENEWED,
          'price' => $order->getTotalPrice()
          ]);
          $log->save();
        }
      }
    }
  }
