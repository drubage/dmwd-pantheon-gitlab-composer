<?php

namespace Drupal\restorationjobs_job_posting_order_log\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the job posting order log entity edit forms.
 */
class JobPostingOrderLogForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New job posting order log %label has been created.', $message_arguments));
      $this->logger('restorationjobs_job_posting_order_log')->notice('Created new job posting order log %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The job posting order log %label has been updated.', $message_arguments));
      $this->logger('restorationjobs_job_posting_order_log')->notice('Created new job posting order log %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.job_posting_order_log.canonical', ['job_posting_order_log' => $entity->id()]);
  }

}
