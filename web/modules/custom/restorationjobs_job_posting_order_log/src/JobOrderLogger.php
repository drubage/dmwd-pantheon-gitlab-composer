<?php

namespace Drupal\restorationjobs_job_posting_order_log;

class JobOrderLogger {

  /**
   * The first order for a new job posting.
   */
  const JOB_ORDER_TYPE_OPENED = 'Opened';

  /**
   * The job posting order was renewed.
   */
  const JOB_ORDER_TYPE_RENEWED = 'Renewed';

  /**
   * Job posting and it's order are closed.
   */
  const JOB_ORDER_TYPE_CLOSED = 'Closed';

  /**
   * A closed job posting/order is reopened.
   */
  const JOB_ORDER_TYPE_REOPENED = 'Reopened';
}
