<?php

namespace Drupal\restorationjobs_job_posting_order_log;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a job posting order log entity type.
 */
interface JobPostingOrderLogInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the job posting order log creation timestamp.
   *
   * @return int
   *   Creation timestamp of the job posting order log.
   */
  public function getCreatedTime();

  /**
   * Sets the job posting order log creation timestamp.
   *
   * @param int $timestamp
   *   The job posting order log creation timestamp.
   *
   * @return \Drupal\restorationjobs_job_posting_order_log\JobPostingOrderLogInterface
   *   The called job posting order log entity.
   */
  public function setCreatedTime($timestamp);

}
