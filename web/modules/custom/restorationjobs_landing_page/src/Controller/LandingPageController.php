<?php

namespace Drupal\restorationjobs_landing_page\Controller;
use Drupal\Core\Controller\ControllerBase;

class LandingPageController extends ControllerBase {
  public function content() {
    return array(
    '#type' => 'markup',
    '#markup' => $this->t('Welcome to my website!'),);
  }
}