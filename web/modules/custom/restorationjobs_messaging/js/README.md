# Vue Training

## Class Ininerary
1. Front end framework theory, best practices, project set up
2. Vue Component Anatomy Overview
3. Basic Templating (data/view binding)
4. Advanced Templating (iterations and conditionals)
5. Nested Components
6. Lifecycle-Hooks and Methods
7. Computed Properties
8. Vue Store Overview
9. Vue Store Actions and Mutators
10. CRUD Vue Store data through components
11. Basic Routing
12. Advanced Routing (dynamic routes)

## Initial setup
   * Clone the repository and CD into the repository directory
   * Run `npm install` to install required Node packages

## Local development
   * CD into the project directory
   * Run `npm run dev` to start the WebPack Development Server with Hot Module Replacement
   * Visit `localhost:8080` in your browser
   
## Build deploy package
   * CD into the project directory
   * Run `npm run prod` to compile and minify assets to the public directory
