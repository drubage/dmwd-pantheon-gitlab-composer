// Load required libraries
import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

// Load Vue components
import MessagesJobs from "../components/MessagesJobs";
import MessagesApplications from "../components/MessagesApplications";
import MessagesThread from "../components/MessagesThread";
import MessagesBreadcrumb from "../components/MessagesBreadcrumb";

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    messages: [],
    jobs: [],
    apps: [],
    user_type: drupalSettings.user_type,
    current_job_id: (drupalSettings.job_id != 'undefined' ? drupalSettings.job_id : ''),
    current_app_id: (drupalSettings.app_id != 'undefined' ? drupalSettings.app_id : ''),
    current_uid: drupalSettings.user.uid,
    csrf_token: '',
    is_loading_messages: 0,
    is_loading_apps: 0,
    is_loading_jobs: 0,
  },
  actions: {
    loadJobs(context) {
      context.commit('setLoadingJobs', 1);
      axios.get('/messages-json').then((response) => {
        context.commit('updateJobs', response.data.data);
        context.commit('setLoadingJobs', 0);
      });
    },
    loadApps(context, unset_app_id) {
      if (unset_app_id) {
        context.commit('setCurrentSelectedAppID', null);
      }
      context.commit('setLoadingApps', 1);
      axios.get('/messages-json/' + context.state.current_job_id).then((response) => {
        context.commit('updateApps', response.data.data);
        context.commit('setLoadingApps', 0);
      });
    },
    loadMessages(context, show_loading) {
      if (show_loading) {
        context.commit('setLoadingMessages', 1);
      }
      axios.get('/messages-json/' + context.state.current_job_id + '/' + context.state.current_app_id).then((response) => {
        context.commit('updateMessages', response.data.data);
        context.commit('setLoadingMessages', 0);
      });
    },
    getCSRF(context) {
      axios.get('rest/session/token', { withCredentials: true }).then(response => {
        context.commit('setCSRF', response.data);
      });
    },
  },
  mutations: {
    updateJobs(state, jobs) {
      state.jobs = jobs;
    },
    setLoadingMessages(state, newValue) {
      state.is_loading_messages = newValue;
    },
    setLoadingApps(state, newValue) {
      state.is_loading_apps = newValue;
    },
    setLoadingJobs(state, newValue) {
      state.is_loading_jobs = newValue;
    },
    updateApps(state, apps) {
      state.apps = apps;
    },
    updateMessages(state, messages) {
      state.messages = messages;
      if (messages.length) {
        var last_message = messages.slice(-1).pop();
        if (Object.keys(state.apps).length) {
          if (last_message.body.length) {
            state.apps[state.current_app_id].snippet = last_message.body.slice(0, 50);
          }
          if (state.apps[state.current_app_id].new_count > 0) {
            state.jobs[state.current_job_id].new_count -= state.apps[state.current_app_id].new_count;
            state.apps[state.current_app_id].new_count = 0;
          }
        } else {
          state.jobs[state.current_job_id].new_count = 0;
        }
      }
    },
    setCurrentSelectedJobID(state, newValue) {
      state.current_job_id = newValue;
    },
    setCurrentSelectedAppID(state, newValue) {
      state.current_app_id = newValue;
    },
    setCSRF(state, newValue) {
      state.csrf_token = newValue;
    },
  }
});


new Vue({
  el: "#messages-vue",
  store,
  data() {
    return {};
  },
  components: {
    MessagesJobs,
    MessagesApplications,
    MessagesThread,
    MessagesBreadcrumb
  },
  computed: {
    currentSelectedAppID: {
      get() {
        return this.$store.state.current_app_id;
      },
      set(newValue) {
        this.$store.commit("setCurrentSelectedAppID", newValue);
      }
    },
    currentSelectedJobID: {
      get() {
        return this.$store.state.current_job_id;
      },
      set(newValue) {
        this.$store.commit("setCurrentSelectedJobID", newValue);
      }
    },
  },
  created() {
    this.$store.dispatch('loadJobs');
    this.$store.dispatch('getCSRF');
    if (this.currentSelectedAppID) {
      this.$store.dispatch('loadApps', false);
      if (this.currentSelectedJobID) {
        this.$store.dispatch('loadMessages', true);
      }
    }
  }
});

window.addEventListener("load", (e) => {
  document.body.className += ' ' + 'loaded';
});
