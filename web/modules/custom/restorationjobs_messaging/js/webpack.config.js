const path = require("path");
const Webpack = require("webpack");
const {VueLoaderPlugin} = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");

let Config = {
  entry: {
    app: "./src/js/app.js"
  },
  output: {
    path: path.resolve(__dirname, "./public"),
    publicPath: "/",
    filename: "js/[name].js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader"
      },
      {
        test: /\.vue$/,
        use: "vue-loader"
      },
      {
        test: /\.css$/,
        use: [
          "vue-style-loader",
          "css-loader"
        ]
      }
    ]
  },
  resolve: {
    alias: {
      vue$: "vue/dist/vue.esm.js"
    },
    extensions: [
      "*",
      ".css",
      ".js",
      ".vue",
      ".json"
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ]
};

module.exports = (env, argv) => {
  if (argv.mode === "production") {
    Config.watch = false;
  }

  if (argv.mode === "development") {
    Config.plugins = Config.plugins.concat([
      new Webpack.HotModuleReplacementPlugin()
    ]);

    Config = Object.assign(Config, {
      watch: true,
      watchOptions: {
        poll: true
      },
      devServer: {
        contentBase: path.join(__dirname, "public"),
        hot: true
      }
    });
  }

  return Config;
};