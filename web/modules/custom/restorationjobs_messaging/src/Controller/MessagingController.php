<?php

namespace Drupal\restorationjobs_messaging\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\restorationjobs_companies\AccessChecks;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\comment\Entity\Comment;
use Drupal\image\Entity\ImageStyle;
use Drupal\restorationjobs_companies\Controller\Notifications;

/**
 * Class MessagingController.
 */
class MessagingController extends ControllerBase {


  public function getCommentSnippet(ContentEntityInterface $job_posting, ContentEntityInterface $job_application) {
    $snippet = 'No messages in this thread.';
    $latest_cid = \Drupal::entityQuery('comment')
      ->condition('entity_id', $job_posting->id())
      ->condition('field_application', $job_application->id())
      ->condition('entity_type', 'job_posting')
      ->sort('created', 'DESC')
      ->range(0, 1)
      ->execute();
    if ($latest_cid) {
      $latest_comment = \Drupal::entityTypeManager()->getStorage('comment')->load(array_keys($latest_cid)[0]);
      $snippet = substr(strip_tags($latest_comment->get('comment_body')->value), 0, 50);
    }
    return $snippet;
  }

  /*
   * Gets job applications for a job_posting entity by id
   */
  public function getApplicationsForJob(ContentEntityInterface $job_posting) {

    //todo convert to entity query
    $results = $job_posting->getApplicationsForJob();
    $applicant_title = $job_posting->get('field_job_title')->entity->getName();
    $apps = [];
    foreach($results as $result) {
      $job_application = \Drupal::entityTypeManager()->getStorage('job_application')->load($result);
      $status = $job_application->get('field_status')->getString();
      $new_count = $this->getUnreadCount($job_posting, $job_application);
      $snippet = $this->getCommentSnippet($job_posting, $job_application);
      $apps[$job_application->id()] = [
        'applicant_name' => $job_application->getOwner()->get('field_first_name')->value . ' ' . $job_application->getOwner()->get('field_last_name')->value,
        'new_count' => $new_count,
        'applicant_title' => $applicant_title,
        'app_id' => $job_application->id(),
        'app_uid' => $job_application->getOwner()->id(),
        'snippet' => $snippet,
        'status' => $status,
      ];
    }
    return new JsonResponse([
      'data' => $apps,
      'method' => 'GET',
    ]);
  }

  public function getUnreadCount($job_posting, $job_application = NULL) {
    $database = \Drupal::database();
    $current_user = \Drupal::currentUser();
    $params = array();
    $sql = "SELECT COUNT(*)
      FROM {comment_field_data} c
      INNER JOIN {comment__field_application} a ON c.cid = a.entity_id
      LEFT JOIN {history_messages} h ON h.job_id = c.entity_id AND h.uid = :uid AND h.app_id = a.field_application_target_id
      WHERE (c.created > h.timestamp OR ISNULL(h.timestamp))
      AND c.entity_id = :job_id";
    if ($job_application) {
      $sql .= " AND a.field_application_target_id = :app_id";
      $params[':app_id'] = $job_application->id();
    }
    $params[':job_id'] = $job_posting->id();
    $params[':uid'] = $current_user->id();
    $count = $database->query($sql, $params)->fetchField();
    return $count;
  }

  /*
   * Gets messages for a job_posting entity and an job_application entity
   */
  public function getMessages(ContentEntityInterface $job_posting, ContentEntityInterface $job_application) {
    $database = \Drupal::database();
    $current_user = \Drupal::currentUser();
    $cids = \Drupal::entityQuery('comment')
      ->condition('entity_id', $job_posting->id())
      ->condition('field_application', $job_application->id())
      ->condition('entity_type', 'job_posting')
      ->sort('created', 'ASC')
      ->execute();
    $results = \Drupal::entityTypeManager()->getStorage('comment')->loadMultiple($cids);
    $messages = [];
    foreach($results as $result) {
      $message = [
        'uid' => $result->getOwner()->id(),
        'author' => substr($result->getOwner()->get('field_first_name')->value, 0, 1) . substr($result->getOwner()->get('field_last_name')->value, 0, 1),
        'body' => $result->get('comment_body')->value,
        'date' => date('m/d/y', $result->getCreatedTime()),
        'time' => date('g:ia', $result->getCreatedTime()),
      ];
      if (!$result->get('field_file')->isEmpty()) {
        $file = $result->field_file->view();
        $file["#title"] = '';
        $message['file'] = render($file);
      }
      $messages[] = $message;
    }

    $query = $database->merge('history_messages');
    $query->key([
      'uid' => $current_user->id(),
      'job_id' => $job_posting->id(),
      'app_id' => $job_application->id(),
    ])
    ->fields([
      'uid' => $current_user->id(),
      'job_id' => $job_posting->id(),
      'app_id' => $job_application->id(),
      'timestamp' => REQUEST_TIME,
    ])
    ->execute();

    return new JsonResponse([
      'data' => $messages,
      'method' => 'GET',
    ]);
  }

  /*
   * Gets jobs for current user
   */
  public function getCurrentUserJobs() {
    $database = \Drupal::database();
    $current_user = \Drupal::currentUser();
    $user_type = (AccessChecks::hasAccessToCompany()->isAllowed() ? 'employer' : 'seeker');

    $params = [];

    if ($user_type == 'employer') {
      //todo convert to entity query
      $sql = "SELECT job.id AS job_id FROM {job_posting_field_data} job
          INNER JOIN {commerce_license__license_job_posting} license_job ON job.id = license_job.license_job_posting_target_id
          INNER JOIN {commerce_license} license ON license_job.entity_id = license.license_id
          AND job.user_id = :uid";
    } else {
      $sql = "SELECT job.id AS job_id, app.id AS app_id, app.user_id AS app_uid FROM {job_application_field_data} app
          INNER JOIN {job_application__field_job_posting} job_ref ON job_ref.entity_id = app.id
          INNER JOIN {job_posting_field_data} job ON job.id = job_ref.field_job_posting_target_id
          INNER JOIN {job_application__field_status} job_status ON job_status.entity_id = job_ref.entity_id
          AND job_status.field_status_value != 'declined'
          AND app.user_id = :uid";
    }
    $params[':uid'] = $current_user->id();
    $results = $database->query($sql, $params);
    $jobs = [];
    foreach($results as $result) {
      $job_posting = \Drupal::entityTypeManager()->getStorage('job_posting')->load($result->job_id);
      $state = $job_posting->get('state')->getString();
      $company = \Drupal::entityTypeManager()->getStorage('group')->load($job_posting->get('field_company')->getString());
      $author = $job_posting->getOwner();
      $jobs[$result->job_id] = [
        'author' => $author->get('field_first_name')->value . ' ' . $author->get('field_last_name')->value,
        'company' => $company->label(),
        'job_title' => $job_posting->get('field_job_title')->entity->getName(),
        'address' => $job_posting->get('field_location')->getValue(),
        'job_id' => $result->job_id,
        'state' => $state,
      ];
      if ($user_type == 'employer') {
        $jobs[$result->job_id]['new_count'] = $this->getUnreadCount($job_posting);
      } elseif ($user_type == 'seeker' && isset($result->app_id)) {
        $job_application = \Drupal::entityTypeManager()->getStorage('job_application')->load($result->app_id);
        $jobs[$result->job_id]['app_id'] = $result->app_id;
        $jobs[$result->job_id]['app_uid'] = $result->app_uid;
        $snippet = $this->getCommentSnippet($job_posting, $job_application);
        $jobs[$result->job_id]['snippet'] = $snippet;
        $jobs[$result->job_id]['new_count'] = $this->getUnreadCount($job_posting, $job_application);
        $jobs[$result->job_id]['app_status'] = $job_application->get('field_status')->getString();
      }
    }
    return new JsonResponse([
      'data' => $jobs,
      'method' => 'GET',
    ]);
  }

  /**
   * Messages.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * Callback for /messages
   */
  public function messages() {

    $user_type = (AccessChecks::hasAccessToCompany()->isAllowed() ? 'employer' : 'seeker');
    $drupal_settings = array(
      'user_type' => $user_type,
    );

    // to do access checks
    if (isset($_REQUEST['job_id']) && is_numeric($_REQUEST['job_id'])) {
      $drupal_settings['job_id'] = $_REQUEST['job_id'];
    }
    if (isset($_REQUEST['app_id']) && is_numeric($_REQUEST['app_id'])) {
      $drupal_settings['app_id'] = $_REQUEST['app_id'];
    }
    return [
      '#theme' => 'vue_messages',
      '#user_type' => $user_type,
      '#cache' => [
        'max-age' => 0
      ],
      '#attached' => array(
        'library' => array(
          'restorationjobs_messaging/messaging_vue',
        ),
        'drupalSettings'=> $drupal_settings,
      ),
    ];
  }

  /**
   * Create Message.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * Callback for /messages
   */
  public function createMessage() {
    $params = $_POST;

    // Avoid users sending notifications when their application is declined.
    $app = \Drupal::entityTypeManager()
      ->getStorage('job_application')
      ->load($params['field_application']);

    $status = $app->get('field_status')->getString();
    if ($status == 'declined') {
      return new JsonResponse([]);
    }

    if (!empty($_FILES['file']['tmp_name'])) {
      $file_data = file_get_contents($_FILES['file']['tmp_name']);
      $filename = $_FILES['file']['name'];
      $directory = 'public://messages';
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
      $filepath = $directory . '/' . $filename;
      $file = file_save_data($file_data, $filepath, FILE_EXISTS_RENAME);
      $params['field_file']['target_id'] = $file->id();
    }

    $comment = Comment::create($params);
    $comment->save();

    $response['data'] = 'Some test data to return';
    $response['method'] = 'PUT';

    $options = [
      'application' => $params['field_application'],
      'message' => $params['comment_body']
    ];

    $job_posting = \Drupal::entityTypeManager()
      ->getStorage('job_posting')->load($params['entity_id']);
    Notifications::sendNotification($job_posting, 'field_notifications_messages', $options);
    return new JsonResponse( $response );
  }
}
