<?php

namespace Drupal\restorationjobs_navigation\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'MainMenuBlock' block.
 *
 * @Block(
 *  id = "main_menu_block",
 *  admin_label = @Translation("Main menu block"),
 * )
 */
class MainMenuBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $links = [];
    $links[] = [
      'title' => 'Home',
      'url' => '/',
      'icon' => 'home',
    ];
    $links[] = [
      'title' => 'My Jobs',
      'url' => '/my-jobs',
      'icon' => 'briefcase',
    ];

    $config = \Drupal::config('restorationjobs_applicants.settings');
    $isRestricted = $config->get('restorationjobs_applicants.restricted_mode');
    if (!$isRestricted) {
      $links[] = [
        'title' => 'Messages',
        'url' => '/messages',
        'icon' => 'comment',
      ];
    }

    $children = NULL;
    if (\Drupal::currentUser()->id() > 0) {
      $children = [];
      $children[] = [
        'title' => 'Profile',
        'url' => '/profile',
      ];
      $children[] = [
        'title' => 'Account',
        'url' => '/account/edit',
      ];
      $children[] = [
        'title' => 'Log Out',
        'url' => '/user/logout',
      ];
    }

    $links[] = [
      'title' => 'Profile',
      'url' => '/profile',
      'icon' => 'user',
      'children' => $children,
    ];

    return [
      '#theme' => 'mainmenu',
      '#links' => $links,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
