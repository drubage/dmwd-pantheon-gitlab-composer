(function ($, window, Drupal) {
  Drupal.behaviors.rjInlineEntityFormWidget = {
    attach: function attach() {
      let selects = $('.field--widget-rj-inline-entity-form select');
      selects.on('change', function() {
        let $this = $(this);
        selects.not($this).attr('disabled', 'disabled');

        if ($this.val().length) {
          $this.closest('.ief-form').find('.ief-entity-submit').mousedown();
          $this.attr('disabled', 'disabled');
        }
      });

        // Avoid multiple rj inline entity form requests at the same
        // time causing wrong form overwrites.
        let buttons = $('.field--widget-rj-inline-entity-form  button');
        buttons.not('.ief-entity-submit').on('mousedown', function() {
          buttons.not($(this)).attr('disabled', 'disabled');
        });

        $(document).ajaxComplete(function () {
          selects.removeAttr('disabled');
          buttons.removeAttr('disabled');
      });

      /* for multi select
      if ($('.field--widget-rj-inline-entity-form select').attr('multiple')) {
        $('.field--widget-rj-inline-entity-form select').selectpicker({
          maxOptions: false,
          actionsBox: false,
          dropupAuto: false,
          size: 5,
          selectedTextFormat: 'count'
        });
        $('.field--widget-rj-inline-entity-form select').on('hide.bs.select', function() {
          if ($(this).val().length) {
            $(this).closest('.ief-form').find('.ief-entity-submit').mousedown();
            $(this).attr('disabled', 'disabled');
          }
        });
      }
     */
    }
  };
})(jQuery, window, Drupal);
