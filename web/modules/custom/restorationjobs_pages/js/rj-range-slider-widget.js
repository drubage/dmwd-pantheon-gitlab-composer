(function ($, window, Drupal) {
  Drupal.behaviors.rjRangeSliderWidget = {
    attach: function attach() {
      $(".rj-range-slider-widget-field").each(function(){
        var $from = $(this).find('.rj-range-slider-from');
        var $to = $(this).find('.rj-range-slider-to');
        let min = parseInt($from.attr("min")) || 1000;
        let max = parseInt($to.attr("max")) || 500000;
        var step = 1000;
        var from_val = parseInt($from.val()) || 10000;
        var to_val = parseInt($to.val()) || 150000;
        var $container = $(this);
        var $slider = $container && $container.find('.rj-range-slider-widget');
        let customHandlers = [];
        const minValueWrapper = $('.rj-range-slider-min-value');
        const maxValueWrapper = $('.rj-range-slider-max-value');
        const payInfoWrapper = $('.rj-payment .pay-info');

        if ($from.val() == '') {
          $from.val(from_val);
        }
        if ($to.val() == '') {
          $to.val(to_val);
        }

        if($slider && $slider.length > 0) {
          let firstHandle = null
          let secondHandle = null;

          $slider.slider({
            range: true,
            min: min,
            max: max,
            step: step,
            values: [ from_val, to_val ],
            slide: function( event, ui ) {
              $from.val(ui.values[0]);
              $to.val(ui.values[1]);
              updateHandlers(customHandlers, [ui.values[0], ui.values[1]], [minValueWrapper, maxValueWrapper]);
              payInfo(payInfoWrapper, [ui.values[0], ui.values[1]]);
            },
            create: function () {
              firstHandle = $('.ui-slider-handle:eq( 0 )');
              secondHandle = $('.ui-slider-handle:eq( 1 )');
              // Add custom handler elements.
              addCustomHandlers([firstHandle, secondHandle], $slider, [minValueWrapper, maxValueWrapper]);
              payInfo(payInfoWrapper, [$from.val(), $to.val()]);
            }
          });
        }
      });
    }
  };
  // Add custom handlers.
  function addCustomHandlers(handlers, slider, wrappers) {
    handlers.forEach((handler, index) => {
      const rawValue = slider.slider("values", index);
      const value = formatHandlerValue(rawValue);
      const customHandler = `<div class="custom-handler handler-${ index }">${ value }</div>`;
      handler.append(customHandler);
      wrappers[index].text(rawValue);
    });
    slider.removeClass('hidden');
  }
  // Update custom handler values.
  function updateHandlers(handlers, values, wrappers) {
    handlers[0] = handlers[0] || $('.custom-handler.handler-0');
    handlers[1] = handlers[1] || $('.custom-handler.handler-1');

    handlers[0].text(formatHandlerValue(values[0]));
    wrappers[0].text(values[0]);

    handlers[1].text(formatHandlerValue(values[1]));
    wrappers[1].text(values[1]);

    if ((values[1] - values[0]) < 40000) {
      handlers[0].addClass('push');
      handlers[1].addClass('push');
    } else {
      handlers[0].removeClass('push');
      handlers[1].removeClass('push');
    }
  }
  // Format handler value,
  function formatHandlerValue(value) {
    if (value < 1000) {
      return `$ ${ value }`;
    }

    return `$ ${ parseInt(value/1000) }K`;
  }
  // Update payment information.
  function payInfo(payInfo, values) {
    values.forEach((value, index) => {
      if (value > 999) {
        value = value.toString().split("");
        value.splice(-3, 0, ',')
        values[index] = value.join('');
      }
    });
    payInfo.text(`$ ${values[0]} - $ ${values[1] }`);
  }

})(jQuery, window, Drupal);
