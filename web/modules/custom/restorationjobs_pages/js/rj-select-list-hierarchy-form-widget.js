(function ($, window, Drupal) {
  Drupal.behaviors.rjSelectListHierarchWidget = {
    attach: function attach() {
      if ($('#edit-field-emphasis').length) {
        $(".rj-select-list-hierarchy-widget-field").change(update_emphasis);
        update_emphasis(false);
      }
      function update_emphasis() {
        var term_specialties = drupalSettings.term_specialties;
        var val = $('.rj-select-list-hierarchy-widget-field').val();
        var dependee_val = $('#edit-field-emphasis').val();
        reset_emphasis();
        if (val != '') {
          if (term_specialties.hasOwnProperty(val)) {
            $('#edit-field-emphasis').removeAttr('disabled');
            for (var dependee in term_specialties[val]) {
              $('#edit-field-emphasis').find('option[value="' + term_specialties[val][dependee] + '"]').removeAttr('disabled');
              if (dependee_val == term_specialties[val][dependee]) {
                $('#edit-field-emphasis').val(term_specialties[val][dependee]);
              }
            }
          }
        }
      }
      function reset_emphasis() {
        $('#edit-field-emphasis').val('_none').attr('disabled', true).find('option').not('option[value="_none"]').attr('disabled', true);
      }
    }
  };

})(jQuery, window, Drupal);
