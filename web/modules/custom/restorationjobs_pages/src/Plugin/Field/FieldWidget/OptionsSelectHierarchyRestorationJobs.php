<?php

namespace Drupal\restorationjobs_pages\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "rj_options_select_hierarchy",
 *   label = @Translation("RJ - Select List Hierarchy"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class OptionsSelectHierarchyRestorationJobs extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $settings = $items->getFieldDefinition()->getSettings();
    $bundles = $settings["handler_settings"]["target_bundles"];
    $terms_array = $options = [];
    foreach($bundles as $vid) {
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($terms as $key => $term) {
        $terms_array[$term->tid] = $term; // store for later
      }
      foreach ($terms_array as $key => $term) {
        foreach ($term->parents as $parent) {
          if ($parent == 0) {
            $options[$term->name] = [];
          } else {
            $options[$terms_array[$parent]->name][$term->tid] = $term->name;
          }
        }
      }
    }
    $element['#empty_option'] = $element['#title'];
    $element['#options'] = $options;

    if ($form["field_emphasis"]) {
      $element['#attributes']['class'][] = "rj-select-list-hierarchy-widget-field";
      $form['#attached']['library'][] = 'restorationjobs_pages/rj_select_list_hierarchy_form_widget';
      $term_specialties = array();
      foreach($terms AS $term) {
        $tid = $term->tid;
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
        $specialties = $term->get('field_specialties')->getValue();
        foreach($specialties as $specialty_tid) {
          $term_specialties[$tid][] = $specialty_tid['target_id'];
        }
      }
      $form['#attached']['drupalSettings']['term_specialties'] = $term_specialties;
    }
    return $element;
  }
}
