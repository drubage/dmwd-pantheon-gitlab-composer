<?php

namespace Drupal\restorationjobs_pages\Plugin\Field\FieldWidget;

use Drupal\range\Plugin\Field\FieldWidget\RangeWidget;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'range slider' widget.
 *
 * @FieldWidget(
 *   id = "rj_range_slider",
 *   label = @Translation("RJ - Range Slider"),
 *   field_types = {
 *     "range_integer",
 *     "range_float",
 *     "range_decimal"
 *   }
 * )
 */
class RangeWidgetRestorationJobs extends RangeWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'restorationjobs_pages/rj_range_slider_widget';
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#field_suffix'] = "<div class='rj-range-slider-min-valued'></div><div class='rjd-range-slider-max-value'></div><div class='hidden rj-range-slider-widget'></div>";
    $element['#attributes']['class'][] = "rj-range-slider-widget-field";
    $element['from']['#attributes']['readonly'] = "readonly";
    $element['from']['#attributes']['class'][] = "rj-range-slider-from";
    $element['to']['#attributes']['readonly'] = "readonly";
    $element['to']['#attributes']['class'][] = "rj-range-slider-to";
    $element['#field_suffix'] .= '
        <div class="rj-payment">
          <div class="pay-info form-control">$ 0 - $ 500,000</div>
          <div class="pay-description">' . t('Depending on experience') . '</div>
      </div>';

    return $element;
  }
}
