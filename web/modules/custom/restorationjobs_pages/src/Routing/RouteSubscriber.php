<?php

namespace Drupal\restorationjobs_pages\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = [
      'commerce_cart.page',
      'commerce_checkout.form',
      'entity.commerce_product.canonical'
    ];

    foreach($routes as $target) {
      if ($route = $collection->get($target)) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
