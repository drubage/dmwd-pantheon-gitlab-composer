<?php

$databases['default']['default'] = array (
  'database' => 'default',
  'username' => 'user',
  'password' => 'user',
  'host' => 'db',
  'prefix' => '',
  'port' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['config_sync_directory'] = '../config';

# Docksal Split Config Settings
$config['config_split.config_split.development']['status'] = TRUE;

# Docksal SMTP Settings
$config['smtp.settings']['smtp_host'] = 'mail';
$config['smtp.settings']['smtp_port'] = 1025;
$config['smtp.settings']['smtp_protocol'] = 'standard';
$config['smtp.settings']['smtp_username'] = '';
$config['smtp.settings']['smtp_password'] = '';

# Docksal Aggregation and Error Reporting Settings
$config['system.performance']['css']['preprocess'] = 0;
$config['system.performance']['js']['preprocess'] = 0;
$config['system.logging']['error_level'] = 'verbose';

$config['commerce_payment.commerce_payment_gateway.braintree']['configuration']['mode'] = getenv('BRAINTREE_MODE');
$config['commerce_payment.commerce_payment_gateway.braintree']['configuration']['merchant_id'] = getenv('BRAINTREE_MERCHANT_ID');
$config['commerce_payment.commerce_payment_gateway.braintree']['configuration']['public_key'] = getenv('BRAINTREE_PUBLIC_KEY');
$config['commerce_payment.commerce_payment_gateway.braintree']['configuration']['private_key'] = getenv('BRAINTREE_PRIVATE_KEY');
$config['commerce_payment.commerce_payment_gateway.braintree']['configuration']['merchant_account_id']['USD'] = getenv('BRAINTREE_MERCHANT_ACCOUNT_ID');

