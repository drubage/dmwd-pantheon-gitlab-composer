(function($){

  Drupal.behaviors.scripts = {
    attach: function (context, settings) {

      $('#job-posting-add-form div[data-drupal-selector=edit-field-hard-skills-form-actions] .js-form-submit, #job-posting-add-form div[data-drupal-selector=edit-field-soft-skills-form-actions] .js-form-submit, #job-posting-add-form div[data-drupal-selector=edit-field-certifications-form-actions] .js-form-submit').once().mousedown(function() {
        $('#edit-field-hard-skills-wrapper button.js-form-submit').attr('disabled', 'disabled');
        $('#edit-field-soft-skills-wrapper button.js-form-submit').attr('disabled', 'disabled');
        $('#edit-field-certifications-wrapper button.js-form-submit').attr('disabled', 'disabled');
      });

      // allows top level menu items to be clickable in footer
      $('.footer-main .menu.nav').find('.dropdown-toggle').removeAttr('data-toggle');

      $('.scrolling-messages-container').scrollbar();
      $('.selectpicker').each(function() {
        $(this).closest('.select-wrapper').addClass('has-selectpicker');
      });
      $('.job-posting-wizard-form').submit(function() {
        var $btn = $(this).find('.form-actions .btn-primary');
        if ($btn.val() != 'Post Job and Go Live') { // this is the modal button
          $btn.addClass('disabled');
        }
      });

      $('.job-posting-wizard-form .form-actions a.btn, .modal-actions .btn').click(function() {
        $(this).addClass('disabled');
      });
      $('.field--name-field-certifications .ief-row-entity').once().each(function() {
        var name = $(this).find('.inline-entity-form-taxonomy_term-label').text();
        $(this).prepend('<input class="required-check checkbox-circle" type="checkbox" name="' + name + '" data-toggle="tooltip" title="Make required"  />');
        $(this).find('.required-check').on('change', function(e) {
          set_required_certs();
        });
      });
      set_required_checkboxes();
      function set_required_checkboxes() {
        $('.field--name-field-required-certifications select option[value="_none"]').prop('selected', false);
        $('.field--name-field-required-certifications select option:selected').each(function() {
          if ($('.field--name-field-certifications .ief-row-entity input[name="' + $(this).text() + '"]').length) {
            $('.field--name-field-certifications .ief-row-entity input[name="' + $(this).text() + '"]').prop('checked', true);
          } else {
            $(this).prop('selected', false);
          }
        });
      }
      function set_required_certs() {
        $('.field--name-field-required-certifications select option').prop('selected', false);
        $('.field--name-field-certifications .ief-row-entity .required-check').each(function() {
          if ($(this).is(':checked')) {
            $('.field--name-field-required-certifications select option:contains(' + $(this).attr('name') + ')').prop('selected', true);
          }
        });
      }
    }
  };

  $(document).ready(function() {

    $('.tabs--primary').addClass('dragscroll');
    dragscroll.reset();
    $('.dragscroll a').click(function(event) {
      if ($(this).closest('.dragscroll').hasClass('dragging')) {
        event.preventDefault();
        return false;
      }
    });

    if ($('.tabs--primary li.active').length) {
      $('.tabs--primary').animate({scrollLeft: $('.tabs--primary li.active').position().left - 40}, 0);
    }

    if ($('#user-profile-job-applicant-form').length) {
      var is_submitting_profile = false;
      $('#user-profile-job-applicant-form').data('serialize', $('#user-profile-job-applicant-form').serialize());
      $('#user-profile-job-applicant-form').submit(function() {
        is_submitting_profile = true;
      });
      $(window).bind('beforeunload', function (e) {
        if (!is_submitting_profile) {
          if ($('#user-profile-job-applicant-form').serialize() != $('#user-profile-job-applicant-form').data('serialize')) {
            return true;
          } else {
            e = null;
          }
        }
      });
    }

  });

}(jQuery));
